package tests;

import junit.framework.Assert;
import househub.appliances.Appliance;
import househub.appliances.parsers.ApplianceParser;

import org.json.simple.JSONValue;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ApplianceParserTest {
	private ApplianceParser parser;
	
	private static final String applianceJson =
			"{ \"kind\" : \"BlockObject\", " +
					"\"id\" : 1, " +
					"\"type\" : \"door\", " +
					"\"reg_time\" : \"2013-12-20 07:52:34\", " +
					"\"validated\" : 1, " +
					"\"connected\" : 1, " +
					"\"schemeName\" : \"basicDoor\", " +
					"\"visual\" : { " +
						"\"name\" : \"\", " +
						"\"condition\" : \"doorOpen\"}, " +
					"\"services\" : [ " +
						"{ \"id\" : \"==QTR1TP\", \"name\" : \"travar\"}, " +
						"{ \"id\" : \"==QTn1TP\", \"name\" : \"destravar\"} ], " +
					"\"status\" : [ " +
						"{ \"id\" : 1, \"name\" : \"aberta\", \"value\" : 1}, " +
						"{ \"id\" : 2, \"name\" : \"travada\", \"value\" : 0} ], " +
					"\"objects\" : [ ]}";
	
	@Before
	public void setUp() throws Exception {
		parser = new ApplianceParser();
	}

	@After
	public void tearDown() throws Exception {
	}

	
	
	@Test
	public void testParse() {
		Appliance appliance = parser.parse(JSONValue.parse(applianceJson));

		Assert.assertEquals(1, appliance.getId());
		Assert.assertEquals("door", appliance.getType());
		Assert.assertEquals(true, appliance.getValidated());
		Assert.assertEquals(true, appliance.getConnected());
		Assert.assertEquals("2013-12-20 07:52:34", appliance.getRegTime());
		Assert.assertEquals("basicDoor", appliance.getSchemeName());
		//Nome default == appliance.getType()
		Assert.assertEquals("Label should be: " + appliance.getType() +
					" but was found: " + appliance.getLabel()
					,appliance.getType(), appliance.getLabel());

		Assert.assertEquals(2, appliance.getServices().size());
		Assert.assertEquals("==QTR1TP", appliance.getServices().get(0).getId());
		Assert.assertEquals("travar", appliance.getServices().get(0).getLabel());
		Assert.assertEquals("==QTn1TP", appliance.getServices().get(1).getId());
		Assert.assertEquals("destravar", appliance.getServices().get(1).getLabel());

		Assert.assertEquals(2, appliance.getStatus().size());
		Assert.assertEquals(1, appliance.getStatus().get(0).getId());
		Assert.assertEquals("aberta", appliance.getStatus().get(0).getName());
		Assert.assertEquals(1, appliance.getStatus().get(0).getValue());
		Assert.assertEquals(2, appliance.getStatus().get(1).getId());
		Assert.assertEquals("travada", appliance.getStatus().get(1).getName());
		Assert.assertEquals(0, appliance.getStatus().get(1).getValue());
		

		Assert.assertEquals(0, appliance.getApplianceChildren().size());
		
	}

}
