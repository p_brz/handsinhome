package tests;

import househub.network.HttpConnection;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.junit.Before;
import org.junit.Test;

public class HttpConnectionTest {
	private HttpConnection conn;
	
	@Before
	public void setup(){
		conn = new HttpConnection();
	}
	
	@Test
	public void testSendGetHttp(){
		try {
//			conn.addParameter("method", "login");
//			conn.addParameter("username", "adm");
//			conn.addParameter("password", "123456");
			conn.addParameter("method", "verify_login");
			String result = conn.sendGet("http://localhost/househub/token.php");
			JSONObject answerJson = (JSONObject) JSONValue.parse(result);
			System.out.println("status: " + answerJson.get("status"));
			JSONObject contentJson = (JSONObject) answerJson.get("content");
			System.out.println("content: " + contentJson);
			System.out.println("phpsessid: " + contentJson.get("phpsessid"));
			System.out.println(answerJson.toJSONString());
			System.out.println(result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testSendPostHttp(){
		try {
			conn.addParameter("method", "login");
			conn.addParameter("username", "adm");
			conn.addParameter("password", "123456");
			String result = conn.sendPost("http://localhost/househub/token.php");
			JSONObject answerJson = (JSONObject) JSONValue.parse(result);
			System.out.println("status: " + (Long)answerJson.get("status"));
			JSONObject contentJson = (JSONObject) answerJson.get("content");
			System.out.println("content: " + contentJson);
			System.out.println("phpsessid: " + contentJson.get("phpsessid"));
			System.out.println(answerJson.toJSONString());
			System.out.println(result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
