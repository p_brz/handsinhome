package tests;

import househub.network.HouseRequester;
import househub.network.RequestAnswer;

import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HouseRequesterTest {
	private HouseRequester requester;
	private static final String serverUrl = "http://localhost/househub/token.php";
	private static final String user = "adm";
	private static final String password = "123456";
	
	@Before
	public void setUp() throws Exception {
		requester = new HouseRequester();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testConnect() {
		requester.connect(user, password, serverUrl);		
		Assert.assertTrue(requester.isConnected());
		Assert.assertNotNull(requester.getSessionId());
	}

	@Test
	public void testDisconnect() {
		requester.connect(user, password, serverUrl);		
		Assert.assertTrue(requester.isConnected());
		requester.disconnect();		
		Assert.assertFalse(requester.isConnected());
		Assert.assertNull(requester.getSessionId());
	}

	@Test
	public void testRequestMethodString() {
		requester.connect(user, password, serverUrl);
		RequestAnswer answer = requester.requestMethod("verify_login");
		Assert.assertTrue(answer.isSuccessful());
		requester.disconnect();
	}

	@Test
	public void testRequestMethodStringListOfRequestParameter() {
		requester.connect(user, password, serverUrl);
		RequestAnswer answer = requester.requestMethod("list_objects");
		
		Assert.assertTrue(answer.isSuccessful());
		Assert.assertNotNull(answer.getContent());
		Assert.assertTrue(answer.getContent() instanceof List<?>);
		
		System.out.println("Objects: " + answer.getContent());
		
		requester.disconnect();
	}

}
