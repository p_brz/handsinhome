package tests;

import gui.components.Border;
import gui.components.Component;
import gui.components.Container;
import gui.components.HorizontalAlignment;
import gui.components.VerticalAlignment;
import gui.components.list.HorizontalListLayout;
import gui.components.list.ListView;

import java.awt.Dimension;
import java.awt.Point;

import org.junit.Before;
import org.junit.Test;

import processing.core.PApplet;
import processing.core.PGraphics;
import tests.utils.TestListLayoutHelper;

@SuppressWarnings("serial")
public class HorizontalListLayoutTest extends PApplet{
	public static void main(String[] args){
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", HorizontalListLayoutTest.class.getName() });
    }
	

	private ListView container;
	private HorizontalListLayout horizontalLayout;
	private TestListLayoutHelper helper = new TestListLayoutHelper();
	
	@Override
	public void setup(){
		this.size(640, 480);
		
		this.container = new ListView(0, height/2);		
		helper.addComponents(container, 5);
		
		horizontalLayout = new HorizontalListLayout(new Border(0,0,30,50));
		horizontalLayout.doLayout(container, null);
	}

	@Override
	public void draw(){
		background(220);
		drawContainer(30,this.height /2 - 50, HorizontalAlignment.Left, VerticalAlignment.Top);
		drawContainer(30,this.height /2, HorizontalAlignment.Center, VerticalAlignment.Center);
		drawContainer(30,this.height /2 + 50, HorizontalAlignment.Right, VerticalAlignment.Bottom);
	}

	private void drawContainer(int x, int y
			, HorizontalAlignment horAlignment, VerticalAlignment vertAlignment)
	{
	    layoutContainer(container, x, y, horAlignment, vertAlignment);
		container.draw(g);
	    drawComponentsBox(this.g, container, horizontalLayout.getComponentBorder());
    }

	
	public void drawComponentsBox(PGraphics graphics, Container container, Border border) {

		graphics.pushStyle();
			graphics.noFill();
			graphics.rect(container.getX(), container.getY(), container.getWidth(), container.getHeight());
		
	    	int x = container.getX();
	    	int y = container.getY();
	
		    for(Component comp : container.getComponents()){
		    	int width = border.getLeft() + border.getRight() + comp.getWidth();
		    	int height = border.getTop() + border.getBottom() + comp.getHeight();
		    	
		    	
		    	graphics.stroke(255,x,y);
		    	graphics.rect(x, y, width, height);
		    	
		    	x += width;
		    }
	    
	    graphics.popStyle();
    }
	
	private void layoutContainer(Container container, int x, int y
			, HorizontalAlignment horAlignment, VerticalAlignment vertAlignment)
	{
	    this.container.setLocation(x, y);
		this.horizontalLayout.setComponentHorizontalAlignment(horAlignment);
		this.horizontalLayout.setComponentVerticalAlignment(vertAlignment);
		this.horizontalLayout.doLayout(container, null);
    }
	
	
	//TESTES

	@Before
	public void setUp() throws Exception {
		container = new ListView();
		helper.addComponents(container, 10);
		horizontalLayout = new HorizontalListLayout();
	}
	
	@Test
	public void testDoLayout() {		
		this.container.clear();
		helper.addComponents(container
				, new Dimension[]{new Dimension(10,10),new Dimension(10,10),new Dimension(10,10)});
		
		
		horizontalLayout.doLayout(container, null);
		Point[] expectedPositions = new Point[]{new Point(0,0), new Point(10,0), new Point(20,0)};
		helper.testPosition(container, expectedPositions);
	}
	@Test
	public void testDoLayoutWithBorder() {		
		this.container.clear();
		helper.addComponents(container
				, new Dimension[]{new Dimension(10,10),new Dimension(10,10),new Dimension(10,10)});
		

		horizontalLayout.setComponentBorder(new Border(10,30,10,20));
		horizontalLayout.doLayout(container, null);
		
		Point[] expectedPositions = new Point[]{new Point(15,20), new Point(55,20), new Point(95,20)};
		helper.testPosition(container, expectedPositions);
	}
	
	@Test
	public void testDoLayoutWithBorderAndAlignment() {
		this.container.clear();
		helper.addComponents(container
				, new Dimension[]{new Dimension(10,10),new Dimension(10,10),new Dimension(10,10)});
		

		horizontalLayout.setComponentBorder(new Border(0,10,10,20));
		horizontalLayout.setComponentAlignment(HorizontalAlignment.Right, VerticalAlignment.Top);
		horizontalLayout.doLayout(container, null);
		
		Point[] expectedPositions = new Point[]{new Point(30,0), new Point(70,0), new Point(110,0)};
		helper.testPosition(container, expectedPositions);
	}
	@Test
	public void testDoLayoutComponentsDiferentSize() {
		horizontalLayout.setComponentBorder(new Border(0,0,10,10));
		horizontalLayout.setComponentAlignment(HorizontalAlignment.Center, VerticalAlignment.Center);
		
		this.container.clear();
		helper.addComponents(container
				, new Dimension[]{new Dimension(20,20),new Dimension(40,40),new Dimension(10,10)});
		
		
		horizontalLayout.doLayout(container, null);
		Point[] expectedPositions = new Point[]{new Point(10,10), new Point(50,0), new Point(110,15)};
		helper.testPosition(container, expectedPositions);
	}

	
	

// AUTOMATIC expected position calculator
//	private void testPosition(HorizontalListLayout listLayout) {
//	    int startX = 0;
//	    int index = 0;
//		for(Component comp : container.getComponents())
//		{
//			Border border = listLayout.getComponentBorder();
//			
//			Point expectedPosition = calculateExpectedPosition(comp, listLayout, startX);
//			Assert.assertEquals("In comp. " + index + " x position is not correct"
//					, (int)expectedPosition.getX(), comp.getX());
//			Assert.assertEquals("In comp. " + index + " y position is not correct"
//					,(int)expectedPosition.getY(), comp.getY());
//			
//			startX += comp.getWidth() + border.getLeft() + border.getRight();
//			++index;
//		}
//	    
//    }
//	private Point calculateExpectedPosition(Component comp,
//			HorizontalListLayout layout, int startX) {
//
//	    int expectedX = getExpectedX(startX, comp, layout);
//	    int expectedY = getExpectedY(comp, layout);
//	    
//	    return new Point(expectedX, expectedY);
//    }
//	private int getExpectedY(Component component, HorizontalListLayout layout ) {
//		VerticalAlignment verticalAlignment = layout.getVerticalAlignment();
//		Border border = layout.getComponentBorder();
//	    if(verticalAlignment == VerticalAlignment.Top){
//	    	return 0;
//	    }
//	    else if(verticalAlignment == VerticalAlignment.Center){
//	    	return (border.getTop() + border.getBottom()) / 2;
//	    }
//	    else if(verticalAlignment == VerticalAlignment.Bottom){
//	    	return (border.getTop() + border.getBottom());
//	    }
//	    else {//error
//	    	return -1;
//	    }
//    }
//	private int getExpectedX(int startX, Component component, HorizontalListLayout layout ) {
//		HorizontalAlignment horAlignment = layout.getHorizontalAlignment();
//		Border border = layout.getComponentBorder();
//	    if(horAlignment == HorizontalAlignment.Left){
//	    	return startX;
//	    }
//	    else if(horAlignment == HorizontalAlignment.Center){
//	    	return startX + (border.getLeft() + border.getRight()) / 2;
//	    }
//	    else if(horAlignment == HorizontalAlignment.Right){
//	    	return startX + (border.getLeft() + border.getRight());
//	    }
//	    else {//error
//	    	return -1;
//	    }
//    }

}
