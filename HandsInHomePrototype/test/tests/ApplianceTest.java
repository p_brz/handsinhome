package tests;

import static org.junit.Assert.fail;
import househub.HomeManager;
import househub.appliances.Appliance;

import java.util.LinkedList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ApplianceTest {

	private Appliance appliance;
	private static List<Appliance> appliancesFake;
	
	
	@BeforeClass
	public static void setupBeforeClass(){
		appliancesFake = new LinkedList<Appliance>(HomeManager.getInstance().loadAppliances());
	}
	
	@Before
	public void setUp() throws Exception {
		appliance = new Appliance();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testAppliance() {
		this.appliance = new Appliance();
		Assert.assertNotNull(appliance);
		Assert.assertTrue(this.appliance.getServices().isEmpty());
		Assert.assertTrue(this.appliance.getStatus().isEmpty());
		Assert.assertTrue(this.appliance.getApplianceChildren().isEmpty());
	}

	@Test
	public final void testApplianceViewComponentsString() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testApplianceStringString() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testApplianceViewComponentsStringString() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testApplianceViewDefinition() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testClone() {
		for(Appliance app : appliancesFake){
			Appliance appClone = (Appliance) app.clone();
			Assert.assertNotSame(app, appClone);
			Assert.assertEquals(app, appClone);
		}
	}

	@Test
	public final void testSetView() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetId() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetLabel() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetStatus() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetServices() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testAddService() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testRemoveService() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testAddStatus() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testRemoveStatus() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetType() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetRegTime() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetValidatedInteger() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetValidatedBoolean() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetConnectedBoolean() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetConnectedInt() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetSchemeName() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testAddApplianceChild() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testRemoveApplianceChild() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetApplianceChildren() {
		fail("Not yet implemented"); // TODO
	}

}
