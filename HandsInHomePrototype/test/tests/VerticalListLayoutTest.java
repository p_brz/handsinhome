package tests;

import gui.components.Border;
import gui.components.Component;
import gui.components.Container;
import gui.components.HorizontalAlignment;
import gui.components.VerticalAlignment;
import gui.components.list.ListView;
import gui.components.list.VerticalListLayout;

import java.awt.Dimension;
import java.awt.Point;

import org.junit.Before;
import org.junit.Test;

import processing.core.PApplet;
import processing.core.PGraphics;
import tests.utils.TestListLayoutHelper;

@SuppressWarnings("serial")
public class VerticalListLayoutTest extends PApplet{
	public static void main(String[] args){
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", VerticalListLayoutTest.class.getName() });
    }

	private ListView container;
	private VerticalListLayout layout;
	private TestListLayoutHelper helper = new TestListLayoutHelper();
	
	@Override
	public void setup(){
		this.size(640, 480);
		
		this.container = new ListView(0, height/2);		
		helper.addComponents(container, 5);
		
		layout = new VerticalListLayout(new Border(10,20,0,0));
		layout.doLayout(container, null);
	}

	@Override
	public void draw(){
		background(220);
		drawContainer(this.width /2 - 50, 30, HorizontalAlignment.Left, VerticalAlignment.Top);
		drawContainer(this.width /2     , 30, HorizontalAlignment.Center, VerticalAlignment.Center);
		drawContainer(this.width /2 + 50, 30, HorizontalAlignment.Right, VerticalAlignment.Bottom);
	}

	private void drawContainer(int x, int y
			, HorizontalAlignment horAlignment, VerticalAlignment vertAlignment)
	{
	    layoutContainer(container, x, y, horAlignment, vertAlignment);
		container.draw(g);
	    drawComponentsBox(this.g, container, layout.getComponentBorder());
    }

	private void layoutContainer(Container container, int x, int y
			, HorizontalAlignment horAlignment, VerticalAlignment vertAlignment)
	{
	    this.container.setLocation(x, y);
		this.layout.setComponentHorizontalAlignment(horAlignment);
		this.layout.setComponentVerticalAlignment(vertAlignment);
		this.layout.doLayout(container, null);
    }

	public void drawComponentsBox(PGraphics graphics, Container container, Border border) {

		graphics.pushStyle();
			graphics.noFill();
			graphics.rect(container.getX(), container.getY(), container.getWidth(), container.getHeight());
		
	    	int x = container.getX() ;
	    	int y = container.getY() ;
	
		    for(Component comp : container.getComponents()){
		    	int width = border.getLeft() + border.getRight() + comp.getWidth();
		    	int height = border.getTop() + border.getBottom() + comp.getHeight();
		    	
		    	
		    	graphics.stroke(255,x,y);
		    	graphics.rect(x, y, width, height);
		    	
		    	y += height;
		    }
	    
	    graphics.popStyle();
    }
	
	
	//TESTES

	@Before
	public void setUp() throws Exception {
		container = new ListView();
		helper.addComponents(container, 10);
		layout = new VerticalListLayout();
	}
	
	@Test
	public void testDoLayout() {		
		this.container.clear();
		helper.addComponents(container
				, new Dimension[]{new Dimension(10,10),new Dimension(10,10),new Dimension(10,10)});
		
		
		layout.doLayout(container, null);
		Point[] expectedPositions = new Point[]{new Point(0,0), new Point(0,10), new Point(0,20)};
		helper.testPosition(container, expectedPositions);
	}
	@Test
	public void testDoLayoutWithBorder() {		
		this.container.clear();
		helper.addComponents(container
				, new Dimension[]{new Dimension(10,10),new Dimension(10,10),new Dimension(10,10)});
		

		layout.setComponentBorder(new Border(10,20,10,30));
		layout.doLayout(container, null);
		
		Point[] expectedPositions = new Point[]{new Point(20,15), new Point(20,55), new Point(20,95)};
		helper.testPosition(container, expectedPositions);
	}
	
	@Test
	public void testDoLayoutWithBorderAndAlignment() {
		this.container.clear();
		helper.addComponents(container
				, new Dimension[]{new Dimension(10,10),new Dimension(10,10),new Dimension(10,10)});
		

		layout.setComponentBorder(new Border(10,20,0,10));
		layout.setComponentAlignment(HorizontalAlignment.Right, VerticalAlignment.Bottom);
		layout.doLayout(container, null);
		
		Point[] expectedPositions = new Point[]{new Point(10,30), new Point(10,70), new Point(10,110)};
		helper.testPosition(container, expectedPositions);
	}
	@Test
	public void testDoLayoutComponentsDiferentSize() {
		layout.setComponentBorder(new Border(10,10,10,0));
		layout.setComponentAlignment(HorizontalAlignment.Center, VerticalAlignment.Center);
		
		this.container.clear();
		helper.addComponents(container
				, new Dimension[]{new Dimension(20,20),new Dimension(40,40),new Dimension(10,10)});
		
		
		layout.doLayout(container, null);
		Point[] expectedPositions = new Point[]{new Point(15,10), new Point(5,50), new Point(20,110)};
		helper.testPosition(container, expectedPositions);
	}
	
//	@Override
//	public void setup(){
//		this.size(640, 480);
//		
//		this.container = new ListView(0, 30);		
//		addComponents(container, 5);
//		
//		verticalLayout = new VerticalListLayout();
//	}
//
//	@Override
//	public void draw(){
//		background(220);
//		layoutContainer(this.container, this.width /2 - 50, 30, 
//				HorizontalAlignment.Left, VerticalAlignment.Top);
//		drawContainer(this.container);
//		
//		layoutContainer(this.container, this.width /2     , 30, 
//				HorizontalAlignment.Center, VerticalAlignment.Center);
//		drawContainer(this.container);
//		
//		layoutContainer(this.container, this.width /2 + 50, 30,
//				HorizontalAlignment.Right, VerticalAlignment.Bottom);
//		drawContainer(this.container);
//	}
//
//	private void layoutContainer(Container container, int x, int y
//			, HorizontalAlignment horAlignment, VerticalAlignment vertAlignment)
//	{
//	    this.container.setLocation(x, y);
//		verticalLayout.setComponentHorizontalAlignment(horAlignment);
//		verticalLayout.setComponentVerticalAlignment(vertAlignment);
//		verticalLayout.doLayout(container, null);
//    }
//	private void drawContainer(Container container)
//	{
//		container.draw(g);
//		g.pushStyle();
//			g.noFill();
//			g.rect(container.getX(), container.getY(), container.getWidth(), container.getHeight());
//		g.popStyle();
//    }
//
//	@Before
//	public void setUp() throws Exception {
//		container = new ListView();
//		addComponents(container, 10);
//		verticalLayout = new VerticalListLayout();
//	}
//
//	private void addComponents(Container container, int count) {
//		for(int i = 0; i < count; ++ i){
//		    container.addComponent(new EllipseComponent(0 , 0, 25, 25));
//		}
//    }
//
//	@Test
//	public void testDoLayout() {
//		verticalLayout.doLayout(container, null);
//		
//		testPosition(verticalLayout);
//	}
//	@Test
//	public void testDoLayoutWithBorder() {
//		verticalLayout.setComponentBorder(new Border(0,0,30,50));
//		verticalLayout.doLayout(container, null);
//		testPosition(verticalLayout);
//	}
//	@Test
//	public void testDoLayoutWithBorderAndAlignment() {
//		verticalLayout.setComponentBorder(new Border(0,0,30,50));
//		verticalLayout.setComponentVerticalAlignment(VerticalAlignment.Top);
//		verticalLayout.setComponentHorizontalAlignment(HorizontalAlignment.Right);
//		
//		verticalLayout.doLayout(container, null);
//		testPosition(verticalLayout);
//	}
//
//	private void testPosition(BaseListLayout listLayout) {
//	    int startX = 0;
//	    int index = 0;
//		for(Component comp : container.getComponents())
//		{
//			Border border = listLayout.getComponentBorder();
//			
//			Point expectedPosition = calculateExpectedPosition(comp, listLayout, startX);
//			Assert.assertEquals("In comp. " + index + " x position is not correct"
//					, (int)expectedPosition.getX(), comp.getX());
//			Assert.assertEquals("In comp. " + index + " y position is not correct"
//					,(int)expectedPosition.getY(), comp.getY());
//			
//			startX += comp.getWidth() + border.getLeft() + border.getRight();
//			++index;
//		}
//	    
//    }
//
//	private Point calculateExpectedPosition(Component comp,
//			BaseListLayout layout, int startX) {
//
//	    int expectedX = getExpectedX(startX, comp, layout);
//	    int expectedY = getExpectedY(comp, layout);
//	    
//	    return new Point(expectedX, expectedY);
//    }
//
//	private int getExpectedY(Component component, BaseListLayout layout ) {
//		VerticalAlignment verticalAlignment = layout.getVerticalAlignment();
//		Border border = layout.getComponentBorder();
//	    if(verticalAlignment == VerticalAlignment.Top){
//	    	return 0;
//	    }
//	    else if(verticalAlignment == VerticalAlignment.Center){
//	    	return (border.getTop() + border.getBottom()) / 2;
//	    }
//	    else if(verticalAlignment == VerticalAlignment.Bottom){
//	    	return (border.getTop() + border.getBottom());
//	    }
//	    else {//error
//	    	return -1;
//	    }
//    }
//	private int getExpectedX(int startX, Component component, BaseListLayout layout ) {
//		HorizontalAlignment horAlignment = layout.getHorizontalAlignment();
//		Border border = layout.getComponentBorder();
//	    if(horAlignment == HorizontalAlignment.Left){
//	    	return startX;
//	    }
//	    else if(horAlignment == HorizontalAlignment.Center){
//	    	return startX + (border.getLeft() + border.getRight()) / 2;
//	    }
//	    else if(horAlignment == HorizontalAlignment.Right){
//	    	return startX + (border.getLeft() + border.getRight());
//	    }
//	    else {//error
//	    	return -1;
//	    }
//    }

}
