package tests;

import static org.junit.Assert.fail;
import gui.components.AbstractComponent;
import gui.components.Component;
import gui.components.list.CarouselListContainer;
import gui.components.list.ListSelection;

import java.util.LinkedList;
import java.util.List;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import processing.core.PGraphics;
import tests.utils.EllipseComponent;

public class CarouselListContainerTest {
	private CarouselListContainer carousel;
	
	@Before
	public void setUp() throws Exception {
		carousel = new CarouselListContainer(0, 0, 100, 50);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public final void testAddComponent() {
		List<Component> comps = new LinkedList<Component>();
		comps.add(new EllipseComponent(0, 0, 20, 20));
		comps.add(new EllipseComponent(0, 0, 20, 20));
		comps.add(new EllipseComponent(0, 0, 20, 20));
		comps.add(new EllipseComponent(0, 0, 20, 20));
		carousel.setMaxVisibleComponents(10);
		
		for(Component comp : comps){
			carousel.addComponent(comp);
		}
		
		for(int i=0; i < comps.size(); ++i){
			Assert.assertEquals(comps.get(i), carousel.getComponent(i));
		}
	}
	@Test
	public final void testGetComponentAt() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetComponentBorderIntIntIntInt() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetComponentBorderBorder() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetMaxVisibleComponents() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetLayout() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetComponentDimensionIntInt() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetComponentDimensionDimension() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSetTypeSelection() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public final void testSelectNext() {
		List<Component> comps = new LinkedList<Component>();
		comps.add(new EllipseComponent(0, 0, 20, 20));
		comps.add(new EllipseComponent(0, 0, 20, 20));
		comps.add(new EllipseComponent(0, 0, 20, 20));
		comps.add(new EllipseComponent(0, 0, 20, 20));
		carousel.setMaxVisibleComponents(10);
		carousel.setTypeSelection(ListSelection.Middle);
		
		for(Component comp : comps){
			carousel.addComponent(comp);
		}
		
		int expectedIndex = comps.size()/2;
		for(int i=0; i < comps.size(); ++i){
			int currentIndex = comps.indexOf(carousel.getComponentSelected());
			Assert.assertEquals(expectedIndex % comps.size(), currentIndex);
			
			expectedIndex++;
			carousel.selectNext();
		}
	}
	@Test
	public final void testSelectNextInvisible() {
		List<Component> comps = new LinkedList<Component>();
		comps.add(new EllipseComponent(0, 0, 20, 20));
		comps.add(new EllipseComponent(0, 0, 20, 20));
		comps.add(new EllipseComponent(0, 0, 20, 20));
		comps.add(new EllipseComponent(0, 0, 20, 20));
		carousel.setMaxVisibleComponents(2);
		carousel.setTypeSelection(ListSelection.Middle);
		
		for(Component comp : comps){
			carousel.addComponent(comp);
		}

		int expectedIndex = carousel.getMaxVisibleComponents() /2;
		for(int i=0; i < comps.size(); ++i){
			int currentIndex = comps.indexOf(carousel.getComponentSelected());
			Assert.assertEquals(expectedIndex % comps.size(), currentIndex);
			
			expectedIndex++;
			carousel.selectNext();
		}
	}

	@Test
	public final void testSelectPrevious() {
//		System.out.println(this.getClass() + "testSelectPrevious()");
		
		List<Component> comps = new LinkedList<Component>();
		comps.add(new EllipseComponent(0, 0, 20, 20));
		comps.add(new EllipseComponent(0, 0, 20, 20));
		comps.add(new EllipseComponent(0, 0, 20, 20));
		comps.add(new EllipseComponent(0, 0, 20, 20));
		carousel.setMaxVisibleComponents(10);
		carousel.setTypeSelection(ListSelection.Middle);
		
		for(Component comp : comps){
			carousel.addComponent(comp);
		}
		
		int expectedIndex = comps.size()/2;
		for(int count = 0; count < 2; ++ count){
			for(int i=0; i < comps.size(); ++i){
				Component selected = carousel.getComponentSelected();
				int currentIndex = comps.indexOf(selected);
				
//				System.out.println(expectedIndex +"["+ comps.get(expectedIndex)+ "]" 
//									+ " X " + 
//									currentIndex+"["+ selected+ "]" );
				
				Assert.assertEquals(expectedIndex, currentIndex);
				
				expectedIndex = (expectedIndex > 0 ? expectedIndex - 1 : comps.size() - 1);
				carousel.selectPrevious();
			}
		}
	}
	@Test
	public final void testSelectPreviousInvisible() {
//		System.out.println(this.getClass() + "testSelectPreviousInvisible()");
		List<Component> comps = new LinkedList<Component>();
		comps.add(new NamedStubComponent("comp1"));
		comps.add(new NamedStubComponent("comp2"));
		comps.add(new NamedStubComponent("comp3"));
		comps.add(new NamedStubComponent("comp4"));
		carousel.setMaxVisibleComponents(2);
		carousel.setTypeSelection(ListSelection.Middle);
		
		for(Component comp : comps){
			carousel.addComponent(comp);
		}
		
		int expectedIndex = carousel.getMaxVisibleComponents() /2;
		for(int count = 0; count < 2; ++ count){
			for(int i=0; i < comps.size(); ++i){
				Component selected = carousel.getComponentSelected();
				int currentIndex = comps.indexOf(selected);
				
//				System.out.println(expectedIndex +"["+ comps.get(expectedIndex)+ "]" 
//									+ " X " + 
//									currentIndex+"["+ selected+ "]" );
				
				Assert.assertEquals(expectedIndex, currentIndex);
				
				expectedIndex = (expectedIndex > 0 ? expectedIndex - 1 : comps.size() - 1);
				carousel.selectPrevious();
			}
		}
	}

	class NamedStubComponent extends AbstractComponent{
		public String name;
		
		public NamedStubComponent(String name){
			this.name = name;
		}
		
		@Override
	    public void draw(PGraphics graphics) {}
		
		@Override
		public String toString(){
			return this.getClass() + ".[" + this.name + "]";
		}
	}

}
