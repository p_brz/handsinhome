package tests;

import gestures.skeleton.BodyPart;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import processing.core.PVector;
import tools.recorder.SkeletonLoader;
import tools.recorder.SkeletonRecorder;

public class SkeletonRecorderTest {
	private SkeletonRecorder recorder;
	private SkeletonLoader loader;
	
	@Before
	public void setup(){
		recorder = new SkeletonRecorder();
		loader = new SkeletonLoader();
	}

	@Test
	public void testSaveSkeleton(){
		try {
			String filepath = this.makeFilePath("testSkeletonRec.skel");
			
			System.out.println(filepath);

			recorder.startRecord(filepath);
			Skeleton skeleton = buildRandomSkeleton();
			recorder.saveFrame(skeleton);
			recorder.endRecord();

			loader.startLoad(filepath);
			Skeleton loadedSkeleton = loader.loadFrame();
			loader.endLoad();
			

//			for(SkeletonPart part : SkeletonPart.values()){
//				BodyPart thisPart = skeleton.getPart(part);
//				BodyPart loadedPart = loadedSkeleton.getPart(part);
//				
//				Assert.assertEquals(part + " is not equals. Original value is " 
//							+ "[" + thisPart.getStartPosition() + "]" 
//						    +  " and loaded value is: "
//							+ "[" + loadedPart.getStartPosition() + "]" 
//						,thisPart, loadedPart);
//			}
			
			
			Assert.assertNotNull("LoadedSkeleton is null",loadedSkeleton);
			Assert.assertEquals("LoadedSkeleton is not equals to Skeleton",skeleton,loadedSkeleton);
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.toString());
		}
	}
	

	@Test
	public void testSaveManySkeleton(){
		try {
			String filepath = this.makeFilePath("testManySkeletons.skel");

			Skeleton savedSkeletons[] = buildSkeletons();
			
			saveSkeletons(filepath, savedSkeletons);

			Skeleton[] loadedSkeletons = loadSkeletons(filepath, savedSkeletons.length);

			
			for(int i=0; i < savedSkeletons.length;++i){
				System.out.println(
						i + ": " 
						   + " SavedSkeleton[" + savedSkeletons[i] + "]"
						   + " LoadedSkeleton[" + loadedSkeletons[i] + "]"
						);
				Assert.assertEquals(savedSkeletons[i], loadedSkeletons[i]);
			}
			
			
			Assert.assertFalse(loadedSkeletons[0].equals(loadedSkeletons[1]));
			Assert.assertTrue(Arrays.equals(savedSkeletons, loadedSkeletons));
//			Assert.assertArrayEquals(savedSkeletons, loadedSkeletons);
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.toString());
		}
	}
	@Test
	public void testLoadNoneSkeleton(){
		try {
			String filepath = this.makeFilePath("testNoneSkeletons.skel");

			recorder.startRecord(filepath);
			recorder.endRecord();

			loader.startLoad(filepath);
			Skeleton loadedSkeleton = loader.loadFrame();
			loader.endLoad();

			Assert.assertNull("LoadedSkeleton should be null",loadedSkeleton);
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.toString());
		}
	}
	@Test
	public void testLoadMoreThanSavedSkeleton(){
		try {
			String filepath = this.makeFilePath("testLoadMoreThanSavedSkeleton.skel");

			recorder.startRecord(filepath);
				recorder.saveFrame(buildRandomSkeleton());
			recorder.endRecord();

			loader.startLoad(filepath);
				Skeleton loadedSkeleton = loader.loadFrame();
				Skeleton secondLoadedSkeleton = loader.loadFrame();
			loader.endLoad();

			Assert.assertTrue("Expected class " + Skeleton.class.getName() 
					+ " but found " + loadedSkeleton.getClass().getName()
					,loadedSkeleton instanceof Skeleton);
			Assert.assertNull(secondLoadedSkeleton);
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.toString());
		}
	}

	@Test
	public void testSaveTheSameTwice(){
		try {
			String filepath = this.makeFilePath("testSaveTheSameTwice.skel");

			Skeleton skelToSave = buildRandomSkeleton();
			Skeleton skelClone = (Skeleton) skelToSave.clone();
			recorder.startRecord(filepath);
				recorder.saveFrame(skelToSave);
				recorder.saveFrame(skelClone);
			recorder.endRecord();

			loader.startLoad(filepath);
				Skeleton loaded1 = loader.loadFrame();
				Skeleton loaded2 = loader.loadFrame();
			loader.endLoad();

//			System.out.println(skelToSave);
//			System.out.println(skelClone);
//			System.out.println(loaded1);
//			System.out.println(loaded2);
			Assert.assertEquals(skelToSave,skelClone);
			Assert.assertEquals(loaded1, loaded2);
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.toString());
		}
	}
	@Test
	public void testSaveTwoDifferentInTheSameReference(){
		try {
			String filepath = this.makeFilePath("testSaveTheSameTwice.skel");

			recorder.startRecord(filepath);
				Skeleton skelToSave = buildRandomSkeleton();
				recorder.saveFrame(skelToSave);
				System.out.println(skelToSave);
				
				skelToSave = buildRandomSkeleton();
				recorder.saveFrame(skelToSave);
				System.out.println(skelToSave);
			recorder.endRecord();

			loader.startLoad(filepath);
				Skeleton loaded1 = loader.loadFrame();
				Skeleton loaded2 = loader.loadFrame();
			loader.endLoad();

			System.out.println(loaded1);
			System.out.println(loaded2);
			Assert.assertFalse(loaded1.equals(loaded2));
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.toString());
		}
	}
	@Test
	public void testSaveTwiceTheWithChanges(){
		try {
			String filepath = this.makeFilePath("testSaveTheSameTwice.skel");

			recorder.startRecord(filepath);
				//Save skeleton
				Skeleton skelToSave = buildRandomSkeleton();
				recorder.saveFrame(skelToSave);
				System.out.println(skelToSave);
				//Change skeleton
				PVector oldPosition = skelToSave.getPart(SkeletonPart.Chest).getEndPosition();
				PVector newPosition = PVector.add(oldPosition, new PVector(1,1,1));
				skelToSave.getPart(SkeletonPart.Chest).setEndPosition(newPosition);
				//Save skeleton again
				recorder.saveFrame(skelToSave);
				System.out.println(skelToSave);
			recorder.endRecord();

			loader.startLoad(filepath);
				Skeleton loaded1 = loader.loadFrame();
				Skeleton loaded2 = loader.loadFrame();
			loader.endLoad();

			System.out.println(loaded1);
			System.out.println(loaded2);
			Assert.assertFalse(loaded1.equals(loaded2));
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.toString());
		}
	}

	private void saveSkeletons(String filepath, Skeleton[] savedSkeletons)
			throws FileNotFoundException, IOException {
		recorder.startRecord(filepath);
			for(Skeleton skel : savedSkeletons){
				recorder.saveFrame(skel);
			}
		recorder.endRecord();
	}

	private Skeleton[] loadSkeletons(String filepath, int number)
			throws FileNotFoundException, IOException, ClassNotFoundException 
	{
		Skeleton[] skeletons = new Skeleton[number];

		loader.startLoad(filepath);
		for(int i=0; i < skeletons.length; ++i){
			skeletons[i] = loader.loadFrame();
		}
		loader.endLoad();
		
		return skeletons;
	}

	private String makeFilePath(String filename){
		return this.recordsPath() + File.separator + filename;
	}
	private String recordsPath(){
		String dirPath = System.getProperty("user.dir");
//		return dirPath + File.separator + "skeletonRecords";
		return dirPath;
	}
	
	private Skeleton buildRandomSkeleton() {
		Skeleton skeleton = new Skeleton();
		for(SkeletonPart part : SkeletonPart.values()){
			skeleton.setPart(part, makeRandomBodyPart());
		}
		return skeleton;
	}

	private BodyPart makeRandomBodyPart() {
		Random random = new Random(System.nanoTime());
		float randX1 = random.nextFloat()*2000;
		float randY1 = random.nextFloat()*2000;
		float randZ1 = random.nextFloat()*2000;
		float randX2 = random.nextFloat()*2000;
		float randY2 = random.nextFloat()*2000;
		float randZ2 = random.nextFloat()*2000;
		return new BodyPart(new PVector(randX1,randY1,randZ1), new PVector(randX2,randY2,randZ2));
	}

	private Skeleton[] buildSkeletons() {
		int numSkeletons = 11;
		Skeleton[] skeletons = new Skeleton[numSkeletons];
		for(int i=0; i < numSkeletons; ++i){
			skeletons[i] = buildRandomSkeleton();
		}
		return skeletons;
	}
}
