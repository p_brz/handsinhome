package tests.prototypes;

import gui.animations.TranslateAnimation;
import gui.components.Component;

import java.util.LinkedList;
import java.util.List;

import processing.core.PApplet;
import processing.core.PConstants;
import tests.utils.EllipseComponent;

@SuppressWarnings("serial")
public class TranslateAnimationPrototype extends PApplet{
	public static void main(String[] args){
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", TranslateAnimationPrototype.class.getName() });
    }
	
	private TranslateAnimation animation;
	private List<Component> components;
	
	public TranslateAnimationPrototype(){
		this.animation = new TranslateAnimation(100, 100);
		this.components = new LinkedList<Component>();
	}

	@Override
	public void setup(){
		this.size(640, 480);
		
		for(int i = 0; i < 10; ++ i){
			int x = i * 30 + 20;
			int y = height/2;
			this.components.add(new EllipseComponent(x , y, 25, 25));
		}
	}

	@Override
	public void draw(){
		background(220);
		for(Component comp : this.components){
			this.animation.draw(this.g, comp);
		}
		
		if(animation.isPlaying()){
			animation.next();
		}
	}
	
	@Override
	public void keyPressed(){
		if(this.key == ' '){
			if(!this.animation.isPlaying()){
				this.animation.play();
			}
			else{
				this.animation.pause();
			}
		}
		else if(key == this.CODED){
			if(keyCode == PConstants.LEFT){
				if(!this.animation.isReverse()){
					this.animation.end();
					this.animation.play();
					this.animation.setReverse(true);
				}
			}
			else if(keyCode == PConstants.RIGHT){
				if(this.animation.isReverse()){
					this.animation.end();
					this.animation.play();
					this.animation.setReverse(false);
				}
			}
		}
	}
}
