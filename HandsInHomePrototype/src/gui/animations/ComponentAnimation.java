package gui.animations;

import gui.components.Component;
import processing.core.PGraphics;

public interface ComponentAnimation {

	public void play();
	public void next();
	public void pause();
	public void end();

	public boolean isPlaying();
	public boolean hasEnd();
	public boolean isReverse();
	public boolean isReversible();
	public void setReverse(boolean reverse);

	public void draw(PGraphics graphics, Component component);
}