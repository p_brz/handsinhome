package gui.components;

public class HorizontalAligner implements ComponentAligner{
    private HorizontalAlignment alignment;
    
    public HorizontalAligner(){
        this(HorizontalAlignment.Center);
    }
    public HorizontalAligner(HorizontalAlignment someAlignment){
        this.alignment = someAlignment;
    }
    
    public HorizontalAlignment getAlignment(){return alignment;}
    public void setAlignment(HorizontalAlignment someAlignment){this.alignment = someAlignment;}
    
    
    @Override
    public void align(Component comp, int xOrigin, int yOrigin ){
        if(alignment == HorizontalAlignment.Left){
            comp.setX(xOrigin);
        }
        else if(alignment == HorizontalAlignment.Center){
            comp.setX(xOrigin - comp.getWidth()/2);
        }
        else if(alignment == HorizontalAlignment.Right){
            comp.setX(xOrigin - comp.getWidth());
        }
    }

	@Override
    public void align(Component comp, int xOrigin, int yOrigin, int width, int height) {
        if(alignment == HorizontalAlignment.Left){
            comp.setX(xOrigin);
        }
        else if(alignment == HorizontalAlignment.Center){
        	int deltaX = width - comp.getWidth();
            comp.setX(xOrigin + deltaX/2);
        }
        else if(alignment == HorizontalAlignment.Right){
        	int rightBorder = xOrigin + width;
            comp.setX(rightBorder - comp.getWidth());
        }
	    
    }

}
