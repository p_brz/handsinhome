package gui.components;

public class Border {

	private int top, bottom, right, left;

	public Border(){
		this(0,0,0,0);
	}
	public Border(final int top, final int bottom, final int right, final int left) {
		this.top = top;
		this.bottom = bottom;
		this.right = right;
		this.left = left;
	}

	public int getTop()    { return top;    }
	public int getBottom() { return bottom; }
	public int getRight()  { return right;  }
	public int getLeft()   { return left;   }

	public void setTop   (final int top) 	  { this.top    = top;}
	public void setBottom(final int bottom) { this.bottom = bottom;}
	public void setRight (final int right)  { this.right  = right;}
	public void setLeft  (final int left)   { this.left   = left;}
	
	public void set(final Border border){
		this.setTop(border.getTop());
		this.setBottom(border.getBottom());
		this.setRight(border.getRight());
		this.setLeft(border.getLeft());
	}
	public int getWidthWithBorder(final Component comp) {
	    return comp.getWidth() + this.left + this.right;
    }
	public int getHeightWithBorder(final Component comp) {
	    return comp.getHeight() + this.top + this.bottom;
    }
}
