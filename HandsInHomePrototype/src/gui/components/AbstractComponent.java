package gui.components;

import processing.core.PGraphics;

public abstract class AbstractComponent implements Component
// public abstract class AbstractComponent implements MouseListener
{
	protected int x, y;
	protected int width, height;
	private CompositeAligner align;
	private Border border;

	public AbstractComponent() {
		this(0, 0, 0, 0);
	}

	public AbstractComponent(int someX, int someY, int someWidth, int someHeight) {
		this.x = someX;
		this.y = someY;
		this.width = someWidth;
		this.height = someHeight;
		this.align = new CompositeAligner();
		this.border = new Border();
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public void setX(int X) {
		this.x = X;
	}

	public void setY(int Y) {
		this.y = Y;
	}

	public void setLocation(int x, int y) {
		this.setX(x);
		this.setY(y);
	}

	public void setWidth(int Width) {
		this.width = Width;
	}

	public void setHeight(int Height) {
		this.height = Height;
	}

	public void setSize(int width, int height) {
		this.setWidth(width);
		this.setHeight(height);
		//TODO: (?)não seria melhor:
//		this.width = width;
//		this.height = height;
	}

	public Border getBorder() {
		return border;
	}

	public void setBorder(Border border) {
		this.border = border;
	}

	public boolean isOver(int X, int Y) {
		return (this.x <= X && (this.x + this.width) >= X)
				&& (this.y <= Y && (this.y + this.height) >= Y);
	}

	public abstract void draw(PGraphics graphics);

	// TODO: adicionar argumentos aos eventos
	public void mouseClicked() {
	}

	public void mouseEntered() {
	}

	public void mouseExited() {
	}

	public void mousePressed() {
	}

	public void mouseReleased() {
	}

	public void select() {
	}

	public void deselect() {
	}

	public void align(int xOrigin, int yOrigin) {
		this.align.align(this, xOrigin + border.getLeft(), yOrigin + border.getTop());
	}

	public void setCompositeAligner(HorizontalAligner horizontal,
			VerticalAligner vertical) {
		this.align.setHorizontalAligner(horizontal);
		this.align.setVerticalAligner(vertical);
	}

}
