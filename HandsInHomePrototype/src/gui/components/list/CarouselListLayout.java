package gui.components.list;

import gui.components.Component;
import gui.components.Container;

import java.awt.Dimension;

public class CarouselListLayout implements ListLayout{
	private ListLayout layout;
	private ListSelection typeSelection;
	private Dimension componentDimension;
	
	public CarouselListLayout(int compWidth, int compHeight){
		this.layout = new HorizontalListLayout();
		this.typeSelection = ListSelection.Middle;
		componentDimension = new Dimension(compWidth, compHeight);
	}

	public ListLayout getListLayout() { return layout;}
	public ListSelection getTypeSelection() { return typeSelection;}
	public Dimension getComponentDimension() { return componentDimension;}

	public void setListLayout(ListLayout layout) { this.layout = layout;}
	public void setTypeSelection(ListSelection typeSelection) {
		this.typeSelection = typeSelection;
	}
	public void setComponentDimension(Dimension componentDimension) {
		this.componentDimension = componentDimension;
	}
	
	@Override
	public void doLayout(Container container, Dimension maxSize) {
		restrictComponentsDimension(container);
		this.layout.doLayout(container, maxSize);
	}

	private void restrictComponentsDimension(Container container) {
	    int selectedIndex = this.typeSelection.getSelected(container);
	    int count = 0;
    	int width = (int)this.componentDimension.getWidth();
    	int height = (int)this.componentDimension.getHeight();
	    for(Component comp : container.getComponents()){
	    	if(count != selectedIndex){
	    		comp.setSize(width, height);
	    	}
	    	else{
		    	comp.setSize(width*2, height*2);
	    	}
	    	++count;
	    }
    }

	@Override
    public void align(Container container) {
	    // TODO Auto-generated method stub
	    
    }

}
