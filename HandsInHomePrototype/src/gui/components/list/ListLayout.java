package gui.components.list;

import gui.components.Container;

import java.awt.Dimension;

public interface ListLayout{
	//TODO: (?)encapsular maxSize dentro de Component
	public void doLayout(Container container, Dimension maxSize);
	/** alinha os componentes do container de acordo com o Layout */
	public void align(Container container);
//	public void adaptableSize(boolean restrict);
//	public boolean isAdaptable();
}
