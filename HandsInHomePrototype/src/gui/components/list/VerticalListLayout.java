package gui.components.list;

import gui.components.Border;
import gui.components.Component;
import gui.components.Container;

import java.awt.Dimension;
import java.util.Iterator;

public class VerticalListLayout extends AbstractListLayout{
	public VerticalListLayout(){
		this(null);
	}
	public VerticalListLayout(Border componentBorder){
		super(componentBorder);
	}

    @Override
	public void doLayout(Container container, Dimension maxSize) {
		layoutPosition(container);
		
		this.align(container);
	}
    
	public void layoutPosition(Container container) {
	    int x = 0;
		int y = 0;
		int maxWidth = 0;
		int totalHeight = 0;
		for(Component component : container.getComponents()){
			int xOrigin = x;
			int yOrigin = y;
			int width = componentBorder.getLeft() + component.getWidth() + componentBorder.getRight();
			int height = componentBorder.getTop() + component.getHeight() + componentBorder.getBottom();
			
//			aligner.align(component, xOrigin, yOrigin, width, height);
			component.setLocation(xOrigin + componentBorder.getLeft()
					, yOrigin + componentBorder.getTop());
			
			maxWidth = Math.max(maxWidth, width);
			
			y += height;
			totalHeight += height;
		}
		container.setSize(maxWidth, totalHeight);
    }
    
    @Override
    public void align(Container container){

		int currentY = 0;
		Iterator<Component> iterator = container.getComponents().iterator();
		while(iterator.hasNext()){
			Component comp = iterator.next();
			
			int xOrigin = 0;
			int yOrigin = currentY;
			int width   = container.getWidth();
			int height  = this.componentBorder.getHeightWithBorder(comp);
			
			this.aligner.align(comp, xOrigin, yOrigin,width,height);
			
			currentY += height;
		}
    }
    
    
}