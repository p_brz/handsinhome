package gui.components.list;

import gui.components.Border;
import gui.components.CompositeAligner;
import gui.components.Container;
import gui.components.HorizontalAlignment;
import gui.components.VerticalAlignment;

import java.awt.Dimension;

public abstract class AbstractListLayout implements ListLayout{

	protected Border componentBorder;
	protected CompositeAligner aligner;

	public AbstractListLayout(Border componentBorder) {
		this.componentBorder = new Border();
		if(componentBorder != null){
			this.componentBorder.set(componentBorder);
		}
		
		aligner = new CompositeAligner(HorizontalAlignment.Center, VerticalAlignment.Center);
	}

	public Border getComponentBorder() {
		return componentBorder;
	}

	public void setComponentBorder(Border componentBorder) {
		this.componentBorder.set(componentBorder);
	}


	public abstract void doLayout(Container container, Dimension maxSize);
	
	@Override
    public void align(Container container) {
    //		int xComp = 0;
    //		for (Component comp : container.getComponents()) {
    //			xComp = comp.getX();
    //			((AbstractComponent) comp).setCompositeAligner(
    //					new HorizontalAligner(HorizontalAlignment.Center),
    //					new VerticalAligner(VerticalAlignment.Center));
    //			comp.align(xComp, container.getHeight() / 2);
    //			comp.setX(xComp);
    //		}
    	}

	public VerticalAlignment getVerticalAlignment() {
	    return this.aligner.getVerticalAlignment();
	}

	public HorizontalAlignment getHorizontalAlignment() {
	    return this.aligner.getHorizontalAlignment();
	}

	public void setComponentVerticalAlignment(VerticalAlignment alignment) {
	    this.aligner.setVerticalAlignment(alignment);
	}

	public void setComponentHorizontalAlignment(HorizontalAlignment alignment) {
	    this.aligner.setHorizontalAlignment(alignment);
	}

	public void setComponentAlignment(HorizontalAlignment horAlignment
			, VerticalAlignment vertAlignment)
	{
		this.aligner.setHorizontalAlignment(horAlignment);
		this.aligner.setVerticalAlignment(vertAlignment);
	}
}