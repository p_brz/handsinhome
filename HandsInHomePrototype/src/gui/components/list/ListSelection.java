package gui.components.list;

import gui.components.Container;

public enum ListSelection {
	First, Middle, Last;
	public int getSelected(Container visibleComponents) {
		switch (this) {
		case First:
			return 0;
		case Middle:
			return visibleComponents.getComponentCount() / 2;
		case Last:
			return visibleComponents.getComponentCount() - 1;
		default:
			return 0;
		}
	}
}