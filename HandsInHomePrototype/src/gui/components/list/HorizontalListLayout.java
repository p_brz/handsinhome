package gui.components.list;

import gui.components.Border;
import gui.components.Component;
import gui.components.Container;

import java.awt.Dimension;
import java.util.Iterator;

public class HorizontalListLayout extends AbstractListLayout{	
	public HorizontalListLayout(){
		this(null);
	}
	public HorizontalListLayout(Border componentBorder){
		super(componentBorder);
	}

	@Override
	public void doLayout(Container container, Dimension maxSize) {
		layoutPosition(container);
		
		this.align(container);
	}
	public void layoutPosition(Container container) {
	    int totalWidth = 0;
		int maxHeight = 0;
		
		for(Component component : container.getComponents()){
			int xOrigin = totalWidth;
			int yOrigin = 0;
			int width = componentBorder.getLeft() + component.getWidth() + componentBorder.getRight();
			int height = componentBorder.getTop() + component.getHeight() + componentBorder.getBottom();
						
//			aligner.align(component, xOrigin, yOrigin, width, height);
			component.setLocation(xOrigin + componentBorder.getLeft()
					, yOrigin + componentBorder.getTop());
			totalWidth += width;
			maxHeight = Math.max(maxHeight, height);
		}
		container.setSize(totalWidth, maxHeight);
    }

	@Override
	public void align(Container container) {
//		int xSlice = container.getWidth() / container.getComponentCount();
		
		int currentX = 0;
		Iterator<Component> iterator = container.getComponents().iterator();
		while(iterator.hasNext()){
			Component comp = iterator.next();
			
			int xOrigin = currentX;
			int yOrigin = 0;
			int width = this.componentBorder.getWidthWithBorder(comp);
			int height = container.getHeight();
			
			this.aligner.align(comp, xOrigin, yOrigin,width,height);
			
			currentX += width;
		}
	}
}
