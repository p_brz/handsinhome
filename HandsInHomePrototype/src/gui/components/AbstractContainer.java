package gui.components;

import java.awt.Point;
import java.util.LinkedList;
import java.util.List;

import processing.core.PGraphics;

public abstract class AbstractContainer extends AbstractComponent implements Container {
	protected List<Component> components;
	private boolean layoutIsEnabled;

	public AbstractContainer() {
		this(0, 0, 0, 0);
	}
	public AbstractContainer(List<Component> someComponents) {
		this(0, 0, 0, 0, someComponents);
	}
	public AbstractContainer(int x, int y, int width, int height) {
		this(x, y, width, height, new LinkedList<Component>());
	}
	public AbstractContainer(int x, int y, int width, int height, List<Component> someComponents) {
		super(x, y, width, height);
		this.components = someComponents;
		layoutIsEnabled = true;
	}

	public void enableLayout(){
		layoutIsEnabled = true;
	}
	public void disableLayout(){
		this.layoutIsEnabled = false;
	}
	public boolean layoutIsEnabled(){
		return this.layoutIsEnabled;
	}
	
	
	@Override
	public void addComponent(Component comp) {
		components.add(comp);
		updateLayout();
	}

	@Override
	public void removeComponent(Component comp) {
		components.remove(comp);
		updateLayout();
	}

	@Override
	public void clear() {
		components.clear();
		updateLayout();
	}

	@Override
	public Iterable<Component> getComponents() {
		return components;
	}

	@Override
	public int getComponentCount() {
		return components.size();
	}

	@Override
	public boolean contains(Component comp) {
		return components.contains(comp);
	}

	@Override
	public Component getComponent(int index) {
		return this.components.get(index);
	}

	protected void addComponentAt(int index, Component comp) {
		this.components.add(index, comp);
	}

	@Override
	public Component getComponentAt(int x, int y) {
		if (this.isOver(x, y)) {
			Point localPos = this.convertToLocal(x, y);
			int localX = (int) localPos.getX();
			int localY = (int) localPos.getY();

			Component comp = null;
			for (Component c : this.getComponents()) {
				if (comp == null) {
					if (c instanceof Container) {// TODO:(?) mover metodo
													// getComponentAt para
													// Component ???
						comp = ((Container) c).getComponentAt(localX, localY);
					} else {
						// System.out.println(c +
						// ".isOver("+localX+","+localY+")?" +
						// c.isOver(localX,localY));
						comp = (c.isOver(localX, localY) ? c : null);
					}
				}
			}
			return (comp == null ? this : comp);
		}

		return null;
	}

	protected Point convertToLocal(int x, int y) {
		return new Point(x - this.getX(), y - this.getY());
	}

	/* TODO: criar layout managers para realizar as operações de layout */
	public void updateLayout(){
		if(this.layoutIsEnabled()){
			doLayout();
		}
	}
	
	protected abstract void doLayout();

	@Override
	public void draw(PGraphics graphics){
		for(Component comp : this.components){
			comp.draw(graphics);
		}
	}

}
