package gui.components;

public class VerticalAligner implements ComponentAligner{
    private VerticalAlignment alignment;
    
    public VerticalAligner(){
        this(VerticalAlignment.Top);
    }
    public VerticalAligner(VerticalAlignment alignment){
        this.alignment = alignment;
    }
    
    public VerticalAlignment getAlignment(){return alignment;}
    public void setAlignment(VerticalAlignment alignment){this.alignment = alignment;}
    
    
    @Override
    public void align(Component comp, int xOrigin, int yOrigin){
        if(alignment == VerticalAlignment.Top){
            comp.setY(yOrigin);
        }
        else if(alignment == VerticalAlignment.Center){
            comp.setY(yOrigin - comp.getHeight()/2);
        }
        else if(alignment == VerticalAlignment.Bottom){
            comp.setY(yOrigin - comp.getHeight());
        }
    }
	@Override
    public void align(Component comp, int xOrigin, int yOrigin, int width,
            int height) {
        if(alignment == VerticalAlignment.Top){
            comp.setY(yOrigin);
        }
        else if(alignment == VerticalAlignment.Center){
        	int deltaY = height - comp.getHeight();
            comp.setY(yOrigin + deltaY/2);
        }
        else if(alignment == VerticalAlignment.Bottom){
        	int bottomBorder = yOrigin + height;
            comp.setY(bottomBorder - comp.getHeight());
        }
	    
    }

}
