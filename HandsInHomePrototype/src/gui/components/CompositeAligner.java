package gui.components;

public class CompositeAligner implements ComponentAligner{
    private HorizontalAligner horAligner;
    private VerticalAligner   vertAligner;
    
    public CompositeAligner(){
        this(new HorizontalAligner(),new VerticalAligner());
    }
    public CompositeAligner(HorizontalAligner horizontalAligner, VerticalAligner verticalAligner){
        horAligner = horizontalAligner;
        vertAligner = verticalAligner;
    }
    
    public CompositeAligner(HorizontalAlignment horAlignment, VerticalAlignment vertAlignment) {
	    this.horAligner = new HorizontalAligner(horAlignment);
	    this.vertAligner = new VerticalAligner(vertAlignment);
    }
	public HorizontalAligner getHorizontalAligner(){return horAligner;}
    public void setHorizontalAligner(HorizontalAligner aligner){this.horAligner = aligner;}
    public VerticalAligner getVerticalAligner(){return vertAligner;}
    public void setVerticalAligner(VerticalAligner aligner){this.vertAligner = aligner;}
    
    
    @Override
    public void align(Component comp, int xOrigin, int yOrigin ){
        horAligner.align(comp,xOrigin,yOrigin);
        vertAligner.align(comp,xOrigin,yOrigin);
    }
	@Override
    public void align(Component comp, int xOrigin, int yOrigin, int width,
            int height) {
	    horAligner.align(comp, xOrigin, yOrigin, width, height);
	    vertAligner.align(comp, xOrigin, yOrigin, width, height);
    }
	
	public VerticalAlignment getVerticalAlignment() {
	    return this.vertAligner.getAlignment();
    }
	public HorizontalAlignment getHorizontalAlignment() {
	    return this.horAligner.getAlignment();
    }
	public void setVerticalAlignment(VerticalAlignment vertAlignment) {
	    this.vertAligner.setAlignment(vertAlignment);
    }
	public void setHorizontalAlignment(HorizontalAlignment horAlignment) {
	    this.horAligner.setAlignment(horAlignment);
    }
	public void setAlignment(HorizontalAlignment horAlign,
            VerticalAlignment vertAlign) {
	    this.setHorizontalAlignment(horAlign);
	    this.setVerticalAlignment(vertAlign);
    }

}
