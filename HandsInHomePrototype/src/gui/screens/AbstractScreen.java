package gui.screens;

import gui.components.AbstractComponent;
import gui.components.AbstractContainer;
import gui.components.Component;

import java.util.LinkedList;
import java.util.List;

import processing.core.PGraphics;

public abstract class AbstractScreen extends AbstractContainer implements
		Screen {
	private List<NavigationListener> navigationListeners;
	private String title;
	protected Boolean isReady;
	private ServiceParameterDialog popUpScreen;
	private String nameIcon;
//	private ImageComponent background;
//	private PImage backgroundImg;
	private int backgroundColor;
	
	public ServiceParameterDialog getPopUpScreen() {
		return popUpScreen;
	}

	public AbstractScreen(String title, String nameIcon) {
		this.isReady = false;
		this.title = title;
		this.nameIcon = nameIcon;
		navigationListeners = new LinkedList<NavigationListener>();
//		this.background = new ImageComponent();
		this.backgroundColor = 220;
	}

	public ServiceParameterDialog popUp(String title) {
		this.popUpScreen = new ServiceParameterDialog(title);
		return this.popUpScreen;
	}

	@Override
	public String getTitle() {
		return title;
	}
	

//	public PImage getBackgroundImage() {
//		return background.getImage();
//	}
//	public PImage getBackgroundImage() {
//		return backgroundImg;
//	}

	@Override
	public String getNameIcon() {
		return nameIcon;
	}

	public int getBackgroundColor() {
		return backgroundColor;
	}
	
	@Override
	public void setWidth(int width) {
		super.setWidth(width);
//		this.background.setWidth(width);
		this.updateLayout();
	}

	@Override
	public void setHeight(int height) {
		super.setHeight(height);
//		this.background.setWidth(height);
		this.updateLayout();
	}
//
//	public void setBackgroundImage(PImage backgroundImage) {
////		this.background.setImage(backgroundImage);
//		this.backgroundImg = backgroundImage;
//		this.backgroundImg.resize(this.getWidth(), this.getHeight());
//	}

	@Override
	public void setNameIcon(String nameIcon) {
		this.nameIcon = nameIcon;
	}

	public void setBackgroundColor(int backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
	
	
	
	@Override
	public void setup(){
		this.isReady = true;
	}
	
	@Override
	public void clearSetup(){
		this.isReady = false;
	}

	@Override
	public boolean isReady() {
		return isReady;
	}

	@Override
	public void draw(PGraphics graphics){
//		if(this.getBackgroundImage() != null){
//			graphics.image(this.getBackgroundImage(),0,0);
//		}
//		else{
//			graphics.background(this.backgroundColor);
//		}
		this.drawTitle(graphics);
		super.draw(graphics);
	}

	public void drawTitle(PGraphics graphics) {
		graphics.pushMatrix();
		graphics.pushStyle();
		graphics.translate(this.getWidth() / 2, 15);
		graphics.fill(100);
		graphics.scale(2);
		graphics.textAlign(graphics.CENTER, graphics.TOP);
//		graphics.text(this.getTitle().toUpperCase(), 0, 0);
		graphics.text(this.getTitle(), 0, 0);
		graphics.popStyle();
		graphics.popMatrix();
	}
	
	@Override
	protected abstract void doLayout();

	protected void fireNavigationEvent(NavigationEvent evt) {
		for (NavigationListener listener : navigationListeners) {
			listener.onNavigate(evt);
		}
	}

	public void mouseMoved(int mouseX, int mouseY, int pmouseX, int pmouseY) {
		Component currentComponent = this.getComponentAt(mouseX, mouseY);
		Component previousComponent = this.getComponentAt(pmouseX, pmouseY);

		if (currentComponent != previousComponent) { // mouseEnter)
			if (currentComponent != null
					&& (currentComponent instanceof AbstractComponent)) {
				// FIXME: remover downcasting
				((AbstractComponent) currentComponent).mouseEntered();
			}
			if (previousComponent != null
					&& (previousComponent instanceof AbstractComponent)) {
				((AbstractComponent) previousComponent).mouseExited();
			}
		}
	}

	public void keyPressed(char key, int keyCode) {
	}

	@Override
	public void addNavigationListener(NavigationListener listener) {
		navigationListeners.add(listener);
	}

	@Override
	public void removeNavigationListener(NavigationListener listener) {
		navigationListeners.remove(listener);
	}
	
	
}