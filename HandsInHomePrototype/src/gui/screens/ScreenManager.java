package gui.screens;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import processing.core.PGraphics;

public class ScreenManager implements NavigationListener {
	private String currentScreen;
	Map<String, Screen> mapScreen;
	private Stack<String> pastDestination;
	

	public ScreenManager() {
		currentScreen = "";
		this.mapScreen = new HashMap<String, Screen>();
		this.pastDestination = new Stack<String>();
	}

	public void draw(PGraphics graphics) {
		getCurrentScreen().draw(graphics);
	}

	public void addScreen(Screen screen) {
		if(screen == null){
			throw new IllegalArgumentException();
		}
		if(this.currentScreen == ""){
			this.currentScreen = screen.getTitle();
		}
		mapScreen.put(screen.getTitle(), screen);
		screen.addNavigationListener(this);

	}

	public void removeScreen(Screen screen) {
		mapScreen.remove(screen.getTitle());
	}
	
	public List<Screen> getScreens(){
		return new ArrayList<Screen>(this.mapScreen.values());
	}

	public void setCurrentScreen(String currentScreen) {
		if (getCurrentScreen() != null && getCurrentScreen().isReady()) {
			System.out.println("Clear " + getCurrentScreen().getTitle());
			getCurrentScreen().clearSetup();
		}
		this.currentScreen = currentScreen;
		if(!getCurrentScreen().isReady()){
			System.out.println("Setup " + getCurrentScreen().getTitle());
			getCurrentScreen().setup();
		}
	}

	public Screen getCurrentScreen() {
		return mapScreen.get(currentScreen);
	}

	public void mouseMoved(int mouseX, int mouseY, int pmouseX, int pmouseY) {

	}

	public void keyPressed(char key, int keyCode) {
		getCurrentScreen().keyPressed(key, keyCode);
	}

	@Override
	public void onNavigate(NavigationEvent evt) {
		if (this.pastDestination.isEmpty()) {
			this.pastDestination.add(currentScreen);
		}// se o fire for BACK

		if (evt.backDestination.equals(evt.getDestination())) {

			if (this.pastDestination.size() > 1) {
				this.pastDestination.pop(); // Tira da pilha a tela atual
				setCurrentScreen(this.pastDestination.peek());
			}

		} else {
			this.pastDestination.push(evt.getDestination());
			setCurrentScreen(evt.getDestination());
		}
		System.out.println(this.pastDestination.toString());

	}

}