package gui.screens;
import gui.components.Component;
import processing.core.PGraphics;

public interface Screen extends Component{
	public void addNavigationListener(NavigationListener listener);
	public void removeNavigationListener(NavigationListener listener);
	public boolean isReady();
	public void setup();
	public void clearSetup();
	public void draw(PGraphics graphics);
	public void setSize(int width, int height);
	public String getTitle();
	public void keyPressed(char key, int keyCode);
	public String getNameIcon();
	public void setNameIcon(String nameIcon);
}
