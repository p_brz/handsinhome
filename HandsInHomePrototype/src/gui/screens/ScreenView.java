package gui.screens;

import gui.components.AbstractComponent;
import gui.components.ImageComponent;
import gui.components.ImageManager;

import java.awt.Point;

import processing.core.PGraphics;
import processing.core.PImage;

public class ScreenView extends AbstractComponent {
	private ImageComponent nodeIcon;

	private Screen screen;

	public ScreenView(Screen screen) {
		this(screen, 0, 0, 0, 0);
	}

	public ScreenView(Screen screen, int width, int height) {
		this(screen,0, 0, width, height);
	}

	public ScreenView(Screen screen, int x, int y, int width, int height) {
		super(x, y, width, height);
		this.screen = screen;
		nodeIcon = new ImageComponent(0, 0, width, height);
		updateScreenView();
		
		if (screen != null && screen.getNameIcon() != null && (width == 0 && height==0)) {
			// Obtem altura e largura da imagem utilizada
			this.setSize(this.getImage().width, this.getImage().height);
			this.updateLayout();
		}
	}

	private void updateScreenView() {
		nodeIcon.setImage(ImageManager.getInstance().getImage(screen.getNameIcon()));
	}

	// private PImage getImage() { return nodeIcon.getImage();}
	public PImage getImage() {
		return this.nodeIcon.getImage();
	}

	@Override
	public boolean isOver(int x, int y) {
		Point ptLocal = convertToLocal(x, y);
		return nodeIcon.isOver((int) ptLocal.getX(), (int) ptLocal.getY());
	}

	private Point convertToLocal(int x, int y) {
		return new Point(x - this.getX(), y - this.getY()); // FIXME: criar
															// metodo auxiliar
	}

	public void draw(PGraphics graphic) {
		graphic.pushMatrix();
			graphic.translate(this.getX(), this.getY());
			nodeIcon.draw(graphic);
			
			drawScreenTitle(graphic);
		graphic.popMatrix();
	}

	void drawScreenTitle(PGraphics graphic) {
		graphic.pushStyle();
			int width = this.nodeIcon.getWidth();
			int height = this.nodeIcon.getHeight();
			graphic.textAlign(graphic.CENTER, graphic.BOTTOM);
			graphic.text(this.screen.getTitle(), width / 2, height + 15);
		graphic.popStyle();
	}

	@Override
	public void setWidth(int someWidth) {
		this.width = someWidth;
		this.updateLayout();
	}

	@Override
	public void setHeight(int someHeight) {
		this.height = someHeight;
		this.updateLayout();
	}

	@Override
	public void setSize(int width, int height) {
		this.setWidth(width);
		this.setHeight(height);
		this.updateLayout();
	}

	public void updateLayout() {
		nodeIcon.setSize(this.getWidth(), this.getHeight());
	}

	public Screen getScreen() {
		return this.screen;
	}

	public void setScreen(Screen screen) {
		this.screen = screen;
		this.updateScreenView();
	}


}
