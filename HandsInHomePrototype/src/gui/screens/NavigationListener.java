package gui.screens;

public interface NavigationListener {
	public void onNavigate(NavigationEvent evt);
}
