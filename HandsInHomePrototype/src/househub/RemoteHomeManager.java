package househub;

import househub.appliances.Appliance;
import househub.appliances.ApplianceService;
import househub.appliances.parsers.ApplianceParser;
import househub.network.HouseRequester;
import househub.network.RequestAnswer;
import househub.network.RequestParameter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Fachada para os serviços de manipulação da casa e comunicação com o 
 * sistema de domótica.
 * */
public class RemoteHomeManager extends HomeManager {
	private static final String LIST_APPLIANCES_METHOD = "list_objects";
	private static final String GET_APPLIANCES_METHOD = "gather_object";
	private static final String APPLIANCE_ID_FIELD = "object";
	private HouseRequester requester;
	private ApplianceParser applianceParser;
	
	public RemoteHomeManager(){
		requester = new HouseRequester();
		applianceParser = new ApplianceParser();
	}
	
	
	@Override
	public boolean connect(String username, String password, String serverUrl){
		return this.requester.connect(username, password, serverUrl);
	}
	@Override
	public boolean disconnect(){
		return this.requester.disconnect();
	}
	
	@Override
	public Collection<Appliance> loadAppliances(){
		RequestAnswer answer = this.requester.requestMethod(LIST_APPLIANCES_METHOD);
		if(answer.isSuccessful()){
			return makeAppliances(answer);
		}
		return null;
	}


	private Collection<Appliance> makeAppliances(RequestAnswer answer) {
		List<?> appliancesEncoded = (List<?>) answer.getContent();
		List<Appliance> appliances = new ArrayList<Appliance>();
		for(Object applianceEncoded : appliancesEncoded){
			appliances.add(this.applianceParser.parse(applianceEncoded));
		}
		return appliances;
	}
	@Override
	public Appliance loadAppliance(Appliance node){
		return this.loadAppliance(node.getId());
	}
	@Override
	public Appliance loadAppliance(int id){
		List<RequestParameter> parameters = new LinkedList<RequestParameter>();
		parameters.add(new RequestParameter(APPLIANCE_ID_FIELD, Integer.toString(id)));
		RequestAnswer answer = this.requester.requestMethod(GET_APPLIANCES_METHOD, parameters);
		if(answer.isSuccessful()){
			return this.applianceParser.parse(answer.getContent());
		}
		return null;
	}
	
	@Override
	public void executeApplianceService(Appliance selectedNode, ApplianceService selectedService) {
		// TODO Auto-generated method stub
		System.out.println("Executed service " + selectedService+ " for Node " + selectedNode );
	}
}
