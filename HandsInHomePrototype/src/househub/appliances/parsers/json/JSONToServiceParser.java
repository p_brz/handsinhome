package househub.appliances.parsers.json;

import househub.appliances.ApplianceService;
import househub.appliances.parsers.ServiceParser;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class JSONToServiceParser {
    private ServiceParser serviceParser;
    
    public JSONToServiceParser(){
    	serviceParser = new ServiceParser();
    }
    
    public ApplianceService parseJSON(String json){
        Object jsonPureObj = JSONValue.parse(json);
        JSONObject jsonObj = (JSONObject) jsonPureObj;
        return parseJSON(jsonObj);
    }
    
    public ApplianceService parseJSON(JSONObject json){        
        return serviceParser.parse(json);
    }
}
