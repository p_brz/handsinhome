/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package househub.appliances.parsers.json;

import househub.appliances.Appliance;
import househub.appliances.parsers.ApplianceParser;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class JSONToApplianceParser {
    private ApplianceParser applianceParser;
    
    public JSONToApplianceParser(){
    	applianceParser = new ApplianceParser();
    }
    
    public Appliance parseJSON(String json){
        Object jsonPureObj = JSONValue.parse(json);
        JSONObject jsonObj = (JSONObject) jsonPureObj;
        return parseJSON(jsonObj);
    }
    
    public Appliance parseJSON(JSONObject json){
        return applianceParser.parse(json);
    }
    
}
