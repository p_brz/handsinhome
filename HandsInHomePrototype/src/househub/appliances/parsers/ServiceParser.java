package househub.appliances.parsers;

import househub.appliances.ApplianceService;

import java.util.Map;


public class ServiceParser {
    
    private static final String NAME_KEY = "name";
	private static final String ID_KEY = "id";

	public ApplianceService parse(Map<?,?> json){
        ApplianceService service = new ApplianceService();
        if(json.containsKey(ID_KEY)){
            service.setId((String) json.get(ID_KEY));
        }
        
        if(json.containsKey(NAME_KEY)){
            service.setLabel((String) json.get(NAME_KEY));
        }
        
        return service;
    }
}
