/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package househub.appliances.parsers;

import househub.appliances.Appliance;
import househub.appliances.ApplianceService;
import househub.appliances.ViewDefinition;

import java.util.List;
import java.util.Map;

public class ApplianceParser {    
	private static final String ID_KEY = "id";
	private static final String TYPE_KEY = "type";
	private static final String REG_TIME_KEY = "reg_time";
	private static final String VALIDATED_KEY = "validated";
	private static final String CONNECTED_KEY = "connected";
	private static final String VISUAL_KEY = "visual";
	private static final String LABEL_KEY = "name";
	private static final String IMAGE_KEY = "image";
	private static final String SERVICES_KEY = "services";
    private static final String STATUS_KEY = "status";
	private static final Object SCHEME_KEY = "schemeName";

	public Appliance parse(Object applianceEncoded){
		if(applianceEncoded instanceof Map<?,?>){
			Map<?,?> applianceSource = (Map<?, ?>) applianceEncoded;
			
	        Appliance appliance = new Appliance();
	        parseAppliance(applianceSource, appliance);
	        parseVisual(applianceSource, appliance);
	        parseServices(applianceSource, appliance);
	        parseStatus(applianceSource, appliance);
	        
	        return appliance;
		}
		return null;
    }

	/**
	 * Realiza o parse da estrutura básica do appliance.
	 * @param applianceSource os dados do appliance codificados na forma chave-valor.
	 * @param appliance o objeto que será definido com os valores de applianceSource.
	 * */
	private void parseAppliance(final Map<?, ?> applianceSource, final Appliance appliance) {
		if(applianceSource.containsKey(ID_KEY)){
            appliance.setId(((Number) applianceSource.get(ID_KEY)).intValue());
        }
        
        if(applianceSource.containsKey(TYPE_KEY)){
            appliance.setType((String) applianceSource.get(TYPE_KEY));
        }
        
        if(applianceSource.containsKey(REG_TIME_KEY)){
            appliance.setRegTime((String) applianceSource.get(REG_TIME_KEY));
        }
        
        if(applianceSource.containsKey(VALIDATED_KEY)){
        	Object validated = applianceSource.get(VALIDATED_KEY);
        	if(validated instanceof Number){
        		appliance.setValidated(((Number) validated).intValue());
        	}
        	else if(validated instanceof Boolean){
        		appliance.setValidated(((Boolean) validated));
        	}
        }
        
        if(applianceSource.containsKey(CONNECTED_KEY)){
        	Object connected = applianceSource.get(CONNECTED_KEY);
        	if(connected instanceof Number){
        		appliance.setConnected(((Number) connected).intValue());
        	}
        	else if(connected instanceof Boolean){
        		appliance.setConnected(((Boolean) connected));
        	}
        }
        
        if(applianceSource.containsKey(SCHEME_KEY)){
        	Object schemeName = applianceSource.get(SCHEME_KEY);
        	appliance.setSchemeName((String)schemeName);
        }
	}

	private void parseVisual(Map<?, ?> applianceSource, Appliance appliance) {
    	ViewDefinition view = new ViewDefinition();
    	//Valores default
    	view.setLabel(appliance.getType());
    	view.setNameImage(appliance.getType() + ".png");
    	
		if(applianceSource.containsKey(VISUAL_KEY)){
        	Map<?,?> visual = (Map<?,?>) applianceSource.get(VISUAL_KEY);
        	String visualLabel = (String)(visual.containsKey(LABEL_KEY) ? visual.get(LABEL_KEY) : "");
        	if(visualLabel != null && !visualLabel.isEmpty()){
        		view.setLabel((String)visual.get(LABEL_KEY)); 
        	}
        	
        	if(visual.containsKey(IMAGE_KEY) && visual.get(IMAGE_KEY) != null){
        		String imagePath = (String) visual.get(IMAGE_KEY);
    			view.setNameImage(imagePath);
        	}
        }
		
    	appliance.setView(view);
	}

	private void parseServices(Map<?, ?> applianceSource, Appliance appliance) {
		if(applianceSource.containsKey(SERVICES_KEY)){
            List<?> services = (List<?>) applianceSource.get(SERVICES_KEY);
            if(!services.isEmpty()){
                ServiceParser parser = new ServiceParser();
                for(Object encodedService : services){
                	ApplianceService service = parser.parse((Map<?, ?>) encodedService);
                	
                	if(service.getView().getNameImage() == null 
                			|| service.getView().getNameImage().isEmpty())
                	{
	                	ViewDefinition servView = service.getView();
	                	servView.setNameImage(appliance.getView().getNameImage());
	                	service.setView(servView);
                	}
                    appliance.addService(service);
                }
            }
        }
	}

	private void parseStatus(Map<?, ?> applianceSource, Appliance appliance) {
		if(applianceSource.containsKey(STATUS_KEY)){
            List<?> status = (List<?>) applianceSource.get(STATUS_KEY);
            if(!status.isEmpty()){
                StatusParser parser = new StatusParser();
                for(Object encodedStatus : status){
                    appliance.addStatus(parser.parse((Map<?, ?>) encodedStatus));
                }
            }
        }
	}
    
}
