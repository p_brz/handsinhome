package househub.appliances;


public class Parameter {
	private String type;
	ViewDefinition view;

	public enum Parameters {
		NULL, INT, STRING, BOOLEAN
	}

	public Parameter() {
		this(Parameters.NULL, null);
	}

	public Parameter(Parameters parameters, ViewDefinition view) {
		this.type = parameters.toString();
		this.view = view;
	}

	public ViewDefinition getView() {
		return view;
	}

	public void setView(ViewDefinition view) {
		this.view = view;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String name) {
		this.type = name;
	}

}