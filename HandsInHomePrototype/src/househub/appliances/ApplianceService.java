package househub.appliances;

import househub.appliances.ViewDefinition.ViewComponents;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ApplianceService {
	private String id;
	private ViewDefinition view;
	private List<Parameter> parameters;
	
	public ApplianceService(String name, String nameImage) {
		this(new ViewDefinition(ViewComponents.BUTTON, name,nameImage), null);		
	}
	
	public ApplianceService(ViewDefinition view) {
		this(view, null);
	}

	public ApplianceService(ViewDefinition view, List<Parameter> parameters) {
		this.view = new ViewDefinition(view.getType(), view.getLabel(), view.getNameImage());
		this.parameters = new LinkedList<Parameter>();
		if(parameters!=null){
			this.parameters.addAll(parameters);
		}
	}

	public ApplianceService() {
		this("", "");
	}

	public String getId() {
		return id;
	}
	public String getLabel(){
		if(view != null){
			return view.getLabel();
		}
		return null;
	}
	
	public ViewDefinition getView() {
		return view;
	}

	public void setId(String id) {
		this.id = id;
	}
	public void setLabel(String name){
		if(view == null){
			view = new ViewDefinition();
		}
		view.setLabel(name);
	}
	public void setView(ViewDefinition view) {
		this.view = view;
	}

	public void addParameter(Parameter parameter) {
		this.parameters.add(parameter);
	}
	public void removeParameter(Parameter parameter) {
		this.parameters.remove(parameter);
	}

	public List<Parameter> getParameters() {
		return Collections.unmodifiableList(parameters);
	}
	

}