package househub.appliances;

import househub.appliances.ViewDefinition.ViewComponents;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Appliance {
	private int id;
	private String type;
	//FIXME: modificar para DateTime
    private String regTime;
    private boolean validated;
    private boolean connected;
    private String schemeName;
	private final ViewDefinition view;
	private final List<ApplianceService> services;
	private final List<ApplianceStatus> status;
	private final List<Appliance> applianceChildren;
	
	// Construtores
	public Appliance() {
		this(ViewComponents.BUTTON, "");
	}

	public Appliance(ViewComponents type, String nodeName) {
		this(type, nodeName, null);
	}

	public Appliance(String nameNode, String nameImage) {
		this(ViewComponents.BUTTON, nameNode, nameImage);
	}

	public Appliance(ViewComponents type, String nodeName, String nameImage) {
		this(new ViewDefinition(type, nodeName, nameImage));
	}

	public Appliance(ViewDefinition viewDefinition) {
		this.view = new ViewDefinition();
		if(viewDefinition != null){
			this.view.set(viewDefinition);
		}
		services = new LinkedList<ApplianceService>();
		status = new LinkedList<ApplianceStatus>();
		applianceChildren = new LinkedList<Appliance>();
	}

	@Override
	public Object clone(){
		Appliance clone = new Appliance();
	
		clone.setConnected(this.connected);
		clone.setId(this.id);
		clone.setType(this.type);
		clone.setValidated(this.validated);
		clone.setRegTime(this.regTime);
		clone.setSchemeName(this.schemeName);
		clone.setView(this.view);
		cloneServices(clone);
		cloneStatus(clone);
		cloneChildren(clone);
		
		return clone;
	}

	//TODO: (?) clonar as instâncias  de Appliances, Status e Serviços ?
	
	private void cloneChildren(Appliance clone) {
	    clone.setApplianceChildren(this.applianceChildren);
    }

	private void cloneStatus(Appliance clone) {
	    clone.setStatus(this.status);
    }

	private void cloneServices(Appliance clone) {
	    clone.setServices(this.services);
    }
	

	@Override
	public boolean equals(Object other){

	    if (this == other){
		    return true;
	    }
	    if (other == null || !this.getClass().equals(other.getClass())){
		    return false;
	    }

		Appliance otherApp = (Appliance) other;
		
		boolean isEqual = true;

		isEqual = checkEqual(isEqual, otherApp.getConnected()		, this.getConnected());
		isEqual = checkEqual(isEqual, otherApp.getId()				, this.getId());
		isEqual = checkEqual(isEqual, otherApp.getType()				, this.getType());
		isEqual = checkEqual(isEqual, otherApp.getValidated()		, this.getValidated());
		isEqual = checkEqual(isEqual, otherApp.getRegTime()			, this.getRegTime());
		isEqual = checkEqual(isEqual, otherApp.getSchemeName()		, this.getSchemeName());
		isEqual = checkEqual(isEqual, otherApp.getView()				, this.getView());
		isEqual = checkEqual(isEqual, otherApp.getApplianceChildren(), this.getApplianceChildren());
		isEqual = checkEqual(isEqual, otherApp.getServices()			, this.getServices());
		isEqual = checkEqual(isEqual, otherApp.getStatus()			, this.getStatus());
		
		return isEqual;
	}
	
	private boolean checkEqual(boolean isEqual, Object attrThis, Object attrOther){
	    return (attrThis == null && attrOther == null) || attrThis.equals(attrOther);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
    public int hashCode() { //gerado pelo eclipse
	    final int prime = 31;
	    int result = 1;

		result = incrementHash(prime, result, this.getConnected());
		result = incrementHash(prime, result, this.getId());
		result = incrementHash(prime, result, this.getType());
		result = incrementHash(prime, result, this.getValidated());
		result = incrementHash(prime, result, this.getRegTime());
		result = incrementHash(prime, result, this.getSchemeName());
		result = incrementHash(prime, result, this.getView());
		result = incrementHash(prime, result, this.getApplianceChildren());
		result = incrementHash(prime, result, this.getServices());
		result = incrementHash(prime, result, this.getStatus());
	    return result;
    }
	int incrementHash(final int seed, final int currentResult, Object member){
		return seed * currentResult + (member == null ? 0 : member.hashCode());
	}


	public ViewDefinition getView() {
		return view;
	}

	public void setView(ViewDefinition view) {
		this.view.set(view);
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLabel(){
		if(this.view != null){
			return view.getLabel();
		}
		return null;
	}

	public void setLabel(final String label){
		this.view.setLabel(label);
	}
	
	public List<ApplianceService> getServices() {
		return Collections.unmodifiableList(services);
	}
	public void setStatus(final Collection<ApplianceStatus> status) {
		this.status.clear();
		this.status.addAll(status);
    }
	public void setServices(final Collection<? extends ApplianceService> services) {
	    this.services.clear();
	    this.services.addAll(services);
    }

	public void addService(final ApplianceService service) {
		this.services.add(service);
	}

	public void removeService(final ApplianceService service) {
		this.services.remove(service);
	}

	public List<ApplianceStatus> getStatus(){
		return Collections.unmodifiableList(status);
	}
	
	public void addStatus(final ApplianceStatus status){
		this.status.add(status);
	}
	
	public void removeStatus(final ApplianceStatus status){
		this.status.remove(status);
	}
	
	public String getType() {
		return type;
	}

	public void setType(final String someType) {
		this.type = someType;
	}

	public String getRegTime() {
		return regTime;
	}

	public void setRegTime(String someTime) {
		this.regTime = someTime;
	}

	public boolean getValidated() {
		return validated;
	}

	public void setValidated(Integer validated) {
		this.validated = (validated == 0 ? false : true);
	}
	public void setValidated(Boolean validated) {
		this.validated = validated;
	}

	public boolean getConnected() {
		return connected;
	}

	public void setConnected(final boolean connected) {
		this.connected = connected;
	}
	public void setConnected(final int connected) {
		this.connected = (connected == 0 ? false : true);
	}

	public String getSchemeName() {
		return schemeName;
	}

	public void setSchemeName(final String schemeName) {
		this.schemeName = schemeName;
	}

	public void addApplianceChild(final Appliance appliance){
		if(appliance != null && this.applianceChildren.contains(appliance) == false){
			this.applianceChildren.add(appliance);
		}
	}
	public void removeApplianceChild(final Appliance appliance){
		if(appliance != null){
			this.applianceChildren.remove(appliance);
		}
	}
	
	public List<Appliance> getApplianceChildren(){
		return Collections.unmodifiableList(this.applianceChildren);
	}
	public void setApplianceChildren(final List<Appliance> children) {
	    this.applianceChildren.clear();
	    this.applianceChildren.addAll(children);
    }
}
