package househub.network;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class JsonAnswerParser {

	public RequestAnswer parse(String result) {
		JSONObject jsonAnswer = (JSONObject) JSONValue.parse(result);

		RequestAnswer answer = new RequestAnswer();
		
		answer.setAnswerStatus(((Number)jsonAnswer.get("status")).intValue());
		answer.setMessage((String) jsonAnswer.get("message"));
		answer.setMethodName((String) jsonAnswer.get("method"));
		answer.setContent(jsonAnswer.get("content"));

		return answer;
	}
}
