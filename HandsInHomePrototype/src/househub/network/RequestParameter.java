package househub.network;

public class RequestParameter {

	protected String field;
	protected String value;

	public RequestParameter() {
		this("","");
	}

	public RequestParameter(String field, String value) {
		this.field = field;
		this.value = value;
	}

	public String getField() {
	    return field;
	}

	public void setField(String field) {
	    this.field = field;
	}

	public String getValue() {
	    return value;
	}

	public void setValue(String value) {
	    this.value = value;
	}

}