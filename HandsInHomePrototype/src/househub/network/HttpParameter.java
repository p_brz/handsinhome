/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package househub.network;

/**
 *
 * @author alison
 */
public class HttpParameter extends RequestParameter {
    public HttpParameter(String field, String value ){
        super(field, value);
    }
    
    public String toString(){
    	return this.field + "=" + this.value;
    }
}
