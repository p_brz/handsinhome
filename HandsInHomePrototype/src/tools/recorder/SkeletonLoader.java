package tools.recorder;

import gestures.skeleton.Skeleton;

import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class SkeletonLoader {
	private ObjectInputStream inputStream;

	private static final String errorLoadWithNoStream = "You should first start the Loading by calling startLoad";
	private static final String errorEndWithNoBegin = errorLoadWithNoStream;

	public void startLoad(File file) throws FileNotFoundException, IOException{
		inputStream = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
	}
	public void startLoad(String filePath) throws FileNotFoundException, IOException{
		inputStream = new ObjectInputStream(new BufferedInputStream(new FileInputStream(filePath)));
	}
	
	public Skeleton loadFrame() throws IOException, IllegalStateException, ClassNotFoundException{
		if(this.inputStream != null){
			try{
				return (Skeleton)inputStream.readObject();
			}
			catch(EOFException ex){
				return null;//Terminou arquivo
			}
		}
		else{
			throw new IllegalStateException(errorLoadWithNoStream);
		}
	}
	public void endLoad() throws IOException{
		if(inputStream != null){
			inputStream.close();
			inputStream = null;
		}
		else{
			throw new IllegalStateException(errorEndWithNoBegin);
		}
	}
}
