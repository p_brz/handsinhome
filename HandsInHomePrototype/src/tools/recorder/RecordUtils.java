package tools.recorder;

import gestures.skeleton.Skeleton;

import java.awt.Component;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;

public class RecordUtils {
	static Logger LOG = Logger.getLogger(RecordUserApp.class.getName() + ".LOG");
	private SkeletonRecorder recorder;
	public static final String ERRORFILE_MSG = "Arquivo não pode ser criado, encerrando a aplicação";
	public static final String ERRORFILENOTFOUND_MSG = "Arquivo não pode ser criado, encerrando a aplicação";
	public static final String ERRORFILESTARTRECORD_MSG = "Ocorreu um erro ao iniciar a gravação do arquivo, encerrando a aplicação";
	
//	private boolean startRecord = false;
//	private boolean recording = false;


	Skeleton savingSkeleton;
	private String savePath;
	private boolean hasStarted = false;
	
	public RecordUtils(){
		this.recorder = new SkeletonRecorder();
		
		savingSkeleton = new Skeleton();
	}
	
	public boolean isRecording(){
		return this.isRecording;
	}

//	public void startRecord(Component root){
//		this.savePath = createFile(root).getAbsolutePath();
//		
//		System.out.println(savePath);
//		
//		try {
//			this.recorder.startRecord(savePath);
//			this.isRecording = true;
//			
//		} catch (FileNotFoundException e) {
//			System.err.println(errorFileNotFound);
//			LOG.log(Level.WARNING, errorFileNotFound, e);
//			System.exit(2);
//		} catch (IOException e) {
//			System.err.println(errorFileStartRecord);
//			LOG.log(Level.WARNING, errorFileStartRecord, e);
//			System.exit(3);
//		}		
//	}
	public void startRecord(Component root){
		this.savePath = chooseFile(root, this.makeFilepath()).getAbsolutePath();
		
		if(savePath != null){
			System.out.println("Save Path: " +savePath);
			this.hasStarted = true;
		}	
	}
	


	public void updateSkeleton(Skeleton skel){
		this.savingSkeleton = skel;

		if(hasStarted && !isRecording){
			try {
				this.createFile(this.savePath);
				this.recorder.startRecord(this.savePath);
				this.isRecording = true;
				
			} catch (FileNotFoundException e) {
				System.err.println(ERRORFILENOTFOUND_MSG);
				LOG.log(Level.WARNING, ERRORFILENOTFOUND_MSG, e);
				System.exit(2);
			} catch (IOException e) {
				System.err.println(ERRORFILESTARTRECORD_MSG);
				LOG.log(Level.WARNING, ERRORFILESTARTRECORD_MSG, e);
				System.exit(3);
			}	
		}
		if(isRecording){
			try {
				recorder.saveFrame(this.savingSkeleton);
//				System.out.println("add Skeleton " + this.savingSkeleton);
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	


	public void finishRecord(){
		try {
			this.recorder.endRecord();
			this.isRecording = false;
			this.hasStarted = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public File chooseFile(Component root, String filepath) {
		// create a file chooser
		final JFileChooser fc = new JFileChooser();
		fc.setSelectedFile(new File(filepath));
		// in response to a button click:
		int returnVal = fc.showSaveDialog(root);
		File file = null;
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			file = fc.getSelectedFile();
		}

		return file;
	}
	
	public void setup(){
	}
	
	boolean isRecording = false;
	boolean savedSkeleton = false;

	private boolean createFile(String filepath) {
		return this.createFile(new File(filepath));
	}
	private boolean createFile(File file) {
		if(file != null){
			try {
				this.createRecordsDir();
				file.createNewFile();
				return true;
			} catch (IOException e) {
				return false;
			}
		}
		return false;
	}

	private String makeFilename() {
		Calendar localCalendar = Calendar.getInstance(TimeZone.getDefault());
	     
        int currentDay = localCalendar.get(Calendar.DATE);
        int currentMonth = localCalendar.get(Calendar.MONTH) + 1;
        int currentYear = localCalendar.get(Calendar.YEAR);
       
        int currentHour =  localCalendar.get(Calendar.HOUR);
        int currentMinute = localCalendar.get(Calendar.MINUTE);
        int currentSecond = localCalendar.get(Calendar.SECOND);
	
		return currentYear + "-"+currentMonth + "-"+currentDay + "_"+currentHour+":"+currentMinute+":"+currentSecond + ".skel";
	}

	private String recordsPath(){
		String dirPath = System.getProperty("user.dir");
		return dirPath + File.separator + "skeletonRecords";
	}
	private String makeFilepath(){
		return recordsPath() + File.separator+ this.makeFilename();
	}
	
	private boolean createRecordsDir(){
		File recordsDir = new File(recordsPath());
		if(!recordsDir.exists()){
			return recordsDir.mkdir();
		}
		return false;
	}
}
