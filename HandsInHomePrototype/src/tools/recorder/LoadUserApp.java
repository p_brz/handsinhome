package tools.recorder;

import gestures.skeleton.Skeleton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;

import main.utils.Skeleton2DAppTemplate;
import main.utils.SkeletonDrawer2D;
import processing.core.PApplet;

public class LoadUserApp extends Skeleton2DAppTemplate{	
	private static final long serialVersionUID = 7947423009234522950L;

	public static void main(String args[])  {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", LoadUserApp.class.getName() });
    }

	static Logger LOG = Logger.getLogger(LoadUserApp.class.getName() + ".LOG");
	private SkeletonLoader loader;
	SkeletonDrawer2D skelDrawer;

	static final String errorFileMsg = "Arquivo não pode ser criado, encerrando a aplicação";
	static final String errorFileNotFound = "Arquivo não pode ser criado, encerrando a aplicação";
	static final String errorFileStartRecord = "Ocorreu um erro ao iniciar a gravação do arquivo, encerrando a aplicação";
	
	List<Skeleton> loadedSkeletons;
	
	
	public LoadUserApp(){
		super();
		this.loader = new SkeletonLoader();
		skelDrawer = new SkeletonDrawer2D();
		
		loadedSkeletons = new LinkedList<Skeleton>();
	}
	
	@Override
	public void setup(){
		// set system look and feel 
		 super.setup();
//		super.setupView();
		
		File file = loadFile();
		try {
			this.loader.startLoad(file);
			this.loading = true;
		} catch (FileNotFoundException e) {
			System.err.println(errorFileNotFound);
			LOG.log(Level.WARNING, errorFileNotFound, e);
			System.exit(2);
		} catch (IOException e) {
			System.err.println(errorFileStartRecord);
			LOG.log(Level.WARNING, errorFileStartRecord, e);
			System.exit(3);
		} catch (IllegalStateException e) {
			e.printStackTrace();
			System.exit(4);
		} 
	}
	
	
	@Override
	public void draw(){
////		super.draw();	
//
//		try {
////			if(keyPressed && key == ' '){
////				System.out.println("load!");
////				Skeleton skel = loader.loadFrame();
////				if(skel != null){
////					this.userSkeleton = skel;
////				}
////			}
////			if(){
////				
////			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		this.drawSkeleton(userSkeleton);
		background(220);
		showSkeletonFrame();
	}

	
//	int index = 0;
//	private void showSkeletonFrame() {
//		if(this.loadedSkeletons != null && this.loadedSkeletons.isEmpty() == false){
//			System.out.println("Show Skel Frame " + index);
//			this.drawSkeleton(this.loadedSkeletons.get(index));
//			index = (index + 1) % loadedSkeletons.size();
//		}
//	}
	
	Skeleton loadedSkeleton = null;
private boolean loading;
	
	private void showSkeletonFrame() {
//		if(this.loadedSkeletons != null && this.loadedSkeletons.isEmpty() == false){
//			System.out.println("Show Skel Frame " + index);
//			this.drawSkeleton(this.loadedSkeletons.get(index));
//			index = (index + 1) % loadedSkeletons.size();
//		}

		if(loading){
			Skeleton loaded;
			try {
				loaded = loader.loadFrame();
				if(loaded != null){
					this.loadedSkeleton = loaded;
					System.out.println("Loaded skeleton " + loaded.toString());
				}
				else{
					loader.endLoad();
					this.loading = false;
				}
			} 
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		this.drawSkeleton(loadedSkeleton);
	}
	

	private File loadFile() {
		this.createRecordsDir();
		File file = chooseFile(this.recordsPath());
		if(file != null){
			try {
				file.createNewFile();
			} catch (IOException e) {
				System.err.println(errorFileMsg);
				LOG.log(Level.WARNING, errorFileMsg, e);
				System.exit(1);
			}
		}
		else{
			System.err.println(errorFileMsg);
			System.exit(0);
		}
		return file;
	}
	
	private File chooseFile(String filepath) {
		// create a file chooser
		final JFileChooser fc = new JFileChooser();
		fc.setSelectedFile(new File(filepath));
		// in response to a button click:
		int returnVal = fc.showOpenDialog(this);
		File file = null;
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			file = fc.getSelectedFile();
		}

		return file;
	}

	private String recordsPath(){
		String dirPath = System.getProperty("user.dir");
		return dirPath + File.separator + "skeletonRecords";
	}
	
	private boolean createRecordsDir(){
		File recordsDir = new File(recordsPath());
		if(!recordsDir.exists()){
			return recordsDir.mkdir();
		}
		return false;
	}
}
