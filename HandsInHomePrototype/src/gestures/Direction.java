package gestures;


import java.util.HashMap;
import java.util.Map;

public enum Direction {
	UP, DOWN, RIGHT, LEFT, FRONT, BACK;

	private static Map<Direction, Direction> oppositeMap = createOppositeMap();

	private static Map<Direction, Direction> createOppositeMap() {
		Map<Direction, Direction> opposite = new HashMap<Direction, Direction>();
		opposite.put(LEFT , RIGHT);
		opposite.put(RIGHT, LEFT);
		opposite.put(FRONT, BACK);
		opposite.put(BACK , FRONT);
		opposite.put(UP   , DOWN);
		opposite.put(DOWN , UP);
		
		return opposite;
	}
	
	public Direction getOposite(Direction dir){
		return oppositeMap.get(dir);
	}
	
	public String toString(){
		String name = super.toString();
		    
		return name.substring(0, 1).concat(name.substring(1).toLowerCase());
	}
}