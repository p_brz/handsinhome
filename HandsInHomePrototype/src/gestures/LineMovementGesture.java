package gestures;


import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import gestures.skeleton.SkeletonPoint;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import processing.core.PVector;

public class LineMovementGesture extends AbstractGesture{
	public enum ModeDirections{
		strict,		/** As direções definidas devem ser iguais as direções atuais*/
		permissive; /** As direções definidas devem estar contidas nas direções atuais*/
		
		public boolean isEquivalent(Collection<? extends Direction> expectedDirection, Collection<? extends Direction> currentDirection){
			if(this == strict){
				return currentDirection.containsAll(expectedDirection) && (currentDirection.size() == expectedDirection.size());
			}
			else if(this == permissive){
				return currentDirection.containsAll(expectedDirection);
			}
			return false;
		}
	}
	
	private static double defaultMinMoveLength = 200;
	/** Armazena o tamanho mínimo necessário para iniciar a distinguir um movimento*/
	private static double defaultMinThresholdLength = 10;
	private SkeletonPoint skelPoint;
	private Set<Direction> moveDirections;
	private ModeDirections directionsMode;
	
	/** Armazena o tamanho mínimo (em milimetros) do movimento para que o gesto seja reconhecido.*/
	private double minimumMoveLength;
	
	private PVector initialPos;
	private PVector currentPos;
	
	
	public LineMovementGesture(Direction[] movementDirections, SkeletonPart part){
		this(movementDirections,part, defaultMinMoveLength);
	}
	public LineMovementGesture(Direction[] movementDirections, SkeletonPart part, double minMoveLength){
		this(movementDirections, new SkeletonPoint(part), minMoveLength);
	}
	public LineMovementGesture(Direction[] movementDirections, SkeletonPoint point, double minMoveLength){
		assertDirectionCount(movementDirections);
		
		this.skelPoint = point;
		this.moveDirections = new HashSet<Direction>();
		Collections.addAll(this.moveDirections, movementDirections);
		this.minimumMoveLength = minMoveLength;
		directionsMode = ModeDirections.strict;
	}
	private void assertDirectionCount(Direction[] movementDirections){
		int count = movementDirections.length;
		if(count <=0 || count > 3){
			throw new IllegalArgumentException("Invalid number of directions: " + count);
		}
	}
	
	public ModeDirections getDirectionsMode() {
		return directionsMode;
	}
	public void setDirectionsMode(ModeDirections directionsMode) {
		this.directionsMode = directionsMode;
	}
	
	@Override
	public RecognitionStatus update(Skeleton skeleton) {
		this.currentPos = skelPoint.getPosition(skeleton);
		
		if(this.currentStatus == RecognitionStatus.notRecognized && initialPos == null){
			//Ainda não foi reconhecido -> inicio do movimento
			this.initialPos = currentPos.get();
		}
		else{
			//Ja iniciou movimento
			updateStatus(skeleton);
		}
		
		return this.getCurrentStatus();
	}
	
	private void updateStatus(Skeleton skeleton) {
		Set<Direction> currentMoveDirections = getMovementDirections(skeleton);

		double currentLength = PVector.dist(this.currentPos, initialPos);
		if(this.directionsMode.isEquivalent(this.moveDirections, currentMoveDirections)){
			if(currentLength >= this.minimumMoveLength){
				this.currentStatus = RecognitionStatus.recognized;
			}
			else if(currentLength >= defaultMinThresholdLength){
				this.currentStatus = RecognitionStatus.inProgress;
			}
		}
		else if(currentLength >= defaultMinThresholdLength){
			this.clearStatus();
		}
		
	}
	private Set<Direction> getMovementDirections(Skeleton skeleton){
		PVector dir = getDirection(this.currentPos, this.initialPos);
		PVector[] skeletonAxis = skeleton.getAxis();
		Set<Direction> currentMovementDirections = new HashSet<Direction>();
		currentMovementDirections.addAll(
				DirectionRecognizer.getInstance().getRelativeDirections(dir, skeletonAxis));
		
		return currentMovementDirections;
	}
	
	private PVector getDirection(PVector finalPos, PVector initPos){
		PVector dir = PVector.sub(finalPos, initPos);
		dir.normalize();
		
		return dir;
	}
	
	@Override
	public void clearStatus(){		
		this.currentStatus = RecognitionStatus.notRecognized;
		this.initialPos = null;
		this.currentPos = null;
	}
	
}
