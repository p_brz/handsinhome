package gestures;


public interface HandListener {
	public void handCreated(HandEvent evt);
	public void handUpdated(HandEvent evt);
	public void handDestroyed(HandEvent evt);
}
