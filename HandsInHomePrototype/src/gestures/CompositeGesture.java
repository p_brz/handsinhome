package gestures;


import gestures.skeleton.Skeleton;

import java.util.ArrayList;
import java.util.List;

public class CompositeGesture extends AbstractCompositeGesture{
	public CompositeGesture(){
		super();
	}
	public CompositeGesture(Gesture... gestures) {
		super(gestures);
	}
	protected List<Gesture> instantiateList(){
		return new ArrayList<Gesture>();
	}
 	
	@Override
	public RecognitionStatus update(Skeleton skeleton) {
		RecognitionStatus finalStatus = RecognitionStatus.recognized;
		for(Gesture gesture : this.gestures){
			RecognitionStatus status = gesture.update(skeleton);
			if(finalStatus != RecognitionStatus.notRecognized){
				if(status == RecognitionStatus.notRecognized){
					finalStatus = RecognitionStatus.notRecognized;
				}
				else if(status == RecognitionStatus.inProgress){
					finalStatus = RecognitionStatus.inProgress;
				}
			}
		}
		this.currentStatus = finalStatus;
		return currentStatus;
	}
}
