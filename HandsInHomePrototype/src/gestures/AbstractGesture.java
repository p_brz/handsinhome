package gestures;


import gestures.skeleton.Skeleton;

public abstract class AbstractGesture implements Gesture{

	protected RecognitionStatus currentStatus;
	protected String name;

	public AbstractGesture() {
		this("");
	}
	public AbstractGesture(String gestureName) {
		currentStatus = RecognitionStatus.notRecognized;
		this.name = gestureName;
	}

	@Override
	public RecognitionStatus getCurrentStatus() {
		return currentStatus;
	}

	@Override
	public void clearStatus() {
		currentStatus = RecognitionStatus.notRecognized;
	}

	@Override
	public String getName(){
		return name;
	}
	
	@Override
	public abstract RecognitionStatus update(Skeleton skeleton);
}