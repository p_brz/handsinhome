package gestures.skeleton;

import gestures.skeleton.Skeleton.SkeletonPart;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import processing.core.PVector;
import SimpleOpenNI.SimpleOpenNI;

public class SkeletonUpdater {
	/* *******************Criar mapeamento entre Skel parts e Joints************** */
	private final static Map<SkeletonPart, Integer[]> mapPartsToJoints = makeMap();

	private static Map<SkeletonPart, Integer[]> makeMap() {
		Map<SkeletonPart, Integer[]> map = new HashMap<Skeleton.SkeletonPart, Integer[]>();
		
		put(map, SkeletonPart.Head, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_HEAD);
		put(map, SkeletonPart.Chest, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_TORSO);
		
		put(map, SkeletonPart.RightShoulder, SimpleOpenNI.SKEL_NECK          , SimpleOpenNI.SKEL_RIGHT_SHOULDER);
		put(map, SkeletonPart.RightArm	   , SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_RIGHT_ELBOW);
		put(map, SkeletonPart.RightForearm , SimpleOpenNI.SKEL_RIGHT_ELBOW   , SimpleOpenNI.SKEL_RIGHT_HAND);
		put(map, SkeletonPart.RightThigh   , SimpleOpenNI.SKEL_RIGHT_HIP     , SimpleOpenNI.SKEL_RIGHT_KNEE);
		put(map, SkeletonPart.RightLeg	   , SimpleOpenNI.SKEL_RIGHT_KNEE    , SimpleOpenNI.SKEL_RIGHT_FOOT);
		
		put(map, SkeletonPart.LeftShoulder, SimpleOpenNI.SKEL_NECK         , SimpleOpenNI.SKEL_LEFT_SHOULDER);
		put(map, SkeletonPart.LeftArm     , SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_LEFT_ELBOW);
		put(map, SkeletonPart.LeftForearm , SimpleOpenNI.SKEL_LEFT_ELBOW   , SimpleOpenNI.SKEL_LEFT_HAND);
		put(map, SkeletonPart.LeftThigh   , SimpleOpenNI.SKEL_LEFT_HIP     , SimpleOpenNI.SKEL_LEFT_KNEE);
		put(map, SkeletonPart.LeftLeg     , SimpleOpenNI.SKEL_LEFT_KNEE    , SimpleOpenNI.SKEL_LEFT_FOOT);
		
		//Mãos não tem direção (inicio e fim são no mesmo ponto)
		put(map, SkeletonPart.RightHand, SimpleOpenNI.SKEL_RIGHT_HAND, SimpleOpenNI.SKEL_RIGHT_HAND);
		put(map, SkeletonPart.LeftHand , SimpleOpenNI.SKEL_LEFT_HAND , SimpleOpenNI.SKEL_LEFT_HAND);
		
		return Collections.unmodifiableMap(map);
	}
	
	private static void put(Map<SkeletonPart, Integer[]> mapParts,
			SkeletonPart part, int startJoint, int endJoint) 
	{
		mapParts.put(part, new Integer[]{startJoint, endJoint});
	}
	/* *************************************************************************** */

	SimpleOpenNI openNi;
	private float threshold;
	
	public SkeletonUpdater(SimpleOpenNI openNI){
		this.openNi = openNI;
		threshold = 0;//
	}

	public void updateSkeleton(int userId,Skeleton skeleton){
		for(SkeletonPart part : mapPartsToJoints.keySet()){
			Integer[] joints = mapPartsToJoints.get(part);
			this.updatePart(userId, skeleton, part, joints[0], joints[1]);
		}
		
		//O openNi nao tem uma Joint que represente o fim do abdomen-> calcula o ponto central dos quadris
		this.updateAbdomen(userId, skeleton);
		//A parte que representa a posição e direção do corpo
		this.updateBody(userId, skeleton);
	}

	private void updatePart(int userId, Skeleton skeleton, SkeletonPart part, int startJoint, int endJoint) {

		PVector startPos = new PVector();
		PVector endPos = new PVector();
		
		float confidence =  openNi.getJointPositionSkeleton(userId,startJoint,startPos);
		confidence += openNi.getJointPositionSkeleton(userId, endJoint,endPos);
		confidence = confidence / 2;

		if(confidence >= threshold){
			skeleton.setPart(part, new BodyPart(startPos, endPos));
		}
		else{
			skeleton.setPart(part, null);
		}
	}

	private void updateAbdomen(int userId, Skeleton skeleton) {		
		PVector torso = new PVector();
		PVector abdomen = new PVector();
	    PVector leftHip = new PVector();
	    PVector rightHip = new PVector();

		float confidence = openNi.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_TORSO,torso);
	    confidence 		+= openNi.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_HIP,leftHip);
	    confidence 		+= openNi.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_HIP,rightHip);
	    confidence = confidence / 0.3f;

	    if(confidence >= threshold){
		    //Posição abdomen) é a média dos dois quadris
		    abdomen = PVector.add(leftHip, rightHip);
		    abdomen.div(2);
	    	//Atualizar posições
		    skeleton.setPart(SkeletonPart.Abdomen,  new BodyPart(torso,abdomen));
		    skeleton.setPart(SkeletonPart.RightHip, new BodyPart(abdomen, rightHip));
		    skeleton.setPart(SkeletonPart.LeftHip,  new BodyPart(abdomen, leftHip));
	    }
	}

	/**Pré-requisito: o método deve ser chamado após as outras posições do corpo estarem definidas.*/
	private void updateBody(int userId, Skeleton skeleton) {
		PVector bodyPos = new PVector();
		//utiliza o torso como centro do corpo
		openNi.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_TORSO,bodyPos);
		PVector bodyEnd = PVector.add(bodyPos, skeleton.getFrontDirection());
		
		skeleton.setPart(SkeletonPart.Body, new BodyPart(bodyPos, bodyEnd));
	}
}
