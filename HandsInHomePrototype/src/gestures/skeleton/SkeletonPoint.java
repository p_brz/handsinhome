package gestures.skeleton;

import gestures.skeleton.Skeleton.SkeletonPart;
import processing.core.PVector;


public class SkeletonPoint {
	public enum PartPoint{
		Start, Center, End
	}
	private PartPoint point;
	private SkeletonPart part;

	public SkeletonPoint(SkeletonPart part){
		this(part,PartPoint.Center);
	}
	public SkeletonPoint(SkeletonPart part, PartPoint point){
		this.point = point;
		this.part = part;
	}
	
	public PVector getPosition(Skeleton skeleton){
		BodyPart bodyPart = skeleton.getPart(part);
		
		PVector pos = null;
		
		if(bodyPart != null){
			switch(this.point){
			case Center:
				pos = bodyPart.getCentralPosition();
				break;
			case Start:
				pos = bodyPart.getStartPosition();
				break;
			case End:
				pos = bodyPart.getEndPosition();
				break;
			}
		}
		
		return pos;
	}

	public SkeletonPart getPart(){
		return this.part;
	}
	public PartPoint getPartPoint(){
		return this.point;
	}
}
