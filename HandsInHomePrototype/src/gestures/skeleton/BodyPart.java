package gestures.skeleton;

import java.io.Serializable;

import processing.core.PVector;


//FIXME: extrair dependencia com PVector: utilizar algo que não dependa do processing
/**
 * @author leobrizolara
 *
 */
public class BodyPart implements Serializable, Cloneable{	
	/**
	 * Generated Serial Version UID
	 */
	private static final long serialVersionUID = 1834787267247733911L;
	private PVector startPos;
	private PVector endPos;
	private PVector centralPos;

	public BodyPart(){
		this(new PVector(), new PVector());
	}
	public BodyPart(PVector startPosition, PVector endPosition){
		this.startPos = startPosition;
		this.endPos = endPosition;
		centralPos = new PVector();
		this.updateCentralPos();
	}
//	public PVector getPartPoint(PartPoint partPoint){
//		
//	}
	public PVector getStartPosition() { return startPos;}
	public PVector getEndPosition() { return endPos;}
	public PVector getDirection(){
		PVector dir = PVector.sub(endPos, startPos);
		dir.normalize();
		return dir;
	}
	
	public void setPosition(PVector startPos, PVector endPos){
		this.setStartPosition(startPos);
		this.setEndPosition(endPos);
	}
	public void setStartPosition(PVector startPos) { 
		this.startPos.set(startPos);
		updateCentralPos();
	}
	public void setEndPosition(PVector endPos) { 
		this.endPos.set(endPos);
		updateCentralPos();
	}
	private void updateCentralPos() {
		PVector pos = PVector.sub(endPos, startPos);
		
		float initLength = pos.mag();
		pos.normalize();
		pos.mult(initLength/2);
		pos.add(startPos);
		this.centralPos.set(pos);
	}

	//FIXME: escolher "API" 1 ou 2
	public PVector getCentralPosition(){
		//Cria nova instância para evitar alterações indevidas
		return new PVector(centralPos.x,centralPos.y,centralPos.z);
	}
	public void getCentralPosition(PVector vec){
		if(vec != null){
			vec.set(this.centralPos);
		}
	}
	
	public double getOrientation(PVector origin, PVector target){
		double angle = Math.acos(origin.dot(this.centralPos));
		PVector.cross(origin, centralPos, target);
		
		return angle;
	}
	

	@Override
    public boolean equals(Object obj) {
	    if (this == obj){
		    return true;
	    }
	    if (obj == null || !this.getClass().equals(obj.getClass())){
		    return false;
	    }
	    
	    BodyPart other = (BodyPart) obj;
	    boolean isEquals = true;
	    isEquals = isEquals && checkEquals(startPos, other.startPos);
	    isEquals = isEquals && checkEquals(endPos, other.endPos);
	    isEquals = isEquals && checkEquals(centralPos, other.centralPos);
	    
	    return isEquals;
    }
	private boolean checkEquals(Object someAttr, Object otherAttr) {
	    return (someAttr == null && otherAttr == null) || someAttr.equals(otherAttr);
    }
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
    public int hashCode() { //gerado pelo eclipse
	    final int prime = 31;
	    int result = 1;
	    result = incrementHash(prime, result, startPos);
	    result = incrementHash(prime, result, endPos);
	    result = incrementHash(prime, result, centralPos);
	    return result;
    }
	int incrementHash(final int seed, final int currentResult, Object member){
		return seed * currentResult + (member == null ? 0 : member.hashCode());
	}
	
	@Override
	public Object clone(){
		return new BodyPart(this.startPos, this.endPos);
	}
}
