package gestures;


import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import gestures.skeleton.SkeletonPoint;

import java.util.HashMap;
import java.util.Map;

import processing.core.PMatrix3D;
import processing.core.PVector;

/** Tipo de gesto que leva em consideração a distância de uma parte do corpo em relação a uma outra
 * em algum dos eixos+sentido definido pelo corpo (Direita, Esquerda, Frente, Trás, Acima, Abaixo) ou
 * de forma absoluta (distância total).
 * */
public class DistanceGesture extends AbstractGesture{
	public enum Distance{
		SmallDistance(80), MediumDistance(150), LargeDistance(500); //TODO: considerar valores
		
		private Distance(double distance){
			this.distance = distance;
		}
		
		/** Distância dada em milimetros*/
		private double distance;
		
		public double getDistance(){
			return this.distance;
		}
	}
	

	private static final Map<Direction, Integer> directionToAxisIndex = createMapDirection();
	private static final Map<Direction, Integer> directionToOrientation = createMapOrientation();
	/** Gera um mapa que relaciona uma determinada direção à um índice (inteiro) que permite identificar a qual eixo essa direção
	 * ser refere.
	 * 	@return Um mapa relacionando uma direção ao índice do eixo (0 para x, 1 para y e 2 para z)
	 * */
	private static Map<Direction, Integer> createMapDirection() {
		Map<Direction,Integer> directionToAxis = new HashMap<Direction,Integer>();

		directionToAxis.put(Direction.RIGHT, 0);
		directionToAxis.put(Direction.LEFT, 0);
		directionToAxis.put(Direction.UP, 1);
		directionToAxis.put(Direction.DOWN, 1);
		directionToAxis.put(Direction.FRONT, 2);
		directionToAxis.put(Direction.BACK, 2);
		
		return directionToAxis;
	}

	private static Map<Direction, Integer> createMapOrientation() {
		Map<Direction,Integer> orientationToAxis = new HashMap<Direction,Integer>();

		orientationToAxis.put(Direction.RIGHT, 1);
		orientationToAxis.put(Direction.LEFT, -1);
		orientationToAxis.put(Direction.UP, 1);
		orientationToAxis.put(Direction.DOWN, -1);
		orientationToAxis.put(Direction.FRONT, 1);
		orientationToAxis.put(Direction.BACK, -1);
		
		return orientationToAxis;
	}
	
	private Direction relativeAxis;
	private Double minDistance;
	private Double maxDistance;
	/** Distância será dada entre part e partBase. 
	 * O eixo da direção será dado iniciando em partBase e terminando em part*/
	private SkeletonPoint basePoint;
	private SkeletonPoint endPoint;
	
	public DistanceGesture(SkeletonPoint endPoint, SkeletonPoint basePoint , Direction axis)
	{
		this(endPoint,basePoint, axis, Distance.MediumDistance.getDistance(),null);
	}
	
	public DistanceGesture(SkeletonPart part, SkeletonPart partBase, Direction axis , Double minDistance, Double maxDistance){
		this(new SkeletonPoint(part), new SkeletonPoint(partBase), axis, minDistance,maxDistance);
	}
	
	public DistanceGesture(SkeletonPoint endPoint, SkeletonPoint basePoint, Direction axis, Double minDistance, Double maxDistance)
	{
		this.endPoint = endPoint;
		this.basePoint = basePoint;
		this.relativeAxis = axis;
		this.minDistance = minDistance;
		this.maxDistance = maxDistance;
	}
	
	public Double getMinDistance() {
		return minDistance;
	}

	public Double getMaxDistance() {
		return maxDistance;
	}

	public SkeletonPoint getBasePoint() {
		return basePoint;
	}

	public SkeletonPoint getEndPoint() {
		return endPoint;
	}

	public void setMinDistance(Double minDistance) {
		this.minDistance = minDistance;
	}

	public void setMaxDistance(Double maxDistance) {
		this.maxDistance = maxDistance;
	}

	public void setBasePoint(SkeletonPoint basePoint) {
		this.basePoint = basePoint;
	}

	public void setEndPoint(SkeletonPoint endPoint) {
		this.endPoint = endPoint;
	}

	@Override
	public RecognitionStatus update(Skeleton skeleton) {
		
		double[] distances = calculateRelativeDistance(skeleton, this.endPoint,this.basePoint);
		double axisDir = chooseDirection(distances);
		
		if(checkMinDistance(axisDir) && checkMaxDistance(axisDir))
		{
			this.currentStatus = RecognitionStatus.recognized;
		}
		else{
			this.currentStatus = RecognitionStatus.notRecognized;
		}
		
		return this.getCurrentStatus();
	}
	
	private boolean checkMinDistance(double axisDir) {
		if(minDistance != null){
			int orientation = directionToOrientation.get(this.relativeAxis);
			return (orientation > 0 && axisDir >= this.minDistance) 
					|| (orientation < 0 && -axisDir >= this.minDistance);
		}
		return true;
	}

	private boolean checkMaxDistance(double axisDir) {
		if(maxDistance != null){
			int orientation = directionToOrientation.get(this.relativeAxis);
			return (orientation > 0 && axisDir <= this.maxDistance)
					|| (orientation < 0 && -axisDir <= this.maxDistance);
		}
		return true;
	}

	private double chooseDirection(double[] distances) {
		int index = directionToAxisIndex.get(this.relativeAxis);
		return distances[index];
	}

	public static double[] calculateRelativeDistance(Skeleton skel, SkeletonPoint endPoint, SkeletonPoint basePoint) {		
		return calculateRelativeDistance(skel, endPoint.getPosition(skel), basePoint.getPosition(skel));
	}
	public static double[] calculateRelativeDistance(Skeleton skel, PVector endPoint, PVector basePoint) {
		PMatrix3D rot = DirectionRecognizer.rotationFromTo(skel.getFrontDirection(), PVector.mult(new PVector(0,0,1), -1));
		PVector rotBase = DirectionRecognizer.getInstance().rotateVector(basePoint, rot);
		PVector rotEnd = DirectionRecognizer.getInstance().rotateVector(endPoint, rot);
		
		double[] distances = new double[3];
		
		distances[0] = (rotEnd.x - rotBase.x);
		distances[1] = (rotEnd.y - rotBase.y);
		distances[1] = (rotEnd.z - rotBase.z);
		
		return distances;
	}

}
