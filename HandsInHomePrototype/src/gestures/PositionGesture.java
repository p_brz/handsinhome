package gestures;


import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import gestures.skeleton.SkeletonPoint;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import processing.core.PMatrix3D;
import processing.core.PVector;

public class PositionGesture extends AbstractGesture{
	SkeletonPoint endPoint, basePoint;
	Collection<Position> positions;
	
//	//Absoluta
//	public PositionGesture(Position[] positions,SkeletonPart part){
//		this(positions,part,null);
//	}
	public PositionGesture(Position[] positions,SkeletonPart part, SkeletonPart partRelativeTo){
		this(positions,new SkeletonPoint(part), new SkeletonPoint(partRelativeTo));
	}
	public PositionGesture(Position[] positions,SkeletonPoint endPoint, SkeletonPoint basePoint){
		this.endPoint = new SkeletonPoint(endPoint.getPart(),endPoint.getPartPoint());
		this.basePoint = new SkeletonPoint(basePoint.getPart(),basePoint.getPartPoint());
		currentStatus = RecognitionStatus.notRecognized;

		assertPositionCount(positions.length);
		this.positions = new HashSet<Position>();
		Collections.addAll(this.positions, positions);
	}
	
	/** Garante que o numero de posições seja valido. Caso contrario, lança uma exceção.*/
	private void assertPositionCount(int count){
		if(count <=0 || count > 3){
			throw new IllegalArgumentException("Invalid number of positions: " + count);
		}
	}
	
	@Override
	public RecognitionStatus update(Skeleton skeleton) {
		Collection<Position> currentPositions = getCurrentPositions(skeleton);

		RecognitionStatus newStatus  = RecognitionStatus.recognized; //Assume status otimista
		
		for(Position dir : this.positions){
			if(!currentPositions.contains(dir)){
				newStatus = RecognitionStatus.notRecognized;
				break;
			}
		}
//		RecognitionStatus newStatus = RecognitionStatus.notRecognized;
//		if(this.positions.size() == currentPositions.size()){
//			newStatus = RecognitionStatus.recognized; //Assume status otimista
//			
//			for(Position dir : this.positions){
//				if(!currentPositions.contains(dir)){
//					newStatus = RecognitionStatus.notRecognized;
//					break;
//				}
//			}
//		}
		this.currentStatus = newStatus;
		
		return currentStatus;
	}

	/**Obtem posição relativa entre as partes especificadas*/
	private Collection<Position> getCurrentPositions(Skeleton skeleton){
		return calculateRelativePosition(skeleton,this.endPoint,this.basePoint);
	}
//	/**Obtem posição relativa entre as partes especificadas*/
//	public static Collection<Position> getCurrentPositions(Skeleton skeleton, SkeletonPart part,SkeletonPart partBase){
//		Set<Position> positions = new HashSet<Position>();
//		PVector pos = skeleton.getPart(part).getCentralPosition();
//		PVector posRelative = skeleton.getPart(partBase).getCentralPosition();
////FIXME: pbter posição relativa
//		positions.add(pos.x > posRelative.x ? Position.Right : Position.Left);	
//		positions.add(pos.y > posRelative.y ? Position.Up : Position.Down);	
//		positions.add(pos.z < posRelative.z ? Position.Front : Position.Back);	
//		
//		return positions;
//	}

	/**Obtem posição relativa entre as partes especificadas*/
	public static Collection<Position> calculateRelativePosition(Skeleton skel, SkeletonPoint endPoint, SkeletonPoint basePoint) {
		return calculateRelativePosition(skel, endPoint.getPosition(skel), basePoint.getPosition(skel));
	}
	/**Obtem posição relativa entre as partes especificadas*/
	public static Collection<Position> calculateRelativePosition(Skeleton skel, SkeletonPart part,SkeletonPart partBase) {
		return calculateRelativePosition(skel, skel.getPart(part).getCentralPosition(), skel.getPart(partBase).getCentralPosition());
	}
	/**Obtem posição relativa entre as partes especificadas*/
	public static Collection<Position> calculateRelativePosition(Skeleton skel, PVector endPoint, PVector basePoint) {
		PMatrix3D rot = DirectionRecognizer.rotationFromTo(skel.getFrontDirection(), PVector.mult(new PVector(0,0,1), -1));
		PVector rotBase = DirectionRecognizer.getInstance().rotateVector(basePoint, rot);
		PVector rotEnd = DirectionRecognizer.getInstance().rotateVector(endPoint, rot);
		
		Set<Position> positions = new HashSet<Position>();

		positions.add((rotEnd.x > rotBase.x? Position.Right : Position.Left));
		positions.add((rotEnd.y > rotBase.y? Position.Above : Position.Below));
		positions.add((rotEnd.z > rotBase.z? Position.Back : Position.Front));
		
		return positions;
	}
}
