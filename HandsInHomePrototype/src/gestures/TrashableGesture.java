package gestures;


import gestures.skeleton.Skeleton;

/** Encapsula um gesto e permite descartar um movimento ou gesto após a execução dele.
 * */
public class TrashableGesture extends AbstractGesture {
	private Gesture trashGesture;
	private Gesture validGesture;
	private long waitForThrashTime;
	private long startTime;
	
	public TrashableGesture(Gesture validGesture, Gesture trashGesture){
		this(validGesture, trashGesture, WaitTime.SmallTime.getTime());
	}
	public TrashableGesture(Gesture someValidGesture, Gesture someTrashGesture, long waitTimeForTrash){
		this.trashGesture = someTrashGesture;
		this.validGesture = someValidGesture;
		this.waitForThrashTime = waitTimeForTrash;
	}
	
	@Override
	public RecognitionStatus update(Skeleton skeleton) {
		if(gestureRecognized(skeleton))
		{
			startTime = System.currentTimeMillis();
			this.currentStatus = RecognitionStatus.inProgress;
		}
		else if(isWaitingForTrash())
		{
			updateTrash(skeleton);
		}
		return getCurrentStatus();
	}

	private boolean isWaitingForTrash() {
		return this.getCurrentStatus() == RecognitionStatus.inProgress;
	}

	private boolean gestureRecognized(Skeleton skeleton) {
		return this.validGesture.getCurrentStatus() != RecognitionStatus.recognized
				&& this.validGesture.update(skeleton) == RecognitionStatus.recognized;
	}

	private void updateTrash(Skeleton skeleton) {
		//Tempo esgotou ou gesto lixo foi reconhecido
		if(trashGesture.update(skeleton) == RecognitionStatus.recognized
				|| (System.currentTimeMillis() - startTime) >= waitForThrashTime)
		{
			this.currentStatus = RecognitionStatus.recognized;
		}
	}
	
	@Override
	public void clearStatus(){
		super.clearStatus();
		this.validGesture.clearStatus();
		this.trashGesture.clearStatus();
	}

}
