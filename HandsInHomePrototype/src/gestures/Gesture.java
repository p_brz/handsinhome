package gestures;


import gestures.skeleton.Skeleton;

public interface Gesture {
	/**	RecognitionStatus representa o status de reconhecimento de um gesto.
	 * 		RecognitionStatus.recognized indica que o gesto for reconhecido por completo
	 * 		RecognitionStatus.notRecognized indica que foi detectado um erro no gesto (ou se ele ainda não foi reconhecido) 
	 * 		RecognitionStatus.inProgress indica que o gesto foi reconhecido parcialmente, mas ainda não concluido
	 * */
	public enum RecognitionStatus{notRecognized, recognized, inProgress}
	
	/**Atualiza/inicia o reconhecimento de um gesto.
	 * @param skeleton - o esqueleto do usuário que está realizando o gesto
	 * @return um RecognitionStatus indicando o novo estado de reconhecimento do gesto
	 * */
	public RecognitionStatus update(Skeleton skeleton);
	/**@return um RecognitionStatus indicando o novo estado de reconhecimento do gesto
	 * */
	public RecognitionStatus getCurrentStatus();
	public void clearStatus();
	
	public String getName();
}
