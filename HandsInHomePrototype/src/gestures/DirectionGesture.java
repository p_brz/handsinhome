package gestures;


import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

/** DirectionGesture representa um gesto estático definido por uma direção de uma parte do corpo.
 * 	Essa direção pode ser absoluta ou em relação a uma outra parte do corpo.
 * */
public class DirectionGesture extends AbstractGesture{
	SkeletonPart part;
	SkeletonPart partBase;
	Collection<Direction> directions;

	//Absoluta
	public DirectionGesture(Direction[] directions,SkeletonPart part){
		this(directions,part,null);
	}
	public DirectionGesture(Direction[] directions,SkeletonPart part, SkeletonPart partRelativeTo){
		init(part,partRelativeTo);

		assertDirectionCount(directions.length);
		this.directions = new HashSet<Direction>();
		Collections.addAll(this.directions, directions);
	}
	private void init(SkeletonPart part, SkeletonPart partRelativeTo){
		this.part = part;
		this.partBase = partRelativeTo;
		currentStatus = RecognitionStatus.notRecognized;
	}
	private void assertDirectionCount(int count){
		if(count <=0 || count > 3){
			throw new IllegalArgumentException("Invalid number of directions: " + count);
		}
	}
	
	@Override
	public RecognitionStatus update(Skeleton skeleton) {
		Collection<Direction> currentDirections = getCurrentDirections(skeleton);
		
		RecognitionStatus newStatus = RecognitionStatus.notRecognized;
		if(this.directions.size() == currentDirections.size()){
			newStatus = RecognitionStatus.recognized; //Assume status otimista
			
			for(Direction dir : this.directions){
				if(!currentDirections.contains(dir)){
					newStatus = RecognitionStatus.notRecognized;
					break;
				}
			}
		}
		this.currentStatus = newStatus;
		
		return currentStatus;
	}
	private Collection<Direction> getCurrentDirections(Skeleton skeleton){
		if(this.partBase != null){//direção relativa
			return DirectionRecognizer.getInstance().getPartDirections(skeleton, this.part, this.partBase);
		}
		else{//direção absoluta
			return DirectionRecognizer.getInstance().getPartDirections(skeleton, this.part);
		}
	}
}
