package gestures;


import processing.core.PVector;
//FIXME: substituir uso de PVector por javax.vecmath.Vector3d

public class HandEvent {
	int handId;
	public enum EventType{
		CREATE_HAND, UPDATE_HAND, DESTROY_HAND
	}
	private EventType type;
	private PVector handPosition;
	private PVector screenPosition;
	
	public int getHandId() { return handId;}
	public EventType getType() { return type;}
	public PVector getHandPosition() { return handPosition;}
	public PVector getScreenPosition() { return screenPosition;}
	
	public void setHandId(int handId) {this.handId = handId;}
	public void setType(EventType type) { this.type = type;}
	public void setHandPosition(PVector position) {this.handPosition = position;}
	public void setScreenPosition(PVector position) { this.screenPosition = position;}
	
	public HandEvent(int handId, EventType type, PVector handPosition, PVector screenPosition) {
		this.handId = handId;
		this.type = type;
		this.handPosition = handPosition;
		this.screenPosition = screenPosition;
	}
	
	
}
