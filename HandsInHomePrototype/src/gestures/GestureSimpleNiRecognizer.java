package gestures;


import java.util.Collection;
import java.util.LinkedList;

import processing.core.PApplet;
import processing.core.PVector;
import SimpleOpenNI.SimpleOpenNI;

public class GestureSimpleNiRecognizer {
	private SimpleOpenNI openNi;
	private String trackingHandGesture;
	private Collection<HandListener> handListeners;
	private boolean handsEnabled;
	private boolean isTrackingHands;
	
	String getTrackingHandGesture() {return trackingHandGesture;}
	void setTrackingHandGesture(String gesture) { 
		if(this.handsEnabled && !this.isTrackingHands){
			this.openNi.removeGesture(this.trackingHandGesture);
			this.openNi.addGesture(gesture);
		}
		this.trackingHandGesture = gesture;
	}
	
	public GestureSimpleNiRecognizer(PApplet app){
		this.openNi = new SimpleOpenNI(app);
		init();
	}
	public GestureSimpleNiRecognizer(SimpleOpenNI openNI){
		this.openNi = openNI;
		init();
	}
	private void init(){
		trackingHandGesture = "RaiseHand";
		handListeners = new LinkedList<HandListener>();
		handsEnabled = false;
		isTrackingHands = false;
	}
	


	public void addHandListener(HandListener listener){
		this.handListeners.add(listener);
	}
	public void removeHandListener(HandListener listener){
		this.handListeners.remove(listener);
	}
	
	//FIXME: criar exception especifica
	public void enableHands() throws RuntimeException{
		if(!handsEnabled){
			if(openNi.enableGesture(this) && openNi.enableHands(this)){
				
				handsEnabled = true;
				openNi.addGesture(this.trackingHandGesture);
				
				System.out.println("Enabled gesture!!");
			}
			else{
				throw new RuntimeException("Could not enable Hands!");
			}
		}
	}
	public void disableHands(){
		openNi.stopTrackingAllHands();
		openNi.removeGesture(this.getTrackingHandGesture());
		handsEnabled = false;
		this.isTrackingHands = false;
	}
	

	public void onRecognizeGesture(String strGesture, 
			PVector idPosition, 
			PVector endPosition)
	{
//		if(strGesture == this.trackingHandGesture){
//		}

		openNi.startTrackingHands(endPosition);
		openNi.removeGesture(this.getTrackingHandGesture());
		this.isTrackingHands = true;

		System.out.println("Recognized gesture: " + strGesture);
	}
	
	// -----------------------------------------------------------------
	// hand events
	public void onCreateHands(int handId, PVector position, float time) {
		HandEvent evt = new HandEvent(handId, HandEvent.EventType.CREATE_HAND, position, getScreenPosition(position));
		for(HandListener listener : handListeners){
			listener.handCreated(evt);
		}
	}
	public void onUpdateHands(int handId, PVector position, float time) {
		HandEvent evt = new HandEvent(handId, HandEvent.EventType.UPDATE_HAND, position, getScreenPosition(position));
		for(HandListener listener : handListeners){
			listener.handUpdated(evt);
		}
	}
	public void onDestroyHands(int handId, float time) {
		HandEvent evt = new HandEvent(handId, HandEvent.EventType.DESTROY_HAND, null, null);
		for(HandListener listener : handListeners){
			listener.handDestroyed(evt);
		}
		//Reinicia captura de gesto
		openNi.addGesture(this.getTrackingHandGesture());
	}
	
	private PVector getScreenPosition(PVector position){
		PVector screenPosition = new PVector();
		openNi.convertRealWorldToProjective(position, screenPosition); 
		return screenPosition;
	}
	// -----------------------------------------------------------------
}
