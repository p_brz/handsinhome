package main.prototype;

import gestures.Direction;
import gestures.DirectionGesture;
import gestures.DirectionRecognizer;
import gestures.Gesture;
import gestures.Gesture.RecognitionStatus;
import gestures.skeleton.BodyPart;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import gestures.skeleton.SkeletonUpdater;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import processing.core.PApplet;
import processing.core.PMatrix3D;
import processing.core.PVector;
import SimpleOpenNI.SimpleOpenNI;

//import processing.opengl.*;

@SuppressWarnings("serial")
public class TesteBodyDirections extends PApplet{
    static public void main(String args[]) {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", TesteBodyDirections.class.getName() });
    }

	SimpleOpenNI  context;
	final boolean autoCalib=false;
	
	Skeleton userSkeleton;
	SkeletonUpdater updater;
	List<Skeleton.SkeletonPart> trackedParts;
    
    Gesture rightGesture;
	private float s = 1.0f;
    
	public void setup()
	{	  
		// So eh necessario para processing 1.x
		this.addMouseWheelListener(new MouseWheelListener() {
			public void mouseWheelMoved(MouseWheelEvent mwe) {
//				mouseWheel(mwe.getWheelRotation());
//				s += mwe.getWheelRotation()*0.1f;
				s += mwe.getWheelRotation()*5;
			}
		});
		  
		size(640, 480, P3D);
		context = new SimpleOpenNI(this);

		if (!context.enableDepth()) {
			System.out.println("ERRO!");
			exit();
		}
		context.enableUser(SimpleOpenNI.SKEL_PROFILE_ALL);
		context.setMirror(true);

		updater = new SkeletonUpdater(context);
		userSkeleton = new Skeleton();
		trackedParts = new LinkedList<SkeletonPart>();
		addTrackedPart(trackedParts);
		rightGesture = new DirectionGesture(new Direction[] { Direction.UP, Direction.RIGHT }, 
				SkeletonPart.RightForearm,
				SkeletonPart.RightArm);
	}
	
//	private void addGestures(GestureSimpleNiRecognizer gestureRecognizer) {
//		
//		
//	}

	private void addTrackedPart(List<Skeleton.SkeletonPart> parts){
		parts.add(SkeletonPart.RightArm);
		parts.add(SkeletonPart.RightForearm);
		parts.add(SkeletonPart.LeftArm);
		parts.add(SkeletonPart.LeftForearm);
		parts.add(SkeletonPart.Head);
		parts.add(SkeletonPart.RightArm);
	}

	public void draw() {
		// update the cam
		context.update();
		background(255);
		translate(width / 2, height / 2, 0);
		rotateX(radians(180));
//		translate(0, 0, -500);
		translate(0, 0, s);
//		scale(s);
		box(50);
//		// draw depthImageMap
//		image(context.depthImage(), 0, 0);

		// draw the skeleton if it's available
		int[] userList = context.getUsers();
		for (int i = 0; i < userList.length; i++) {
			if (context.isTrackingSkeleton(userList[i])) {

				updater.updateSkeleton(userList[0], userSkeleton);

				if (rightGesture.update(userSkeleton) == RecognitionStatus.recognized) {
					stroke(0, 255, 0);
				} else {
					stroke(255, 0, 0);
				}

				for (SkeletonPart part : trackedParts) {
					showDirection(part);
				}

////				drawRelativeAxisToZ(userSkeleton, SkeletonPart.RightArm);
////				drawRelativeAxisToZ(userSkeleton, SkeletonPart.RightForearm);
//				drawRelativeAxisToBody(userSkeleton, SkeletonPart.RightArm);
//				drawRelativeAxisToBody(userSkeleton, SkeletonPart.RightForearm);
				drawRelativeAxisToBody(userSkeleton, SkeletonPart.RightForearm, SkeletonPart.RightArm);

				drawBodyDirections(userSkeleton);
			}
		}
	}

	private void drawBodyDirections(Skeleton skeleton) {
		PVector bodyPos = skeleton.getPosition();
		PVector frontDir = skeleton.getFrontDirection();
		PVector rightDir = skeleton.getRightDirection();
		PVector upDir 	 = skeleton.getUpDirection();
		
		pushMatrix();
		pushStyle();
			translate(bodyPos.x,bodyPos.y,bodyPos.z);
			strokeWeight(5);
			drawVector(rightDir, 200, this.color(255,0,0));
			drawVector(upDir   , 200, this.color(0,255,0));
			drawVector(frontDir, 200, this.color(0,0,255));
		popStyle();
		popMatrix();
	}

//	private void drawRelativeAxisToBody(Skeleton skeleton, SkeletonPart skelPartBase){
//		  BodyPart partBase   = skeleton.getPart(skelPartBase);
//		  
//		  PVector bodyFrontDir = skeleton.getFrontDirection();
////		  bodyFrontDir.mult(-1);
//
////		  PMatrix3D rot = DirectionRecognizer.rotationFromTo(partBase.getDirection(), bodyFrontDir);
//		  PMatrix3D rot = DirectionRecognizer.rotationFromTo(bodyFrontDir, partBase.getDirection());
//		  drawAxis(partBase.getStartPosition(), rot);		  
//	}
	private void drawRelativeAxisToBody(Skeleton skeleton, SkeletonPart skelPart, SkeletonPart skelPartBase){
		  BodyPart part   	  = skeleton.getPart(skelPart);
		  BodyPart partBase   = skeleton.getPart(skelPartBase);
		  
		  PVector bodyFrontDir = skeleton.getFrontDirection();
//		  bodyFrontDir.mult(-1);

//		  PMatrix3D rot = DirectionRecognizer.rotationFromTo(partBase.getDirection(), bodyFrontDir);
		  PMatrix3D rot = DirectionRecognizer.rotationFromTo(bodyFrontDir, partBase.getDirection());
		  drawAxis(part.getStartPosition(), rot);	
		  
		  pushStyle();
			  strokeWeight(5);
			  rot = DirectionRecognizer.rotationFromTo(bodyFrontDir, part.getDirection());
			  drawAxis(part.getStartPosition(), rot, this.color(0,0,0), color(255,255,0),color(0,255,255));		  
		  popStyle();
	}

//	private void drawRelativeAxisToZ(Skeleton skeleton, SkeletonPart skelPart) {
//		BodyPart part = skeleton.getPart(skelPart);
//		PMatrix3D rot = DirectionRecognizer.rotationFromTo(part.getDirection(), new PVector(0,0,-1));
//
//		drawAxis(part.getStartPosition(), rot);
//	}
	
	void drawAxis(PVector start, PMatrix3D rot){
		this.drawAxis(start, rot, this.color(255,0,0), this.color(0,255,0), this.color(0,0,255));
		
	}
	
	void drawAxis(PVector start, PMatrix3D rot, int colorX, int colorY, int colorZ){
		this.pushMatrix();
			this.translate(start.x,  start.y, start.z);
			
			this.applyMatrix(rot);
			stroke(colorX);
			line(0,0,0,200,0,0);
			stroke(colorY);
			line(0,0,0,0,200,0);
			stroke(colorZ);
			line(0,0,0,0,0,200);
		this.popMatrix();
		
	}

	void drawVector(PVector vector, int length, int c) {
		pushStyle();
		stroke(c);
		PVector vec = new PVector();
		vec.set(vector);
		vec.normalize();
		vec.mult(length);
		line(0, 0, 0, vec.x, vec.y, vec.z);
		popStyle();
	}


	private void showDirection(Skeleton.SkeletonPart part) {
		Collection<Direction> directions = DirectionRecognizer.getInstance().getPartDirections(userSkeleton, part);
		String partName = part.toString();
		partName += "[";
		for(Direction dir : directions){
			partName += dir + " ";
		}
		partName += "]";
		
		System.out.println(partName);
		
		drawBodyPart(partName, userSkeleton.getPart(part));
	}
	

	
	public void drawBodyPart(String partName, BodyPart part){
		
		this.pushStyle();
			PVector center = part.getCentralPosition();
			
			drawLimb(part);
			
			pushMatrix();
				translate(center.x,center.y,center.z);
				sphere(25);
				
				fill(255,0,0);
				text(partName, 0,0);
			popMatrix();
		this.popStyle();
	}

	void drawLimb(BodyPart part)
	{
		PVector start = part.getStartPosition();
		PVector end = part.getEndPosition();
		
		line(start.x, start.y,  start.z, end.x, end.y, end.z);
	}
	
//	public void drawBodyPart(String partName, BodyPart part){
//		this.pushStyle();
//			PVector center = new PVector();
//			part.getCentralPosition(center);
//			context.convertRealWorldToProjective(center, center);
//			
//			drawLimb(part);
//			ellipse(center.x,center.y,25,25);
//			
//			fill(255,0,0);
//			text(partName, center.x,center.y);
//		this.popStyle();
//	}
//	private void drawLimb(BodyPart part) {
//		PVector start = new PVector();
//		PVector end = new PVector();
//		context.convertRealWorldToProjective(part.getStartPosition(), start);
//		context.convertRealWorldToProjective(part.getEndPosition(), end);
//		
//		line(start.x,start.y,end.x,end.y);
//	}

	// -----------------------------------------------------------------
	// SimpleOpenNI events

	public void onNewUser(int userId)
	{
	  println("onNewUser - userId: " + userId);
	  println("  start pose detection");
	  
	  if(autoCalib)
	    context.requestCalibrationSkeleton(userId,true);
	  else    
	    context.startPoseDetection("Psi",userId);
	}

	public void onLostUser(int userId)
	{
	  println("onLostUser - userId: " + userId);
	}

	public void onExitUser(int userId)
	{
	  println("onExitUser - userId: " + userId);
	}

	public void onReEnterUser(int userId)
	{
	  println("onReEnterUser - userId: " + userId);
	}

	public void onStartCalibration(int userId)
	{
	  println("onStartCalibration - userId: " + userId);
	}

	public void onEndCalibration(int userId, boolean successfull)
	{
	  println("onEndCalibration - userId: " + userId + ", successfull: " + successfull);
	  
	  if (successfull) 
	  { 
	    println("  User calibrated !!!");
	    context.startTrackingSkeleton(userId); 
	  } 
	  else 
	  { 
	    println("  Failed to calibrate user !!!");
	    println("  Start pose detection");
	    context.startPoseDetection("Psi",userId);
	  }
	}

	public void onStartPose(String pose,int userId)
	{
	  println("onStartPose - userId: " + userId + ", pose: " + pose);
	  println(" stop pose detection");
	  
	  context.stopPoseDetection(userId); 
	  context.requestCalibrationSkeleton(userId, true);
	 
	}

	public void onEndPose(String pose,int userId)
	{
	  println("onEndPose - userId: " + userId + ", pose: " + pose);
	}

}

