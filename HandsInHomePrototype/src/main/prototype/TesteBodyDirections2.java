package main.prototype;
import gestures.Direction;
import gestures.DirectionGesture;
import gestures.DirectionRecognizer;
import gestures.Gesture;
import gestures.Gesture.RecognitionStatus;
import gestures.skeleton.BodyPart;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import gestures.skeleton.SkeletonUpdater;
import main.utils.AppDrawSkeleton3DTemplate;
import processing.core.PMatrix3D;
import processing.core.PVector;

//TODO: (?) substituir isso por testes
@SuppressWarnings("serial")
public class TesteBodyDirections2 extends AppDrawSkeleton3DTemplate{
	Skeleton userSkeleton;
    Gesture rightGesture;

    static final PVector XAXIS = new PVector(1,0,0);
    static final PVector YAXIS = new PVector(0,1,0);
    static final PVector ZAXIS = new PVector(0,0,1);
    
	@Override
	public void setup(){
		super.setup();

		updater = new SkeletonUpdater(context);
		userSkeleton = new Skeleton();
		rightGesture = new DirectionGesture(new Direction[] { Direction.UP, Direction.RIGHT }, 
				SkeletonPart.RightForearm,
				SkeletonPart.RightArm);
	}
	
	@Override
	public void drawUser(int userId){
		super.drawUser(userId);

		if (rightGesture.update(userSkeleton) == RecognitionStatus.recognized) {
			stroke(0, 255, 0);
		} else {
			stroke(255, 0, 0);
		}

		drawRelativeAxisToBody(userSkeleton, SkeletonPart.RightForearm, SkeletonPart.RightArm);
		drawRelativeAxisToBody(userSkeleton, SkeletonPart.LeftForearm, SkeletonPart.LeftArm);
		drawBodyDirections(userSkeleton);
	}

	private void drawBodyDirections(Skeleton skeleton) {
		PVector bodyPos = skeleton.getPosition();
		PVector frontDir = skeleton.getFrontDirection();
		PVector rightDir = skeleton.getRightDirection();
		PVector upDir 	 = skeleton.getUpDirection();
		
		pushMatrix();
		pushStyle();
			translate(bodyPos.x,bodyPos.y,bodyPos.z);
			strokeWeight(5);
			drawVector(rightDir, 200, this.color(255,0,0));
			drawVector(upDir   , 200, this.color(0,255,0));
			drawVector(frontDir, 200, this.color(0,0,255));
		popStyle();
		popMatrix();
	}
	void drawVector(PVector vector, int length, int c) {
		pushStyle();
		stroke(c);
		PVector vec = new PVector();
		vec.set(vector);
		vec.normalize();
		vec.mult(length);
		line(0, 0, 0, vec.x, vec.y, vec.z);
		popStyle();
	}
	
	private void drawRelativeAxisToBody(Skeleton skeleton, SkeletonPart skelPart, SkeletonPart skelPartBase){

		  PVector[] baseAxis = getRelativeAxisToBody(skeleton, skelPartBase);
		  drawAxis(skeleton.getPart(skelPart).getStartPosition(), baseAxis, color(255,0,0), color(0,255,0), color(0,0,255));

		  if(super.frameCount % 10 == 0){
			  
//			  showDirectionsPart(skeleton, skelPart, new PVector[]{XAXIS,YAXIS,ZAXIS}); //Dir absoluta

			  System.out.print("TO BODY: ");
			  showDirectionsPart(skeleton, skelPart, baseAxis); //DirRelativa
			  System.out.print("TO Z: ");
			  showDirectionsPart(skeleton, skelPart, getRelativeAxisToZ(skeleton, skelPartBase)); //DirRelativa a Z
		  }
	}

	private PVector[] getRelativeAxisToBody(Skeleton skeleton, SkeletonPart skelPartBase){
		  BodyPart partBase   = skeleton.getPart(skelPartBase);
		  PVector bodyFrontDir = skeleton.getFrontDirection();

		  PMatrix3D rotBase = DirectionRecognizer.rotationFromTo(bodyFrontDir, partBase.getDirection());
		  PVector[] baseAxis = getRotatedAxis(rotBase);
		  
		  return baseAxis;
	}
	private PVector[] getRelativeAxisToZ(Skeleton skeleton, SkeletonPart skelPartBase){
		  BodyPart partBase   = skeleton.getPart(skelPartBase);

		  PMatrix3D rotBase = DirectionRecognizer.rotationFromTo(ZAXIS, partBase.getDirection());
		  PVector[] baseAxis = getRotatedAxis(rotBase);
		  
		  return baseAxis;
	}

	private void showDirectionsPart(Skeleton skeleton, SkeletonPart skelPart, PVector[] baseAxis) {
		PVector proj = new PVector();
		PVector partDir = skeleton.getPart(skelPart).getDirection();
		proj.x = PVector.dot(partDir, baseAxis[0]);
		proj.y = PVector.dot(partDir, baseAxis[1]);
		proj.z = PVector.dot(partDir, baseAxis[2]);

		String directions = new String();
		
		double threshold = 0.5;
		if(this.abs(proj.x) > threshold){
			directions += " ";
			directions += (proj.x > 0 ? Direction.RIGHT : Direction.LEFT);
		}
		if(this.abs(proj.y) > threshold){
			directions += " ";
			directions += (proj.y > 0 ? Direction.UP : Direction.DOWN);
		}
		if(this.abs(proj.z) > threshold){
			directions += " ";
			directions += (proj.z > 0 ? Direction.FRONT : Direction.BACK);
		}
	
		System.out.println("dir["+skelPart+"] " + directions + " \t[" + proj.x + ", " + proj.y + ", " + proj.z + "]");
	}

//	private void showProjectionBetweenAxis(String part1, String part2, PVector[] baseAxis, PVector[] partAxis) {
//		float xProj = PVector.dot(partAxis[0], baseAxis[0]);
//		float yProj = PVector.dot(partAxis[1], baseAxis[1]);
//		float zProj = PVector.dot(partAxis[2], baseAxis[2]);
//	
//		System.out.println("proj["+part1+","+part2+"] " +" X: " + xProj + " Y: " + yProj + " Z: " + zProj );
//	
//	}	
//
//	private void showAnglesBetweenAxis(PVector[] baseAxis, PVector[] partAxis) {
//		float xAngle = degrees(PVector.angleBetween(baseAxis[0], partAxis[0]));
//		float yAngle = degrees(PVector.angleBetween(baseAxis[1], partAxis[1]));
//		float zAngle = degrees(PVector.angleBetween(baseAxis[2], partAxis[2]));
//	
//		System.out.println("X: " + xAngle + " Y: " + yAngle + " Z: " + zAngle);
//	}

	PVector[] getRotatedAxis(PMatrix3D rot){
		PVector[] axis = new PVector[3];
		axis[0] = rotateVector(XAXIS, rot);
		axis[1] = rotateVector(YAXIS, rot);
		axis[2] = rotateVector(ZAXIS, rot);
		
		return axis;
	}
	
	PVector rotateVector(PVector vec, PMatrix3D rot){
		PVector rotatedVector = new PVector();
		
		rot.mult(vec, rotatedVector);
		
		return rotatedVector;
	}
	
	void drawAxis(PVector start, PMatrix3D rot){
		this.drawAxis(start, rot, this.color(255,0,0), this.color(0,255,0), this.color(0,0,255));
		
	}
	
	void drawAxis(PVector start, PMatrix3D rot, int colorX, int colorY, int colorZ){
		this.pushMatrix();
			this.translate(start.x,  start.y, start.z);
			
			this.applyMatrix(rot);
			stroke(colorX);
			line(0,0,0,200,0,0);
			stroke(colorY);
			line(0,0,0,0,200,0);
			stroke(colorZ);
			line(0,0,0,0,0,200);
		this.popMatrix();
	}
	
	void drawAxis(PVector loc, PVector[] axis, int colorX, int colorY, int colorZ){
		this.pushMatrix();
			this.translate(loc.x,  loc.y, loc.z);

			drawVector(axis[0], 200, colorX);
			drawVector(axis[1], 200, colorY);
			drawVector(axis[2], 200, colorZ);
		this.popMatrix();
	}
}
