package main.prototype;


import gestures.Direction;
import gestures.skeleton.BodyPart;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import gestures.skeleton.SkeletonUpdater;
import processing.core.PApplet;
import processing.core.PVector;
import SimpleOpenNI.SimpleOpenNI;

@SuppressWarnings("serial")
public class ShowSkeletonParts extends PApplet{

	SimpleOpenNI  context;
	final boolean       autoCalib=false;
	Skeleton userSkeleton;
	SkeletonUpdater skelUpdater;
	
    static public void main(String args[]) {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", ShowSkeletonParts.class.getName() });
    }
    
	public void setup()
	{
	  
	  size(640,480); 
	  context = new SimpleOpenNI(this);
	   
	  // enable depthMap generation 
	//  if(context.enableDepth() == false)
	//  {
//	     println("Can't open the depthMap, maybe the camera is not connected!"); 
//	     exit();
//	     return;
	//  }
	  context.enableDepth();
	  // enable skeleton generation for all joints
	  context.enableUser(SimpleOpenNI.SKEL_PROFILE_ALL);
//	  context.enableUser(SimpleOpenNI.SKEL_PROFILE_UPPER);
	  context.setMirror(true);
	 

	  stroke(0,0,255);
	  strokeWeight(3);
	  smooth();
	//  size(context.depthWidth(), context.depthHeight()); 
	  userSkeleton = new Skeleton();
	  skelUpdater = new SkeletonUpdater(this.context);
	}

	public void draw()
	{
		background(200);
	  // update the cam
	  context.update();
	  
	  // draw depthImageMap
	  //image(context.depthImage(),0,0);
	  
	  // draw the skeleton if it's available
	  int[] userList = context.getUsers();
//	  for(int i=0;i<userList.length;i++)
//	  {
//	    if(context.isTrackingSkeleton(userList[i])){
//	      //drawSkeleton(userList[i]);
//	      showPartsDirection(userList[i]);
//	    }
//	  } 
	  if(userList.length>0)
	  {
	    if(context.isTrackingSkeleton(userList[0])){
	      skelUpdater.updateSkeleton(userList[0], userSkeleton);
	      for(SkeletonPart part : SkeletonPart.values()){
	    	  showPart(part.toString(),userSkeleton.getPart(part));
	      }
	    }
	  }    
	}

	void correctAxis(){
	  translate(width/2, height/2, 0);
	  rotateX(radians(180));
	}

	void drawAxis(){
	  PVector xAxis = new PVector(100,0,0);
	  PVector yAxis = new PVector(100,0,0);
	  PVector zAxis = new PVector(0,0,100);
	  context.convertRealWorldToProjective(xAxis, xAxis);
	  context.convertRealWorldToProjective(yAxis, yAxis);
	  context.convertRealWorldToProjective(zAxis, zAxis);
	  pushMatrix();
	  translate(width/2,height/2);
	  pushStyle();
	    stroke(255,0,0);
	    line(0,0,xAxis.x,xAxis.y);
	    stroke(0,255,0);
	    line(0,0,yAxis.x,yAxis.y);
	    stroke(0,0,255);
	    line(0,0,zAxis.x,zAxis.y);
	  popStyle();
	  popMatrix();
	}
	

	public void drawBodyPart(BodyPart part,String partName){

		PVector center = new PVector();
		part.getCentralPosition(center);
		//translate(center.x,center.y,center.z);
		context.convertRealWorldToProjective(center, center);
		text(partName, center.x,center.y);
	}
	void showPart(String partName, BodyPart part){
		Direction directions[] = getDirection(part.getStartPosition(), part.getEndPosition());
		PVector centralPoint = part.getCentralPosition();
		context.convertRealWorldToProjective(centralPoint, centralPoint);
		
		//Desenhar texto no ponto central
		String text = partName;
		for(Direction dir : directions){
			text+= ", " + dir.toString();
		}

		PVector startPos = new PVector();
		PVector endPos = new PVector();
		context.convertRealWorldToProjective(part.getStartPosition(), startPos);
		context.convertRealWorldToProjective(part.getEndPosition(), endPos);

		
		pushStyle();
			fill(0,0,255);
			line(startPos.x, startPos.y, endPos.x, endPos.y);
			
			fill(0,255,0);
			ellipse(startPos.x, startPos.y,30,30);
			ellipse(endPos.x, endPos.y,30,30);
			
			fill(255,0,0);
			text(text, centralPoint.x, centralPoint.y);

		popStyle();
	}

	Direction[] getDirection(PVector start, PVector end){
	  Direction directions[] = new Direction[3];
	  PVector vecDir = PVector.sub(end,start);
	  //vecDir.normalize();
	  
	  //println("vecDir: " + vecDir);
	  
	  //  esq < X < dir.
	  directions[0] = (vecDir.x > 0 ? Direction.RIGHT : Direction.LEFT);
	  //  baixo < Y < cima.
	  directions[1] = (vecDir.y > 0 ? Direction.UP : Direction.DOWN);
	  //  frente > Z > fre.
	  directions[2] = (vecDir.z < 0 ? Direction.FRONT : Direction.BACK);
	  
	  return directions;
	}

	// draw the skeleton with the selected joints
	void drawSkeleton(int userId)
	{
	  // to get the 3d joint data
	  /*
	  PVector jointPos = new PVector();
	  context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_NECK,jointPos);
	  println(jointPos);
	  */
	  
//	  context.drawLimb(userId, SimpleOpenNI.SKEL_HEAD, SimpleOpenNI.SKEL_NECK);
//
//	  context.drawLimb(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_LEFT_SHOULDER);
//	  context.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_LEFT_ELBOW);
//	  context.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_ELBOW, SimpleOpenNI.SKEL_LEFT_HAND);
//
//	  context.drawLimb(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_RIGHT_SHOULDER);
//	  context.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_RIGHT_ELBOW);
//	  context.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_ELBOW, SimpleOpenNI.SKEL_RIGHT_HAND);
//
//	  context.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_TORSO);
//	  context.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_TORSO);
//
//	  context.drawLimb(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_LEFT_HIP);
//	  context.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_HIP, SimpleOpenNI.SKEL_LEFT_KNEE);
//	  context.drawLimb(userId, SimpleOpenNI.SKEL_LEFT_KNEE, SimpleOpenNI.SKEL_LEFT_FOOT);
//
//	  context.drawLimb(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_RIGHT_HIP);
//	  context.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_HIP, SimpleOpenNI.SKEL_RIGHT_KNEE);
//	  context.drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_KNEE, SimpleOpenNI.SKEL_RIGHT_FOOT);  
	}

	// -----------------------------------------------------------------
	// SimpleOpenNI events

	public void onNewUser(int userId)
	{
	  println("onNewUser - userId: " + userId);
	  println("  start pose detection");
	  
	  if(autoCalib)
	    context.requestCalibrationSkeleton(userId,true);
	  else    
	    context.startPoseDetection("Psi",userId);
	}

	public void onLostUser(int userId)
	{
	  println("onLostUser - userId: " + userId);
	}

	public void onExitUser(int userId)
	{
	  println("onExitUser - userId: " + userId);
	}

	public void onReEnterUser(int userId)
	{
	  println("onReEnterUser - userId: " + userId);
	}

	public void onStartCalibration(int userId)
	{
	  println("onStartCalibration - userId: " + userId);
	}

	public void onEndCalibration(int userId, boolean successfull)
	{
	  println("onEndCalibration - userId: " + userId + ", successfull: " + successfull);
	  
	  if (successfull) 
	  { 
	    println("  User calibrated !!!");
	    context.startTrackingSkeleton(userId); 
	  } 
	  else 
	  { 
	    println("  Failed to calibrate user !!!");
	    println("  Start pose detection");
	    context.startPoseDetection("Psi",userId);
	  }
	}

	public void onStartPose(String pose,int userId)
	{
	  println("onStartPose - userId: " + userId + ", pose: " + pose);
	  println(" stop pose detection");
	  
	  context.stopPoseDetection(userId); 
	  context.requestCalibrationSkeleton(userId, true);
	 
	}

	public void onEndPose(String pose,int userId)
	{
	  println("onEndPose - userId: " + userId + ", pose: " + pose);
	}

}
