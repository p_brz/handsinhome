package main.prototype;

import gestures.Direction;
import gestures.DirectionGesture;
import gestures.DirectionRecognizer;
import gestures.Gesture;
import gestures.Gesture.RecognitionStatus;
import gestures.skeleton.BodyPart;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import gestures.skeleton.SkeletonUpdater;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import processing.core.PApplet;
import processing.core.PMatrix3D;
import processing.core.PVector;
import SimpleOpenNI.SimpleOpenNI;

//import processing.opengl.*;

//TODO: (?) substituir isso por testes
@SuppressWarnings("serial")
public class TesteCalcBodyDirections extends PApplet{
    static public void main(String args[]) {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", TesteCalcBodyDirections.class.getName() });
    }

	SimpleOpenNI  context;
	final boolean autoCalib=false;
	
	Skeleton userSkeleton;
	SkeletonUpdater updater;
	List<Skeleton.SkeletonPart> trackedParts;
    
    Gesture rightGesture;
    
	public void setup()
	{	  
		size(640, 480, P3D);
		context = new SimpleOpenNI(this);

		if (!context.enableDepth()) {
			System.out.println("ERRO!");
			exit();
		}
		context.enableUser(SimpleOpenNI.SKEL_PROFILE_ALL);
		context.setMirror(true);

		updater = new SkeletonUpdater(context);
		userSkeleton = new Skeleton();
		trackedParts = new LinkedList<SkeletonPart>();
		addTrackedPart(trackedParts);
		rightGesture = new DirectionGesture(new Direction[] { Direction.UP, Direction.RIGHT }, 
				SkeletonPart.RightForearm,
				SkeletonPart.RightArm);
	}

	private void addTrackedPart(List<Skeleton.SkeletonPart> parts){
		parts.add(SkeletonPart.RightArm);
		parts.add(SkeletonPart.RightForearm);
		parts.add(SkeletonPart.LeftArm);
		parts.add(SkeletonPart.LeftForearm);
		parts.add(SkeletonPart.Head);
		parts.add(SkeletonPart.RightArm);
	}

	public void draw() {
		// update the cam
		context.update();
		background(255);
		translate(width / 2, height / 2, 0);
		rotateX(radians(180));
		translate(0, 0, -500);
		this.scale(5.0f);
//		// draw depthImageMap
//		image(context.depthImage(), 0, 0);

		// draw the skeleton if it's available
		int[] userList = context.getUsers();
		for (int i = 0; i < userList.length; i++) {
			if (context.isTrackingSkeleton(userList[i])) {
				// drawSkeleton(userList[i]);
				// showPartsDirection(userList[i]);

				updater.updateSkeleton(userList[0], userSkeleton);

				if (rightGesture.update(userSkeleton) == RecognitionStatus.recognized) {
					stroke(0, 255, 0);
				} else {
					stroke(255, 0, 0);
				}

				for (SkeletonPart part : trackedParts) {
					showDirection(part);
				}

//				drawAxis(userSkeleton, SkeletonPart.RightArm);
//				drawAxis(userSkeleton, SkeletonPart.RightForearm);
				Collection<Direction> directions = this.getPartDirections(userSkeleton, SkeletonPart.RightForearm, SkeletonPart.RightArm);
				System.out.println("Dir from " + SkeletonPart.RightForearm + " to " + SkeletonPart.RightArm + ": " + directions);
				
				drawRelativeAxis(userSkeleton, SkeletonPart.RightForearm,SkeletonPart.RightArm);
				
				drawBodyDirections(userSkeleton);
			}
		}
	}

	private final float minThreshold = 0.4f;
	public PVector getRelativeDir(Skeleton skeleton, SkeletonPart skelPart, SkeletonPart skelPartReference) {
		  PVector bodyFrontDir = skeleton.getFrontDirection();
		  PVector referenceDir = skeleton.getPart(skelPartReference).getDirection();
		  
		  PMatrix3D rotToFront = rotationFromTo(referenceDir, bodyFrontDir);
		  		  
		  PVector relativeDir = new PVector();
		  rotToFront.mult( skeleton.getPart(skelPart).getDirection(), relativeDir);
		  
		  return relativeDir;
	}
	public Collection<Direction> getDirections(PVector dir, PVector up, PVector front, PVector right) {
		Set<Direction> directions = new HashSet<Direction>();

		float horProjection = dir.dot(right);
		float vertProjection = dir.dot(up);
		float frontProjection = dir.dot(front);

		// X: oeste < 0 < leste.
		if (Math.abs(horProjection) > this.minThreshold) {
			directions.add(horProjection > 0 ? Direction.RIGHT : Direction.LEFT);
		}
		// Y: sul < 0 < norte.
		if (Math.abs(vertProjection) > this.minThreshold) {
			directions.add(vertProjection > 0 ? Direction.UP
					: Direction.DOWN);
		}
		// Z:  tras < 0 < frente.
		if (Math.abs(frontProjection) > this.minThreshold) {
			directions.add(frontProjection > 0 ? Direction.FRONT : Direction.BACK);
		}

		return directions;
	}
	public Collection<Direction> getPartDirections(Skeleton skeleton, SkeletonPart skelPart, SkeletonPart skelPartReference) {
		PVector relativeDir = this.getRelativeDir(skeleton, skelPart, skelPartReference);
		
		return getDirections(relativeDir, 
				skeleton.getUpDirection(), 
				skeleton.getFrontDirection(), 
				skeleton.getRightDirection());
	}
	
	public static PMatrix3D rotationFromTo(PVector dirFrom, PVector dirTo){
		//Obter angulo entre vetores
		float angle = PVector.angleBetween(dirFrom,dirTo);
		//Obter vetor normal
		PVector vecNormal = new PVector();
		PVector.cross(dirFrom,dirTo,vecNormal);
		vecNormal.normalize();
		  
		//Criar matriz de rotacao
		PMatrix3D mRot = new PMatrix3D();
		mRot.rotate(angle,vecNormal.x,vecNormal.y,vecNormal.z);//Rotaciona sobre eixo da normal
		  
		return mRot;
	}
	
	private void drawBodyDirections(Skeleton skeleton) {
		PVector bodyPos = skeleton.getPosition();
		PVector frontDir = skeleton.getFrontDirection();
		PVector rightDir = skeleton.getRightDirection();
		PVector upDir 	 = skeleton.getUpDirection();
		
		pushMatrix();
		pushStyle();
			translate(bodyPos.x,bodyPos.y,bodyPos.z);
			strokeWeight(5);
			drawVector(rightDir, 200, this.color(255,0,0));
			drawVector(upDir   , 200, this.color(0,255,0));
			drawVector(frontDir, 200, this.color(0,0,255));
		popStyle();
		popMatrix();
	}

//	private void drawAxis(Skeleton skeleton, SkeletonPart skelPart) {
//		BodyPart part = skeleton.getPart(skelPart);
//		PMatrix3D rot = DirectionRecognizer.rotationFromTo(part.getDirection(), new PVector(0,0,-1));
//
//		this.pushMatrix();
//			PVector start = part.getStartPosition();
//			this.translate(start.x,  start.y, start.z);
//			
//			this.applyMatrix(rot);
//			stroke(255,0,0);
//			line(0,0,0,200,0,0);
//			stroke(0,255,0);
//			line(0,0,0,0,200,0);
//			stroke(0,0,255);
//			line(0,0,0,0,0,200);
//		this.popMatrix();
//	}
	
	private void drawRelativeAxis(Skeleton skeleton, SkeletonPart skelPart, SkeletonPart skelPartReference) {
		
		
		this.pushMatrix();
			PVector start = skeleton.getPart(skelPart).getStartPosition();
			this.translate(start.x,  start.y, start.z);
			
			this.drawVector(skeleton.getRightDirection(), 150, color(255,0,0));
			this.drawVector(skeleton.getUpDirection()	, 150, color(0,255,0));
			this.drawVector(skeleton.getFrontDirection(), 150, color(0,0,255));

			pushStyle();
				this.strokeWeight(5);
//				PVector dir = this.getRelativeDir(skeleton, skelPart, skelPartReference);
				this.drawVector(skeleton.getFrontDirection(), 200, color(0,0,0));
			popStyle();
		this.popMatrix();
	}

	void drawVector(PVector vector, int length, int c) {
		pushStyle();
		stroke(c);
		PVector vec = new PVector();
		vec.set(vector);
		vec.normalize();
		vec.mult(length);
		line(0, 0, 0, vec.x, vec.y, vec.z);
		popStyle();
	}


	private void showDirection(Skeleton.SkeletonPart part) {
		Collection<Direction> directions = DirectionRecognizer.getInstance().getPartDirections(userSkeleton, part);
		String partName = part.toString();
		partName += "[";
		for(Direction dir : directions){
			partName += dir + " ";
		}
		partName += "]";
		
		drawBodyPart(partName, userSkeleton.getPart(part));
	}
	

	
	public void drawBodyPart(String partName, BodyPart part){
		
		this.pushStyle();
			PVector center = part.getCentralPosition();
			
			drawLimb(part);
			
			pushMatrix();
				translate(center.x,center.y,center.z);
				sphere(25);
				
				fill(255,0,0);
				text(partName, 0,0);
			popMatrix();
		this.popStyle();
	}

	void drawLimb(BodyPart part)
	{
		PVector start = part.getStartPosition();
		PVector end = part.getEndPosition();
		
		line(start.x, start.y,  start.z, end.x, end.y, end.z);
	}
	
//	public void drawBodyPart(String partName, BodyPart part){
//		this.pushStyle();
//			PVector center = new PVector();
//			part.getCentralPosition(center);
//			context.convertRealWorldToProjective(center, center);
//			
//			drawLimb(part);
//			ellipse(center.x,center.y,25,25);
//			
//			fill(255,0,0);
//			text(partName, center.x,center.y);
//		this.popStyle();
//	}
//	private void drawLimb(BodyPart part) {
//		PVector start = new PVector();
//		PVector end = new PVector();
//		context.convertRealWorldToProjective(part.getStartPosition(), start);
//		context.convertRealWorldToProjective(part.getEndPosition(), end);
//		
//		line(start.x,start.y,end.x,end.y);
//	}

	// -----------------------------------------------------------------
	// SimpleOpenNI events

	public void onNewUser(int userId)
	{
	  println("onNewUser - userId: " + userId);
	  println("  start pose detection");
	  
	  if(autoCalib)
	    context.requestCalibrationSkeleton(userId,true);
	  else    
	    context.startPoseDetection("Psi",userId);
	}

	public void onLostUser(int userId)
	{
	  println("onLostUser - userId: " + userId);
	}

	public void onExitUser(int userId)
	{
	  println("onExitUser - userId: " + userId);
	}

	public void onReEnterUser(int userId)
	{
	  println("onReEnterUser - userId: " + userId);
	}

	public void onStartCalibration(int userId)
	{
	  println("onStartCalibration - userId: " + userId);
	}

	public void onEndCalibration(int userId, boolean successfull)
	{
	  println("onEndCalibration - userId: " + userId + ", successfull: " + successfull);
	  
	  if (successfull) 
	  { 
	    println("  User calibrated !!!");
	    context.startTrackingSkeleton(userId); 
	  } 
	  else 
	  { 
	    println("  Failed to calibrate user !!!");
	    println("  Start pose detection");
	    context.startPoseDetection("Psi",userId);
	  }
	}

	public void onStartPose(String pose,int userId)
	{
	  println("onStartPose - userId: " + userId + ", pose: " + pose);
	  println(" stop pose detection");
	  
	  context.stopPoseDetection(userId); 
	  context.requestCalibrationSkeleton(userId, true);
	 
	}

	public void onEndPose(String pose,int userId)
	{
	  println("onEndPose - userId: " + userId + ", pose: " + pose);
	}

}

