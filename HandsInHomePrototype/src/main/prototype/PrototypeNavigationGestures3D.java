package main.prototype;

import gestures.CompositeGesture;
import gestures.Direction;
import gestures.DirectionGesture;
import gestures.Gesture;
import gestures.Gesture.RecognitionStatus;
import gestures.HoldGesture;
import gestures.HoldPositionGesture;
import gestures.LineMovementGesture;
import gestures.Position;
import gestures.PositionGesture;
import gestures.SequenceGesture;
import gestures.WaitTime;
import gestures.skeleton.BodyPart;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import gui.screens.HomeObjectScreen;

import java.util.HashMap;
import java.util.Map;

import main.utils.AppDrawSkeleton3DTemplate;
import processing.core.PApplet;
import processing.core.PVector;

//TODO: (?) substituir isso por testes
@SuppressWarnings("serial")
public class PrototypeNavigationGestures3D extends AppDrawSkeleton3DTemplate{

    static public void main(String args[])  {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", PrototypeNavigationGestures3D.class.getName() });
    }

	private HomeObjectScreen favoritesScreen;	

	Map<String, Gesture> gestures;
	String lastGesture;
	
	public PrototypeNavigationGestures3D(){
		favoritesScreen = new HomeObjectScreen("objetos", "objetos.png");
		gestures = new HashMap<String,Gesture>();
		lastGesture = "";
	}
	
	public void setup() {
//		// size(640,480);
//		size(1100, 600, P3D); //800x2,450
//		this.frameRate(15);
//		context = new SimpleOpenNI(this);
//		context.enableDepth();
//
//		// enable skeleton generation for all joints
//		context.enableUser(SimpleOpenNI.SKEL_PROFILE_ALL);
//		// context.enableUser(SimpleOpenNI.SKEL_PROFILE_UPPER);
//		context.setMirror(true);
//
//		background(220);
//
//		stroke(0, 0, 255);
//		strokeWeight(3);
//		smooth();
//
//		userSkeleton = new Skeleton();
//		updater = new SkeletonUpdater(context);

		this.myWidth = 1100;
		this.myHeight = 600;
		super.setup();
		
		addGestures();

//		visibleParts = new LinkedList<Skeleton.SkeletonPart>();
//		addVisiblePart(visibleParts);

		favoritesScreen.setSize(this.width, this.height);
		//FIXME: permitir definir qualquer tamanho para tela e manter proporções (?)
//		favoritesScreen.setSize(1100, 600);//Tamanho "ótimo"
		favoritesScreen.setup();
	}
	
	private void addGestures() {
		gestures.put("PassRight", this.createPassRightLHand());
		gestures.put("PassLeft", this.createPassLeftRHand());
//		gestures.put("PassRightRHand", this.createPassRightRHand());
		gestures.put("PassUp", this.createPassUpRHand());
		gestures.put("PassDown", this.createPassDown());
		gestures.put("PassDownRight", this.createPassDownRHand());
		gestures.put("Click", this.createClick());
		gestures.put("GoBack", this.createGoBack());
		gestures.put("Over", this.createOver());
	}

//	private void addVisiblePart(List<Skeleton.SkeletonPart> parts){
//		parts.add(SkeletonPart.RightArm);
//		parts.add(SkeletonPart.RightForearm);
//		parts.add(SkeletonPart.LeftArm);
//		parts.add(SkeletonPart.LeftForearm);
//		parts.add(SkeletonPart.Head);
//		parts.add(SkeletonPart.Abdomen);
//		
//
//		parts.add(SkeletonPart.RightThigh);
//		parts.add(SkeletonPart.RightHip);
//		parts.add(SkeletonPart.RightLeg);
//		parts.add(SkeletonPart.LeftThigh);
//		parts.add(SkeletonPart.LeftHip);
//		parts.add(SkeletonPart.LeftLeg);
//		parts.add(SkeletonPart.RightArm);
//	}

	//******************************CRIAR GESTOS *********************************************************
	private Gesture createPassLeftRHand() {
		PositionGesture handRightShoulder = new PositionGesture(new Position[]{Position.Right}, SkeletonPart.RightHand,SkeletonPart.RightShoulder);
		HoldGesture holdHandRight = new HoldGesture(handRightShoulder,WaitTime.SmallTime.getTime()/4);
//		HoldGesture holdHandBelow = new HoldGesture(handBelowAbdomen,0);
		
		LineMovementGesture moveLeft = new LineMovementGesture(
				new Direction[]{Direction.LEFT}, SkeletonPart.RightHand,80);

		SequenceGesture sequenceGesture = new SequenceGesture(holdHandRight, moveLeft);
		
		return sequenceGesture;
	}
//	private Gesture createPassRightRHand() {		
//		PositionGesture handLeftShoulder = new PositionGesture(new Position[]{Position.Left}, SkeletonPart.RightHand,SkeletonPart.LeftShoulder);
//		HoldGesture holdHandLeft = new HoldGesture(handLeftShoulder,WaitTime.SmallTime.getTime()/4);
////		HoldGesture holdHandBelow = new HoldGesture(handBelowAbdomen,0);
//		LineMovementGesture moveRight = new LineMovementGesture(
//				new Direction[]{Direction.RIGHT}, SkeletonPart.RightHand,80);
//
//		SequenceGesture sequenceGesture = new SequenceGesture(holdHandLeft, moveRight);
//		
//		return sequenceGesture;
//	}
	private Gesture createPassRightLHand() {
		PositionGesture handLeftShoulder = new PositionGesture(new Position[]{Position.Left}, SkeletonPart.LeftHand,SkeletonPart.LeftShoulder);
		HoldGesture holdHandLeft = new HoldGesture(handLeftShoulder,WaitTime.SmallTime.getTime()/4);
//		HoldGesture holdHandBelow = new HoldGesture(handBelowAbdomen,0);
		
		LineMovementGesture moveRight = new LineMovementGesture(
				new Direction[]{Direction.RIGHT}, SkeletonPart.LeftHand,80);

		SequenceGesture sequenceGesture = new SequenceGesture(holdHandLeft, moveRight);
		
		return sequenceGesture;
	}
	
	
	private Gesture createPassUpRHand() {
		PositionGesture handBelowAbdomen = new PositionGesture(new Position[]{Position.Below}, SkeletonPart.RightHand,SkeletonPart.Abdomen);
		HoldGesture holdHandBelow = new HoldGesture(handBelowAbdomen,WaitTime.SmallTime.getTime()/4);
//		HoldGesture holdHandBelow = new HoldGesture(handBelowAbdomen,0);
		
		LineMovementGesture handMoveUp = new LineMovementGesture(
				new Direction[]{Direction.UP}, SkeletonPart.RightHand);

		SequenceGesture sequenceGesture = new SequenceGesture(holdHandBelow, handMoveUp);
		
		return sequenceGesture;
	}
	private Gesture createPassDownRHand() {
		PositionGesture handAboveHead = new PositionGesture(new Position[]{Position.Above}, SkeletonPart.RightHand,SkeletonPart.Head);
		HoldGesture holdHandAbove = new HoldGesture(handAboveHead,WaitTime.SmallTime.getTime()/4);
//		HoldGesture holdHandAbove = new HoldGesture(handAboveHead,0);
		LineMovementGesture handMoveDown = new LineMovementGesture(
				new Direction[]{Direction.DOWN}, SkeletonPart.RightHand);

		SequenceGesture sequenceGesture = new SequenceGesture(holdHandAbove, handMoveDown);
		
		return sequenceGesture;
	}
//	private Gesture createPassDownRHand() {
//		PositionGesture handUp = new PositionGesture(new Position[]{Position.Up}, SkeletonPart.RightHand,SkeletonPart.Chest);
//		HoldPositionGesture waitALittle = new HoldPositionGesture(SkeletonPart.RightHand, WaitTime.SmallTime.getTime()/2);
//		
//		CompositeGesture waitHandUp = new CompositeGesture(handUp, waitALittle);
//		
//		LineMovementGesture moveHandDown = new LineMovementGesture(
//				new Direction[]{Direction.DOWN}, SkeletonPart.RightHand);
//		
//		SequenceGesture gesture = new SequenceGesture(waitHandUp,moveHandDown);
//		
//		return gesture;
//	}
	private Gesture createPassDown() {
		LineMovementGesture gesture = new LineMovementGesture(
				new Direction[]{Direction.DOWN}, SkeletonPart.LeftHand);
		
		return gesture;
	}

	private Gesture createClick() {
		SequenceGesture gesture = new SequenceGesture();
		LineMovementGesture firstMove = new LineMovementGesture(
				new Direction[]{Direction.FRONT}, SkeletonPart.LeftHand);
		LineMovementGesture secondMove = new LineMovementGesture(
				new Direction[]{Direction.BACK}, SkeletonPart.LeftHand);

		gesture.addGesture(firstMove);
		gesture.addGesture(secondMove);
		
		return gesture;
	}

	private Gesture createGoBack() {
		DirectionGesture dirForearm 
							= new DirectionGesture(new Direction[]{Direction.UP}, SkeletonPart.RightForearm, SkeletonPart.RightArm);
		LineMovementGesture forearmMove 
							= new LineMovementGesture(new Direction[]{Direction.BACK}, SkeletonPart.RightHand);
		
		SequenceGesture gesture = new SequenceGesture();
		gesture.addGesture(dirForearm);
		gesture.addGesture(forearmMove);
		
		return gesture;
	}

	private Gesture createOver() {
		PositionGesture handFrontBody = new PositionGesture(new Position[]{Position.Front}, SkeletonPart.RightHand, SkeletonPart.Body);
		PositionGesture handUpAbdomen = new PositionGesture(new Position[]{Position.Above}, SkeletonPart.RightHand, SkeletonPart.Abdomen);
		PositionGesture handDownHead  = new PositionGesture(new Position[]{Position.Below}, SkeletonPart.RightHand, SkeletonPart.Head);
		HoldPositionGesture handStopped   = new HoldPositionGesture(SkeletonPart.RightHand, WaitTime.MediumTime);
		
		CompositeGesture gesture = new CompositeGesture();
		gesture.addGesture(handFrontBody);
		gesture.addGesture(handUpAbdomen);
		gesture.addGesture(handDownHead);
		gesture.addGesture(handStopped);
		
		return gesture;
	}

	//*********************************************************************************************
	
	public void draw() {
		// Desenha tela
		favoritesScreen.draw(this.g);

		int newHeight = this.height/3;
		translate(0,this.height - newHeight);
		scale(1f/3f);
		
		super.draw();
		if (lastGesture != null && lastGesture != "") {
			text(lastGesture, 50, 50);
		}

		if(this.getSkeletons().size() > 0){
			Skeleton firstSkeleton = (Skeleton) this.getSkeletons().toArray()[0];
			checkGestures(firstSkeleton);
	//		showDirection(SkeletonPart.RightArm, SkeletonPart.Body);
//			for(SkeletonPart part: SkeletonPart.values()){
//				showPart(userSkeleton, part);
//			}
		}
	}
	



//	private void showPart(Skeleton skeleton, SkeletonPart part) {
//		String partName = part.toString();
//		
//		drawBodyPart(partName, skeleton.getPart(part));
//	}
//
//	private void showDirection(Skeleton.SkeletonPart part, Skeleton.SkeletonPart toPart) {
//		Collection<Direction> directions = DirectionRecognizer.getInstance().getPartDirections(userSkeleton, part,toPart);
//		String partName = part.toString();
//		partName += "[";
//		for(Direction dir : directions){
//			partName += dir + " ";
//		}
//		partName += "]";
//		
//		drawBodyPart(partName, userSkeleton.getPart(part));
//	}

	private void checkGestures(Skeleton skeleton) {
		
		for(String str: this.gestures.keySet()){
			if(this.gestures.get(str).getCurrentStatus() != RecognitionStatus.recognized
					&& this.gestures.get(str).update(skeleton) == RecognitionStatus.recognized){
				this.lastGesture = str + System.currentTimeMillis()/1000;
				this.fireGesture(str);
				
//				this.gestures.get(str).clearStatus();

				/*TODO: filtrar gestos a serem reiniciados. 
				 * Pois desta forma não é possível haver gestos concorrentes
				 * Talvez considerar apenas gestos realizados com as mesmas partes*/
				clearAll();
				break;
			}
		}
	}

	private void clearAll() {
		for(Gesture gesture: this.gestures.values()){
			gesture.clearStatus();
		}
	}

	private void fireGesture(String gestureName) {
		System.out.println(gestureName);
		if(gestureName == "PassRight" || gestureName == "PassRightRHand"){
			favoritesScreen.keyPressed((char)CODED, RIGHT);
		}
		else if(gestureName == "PassLeft"){
			favoritesScreen.keyPressed((char)CODED, LEFT);
			
		}
		else if(gestureName == "PassUp" ){
			favoritesScreen.keyPressed((char)CODED, UP);
			System.out.println("Up");
		}
		else if(gestureName == "PassDown" || gestureName == "PassDownRight"){
			favoritesScreen.keyPressed((char)CODED, DOWN);
			System.out.println("Down");
			
		}
		else if(gestureName == "Click"){
			favoritesScreen.mouseClicked();
			favoritesScreen.keyPressed('c',0);
			System.out.println("Click!");
		}
		else if(gestureName == "GoBack"){
			favoritesScreen.keyPressed('b',0);
		}
		else if(gestureName == "Over"){
			
		}
	}

	public void mouseMoved() {
		favoritesScreen.mouseMoved(mouseX, mouseY, pmouseX, pmouseY);
	}

	public void keyPressed() {
		favoritesScreen.keyPressed(key, keyCode);
	}
	

	public void drawBodyPart(String partName, BodyPart part){
		this.pushStyle();
			PVector center = new PVector();
			part.getCentralPosition(center);
			context.convertRealWorldToProjective(center, center);
			
			drawLimb(part);
			ellipse(center.x,center.y,25,25);
			
			fill(255,0,0);
			text(partName, center.x,center.y);
		this.popStyle();
	}

	private void drawLimb(BodyPart part) {
		PVector start = new PVector();
		PVector end = new PVector();
		context.convertRealWorldToProjective(part.getStartPosition(), start);
		context.convertRealWorldToProjective(part.getEndPosition(), end);
		
		line(start.x,start.y,end.x,end.y);
	}

	// -----------------------------------------------------------------
	// SimpleOpenNI events

	public void onNewUser(int userId)
	{
	  println("onNewUser - userId: " + userId);
	  println("  start pose detection");
	  
	  if(autoCalib)
	    context.requestCalibrationSkeleton(userId,true);
	  else    
	    context.startPoseDetection("Psi",userId);
	}

	public void onLostUser(int userId)
	{
	  println("onLostUser - userId: " + userId);
	}

	public void onExitUser(int userId)
	{
	  println("onExitUser - userId: " + userId);
	}

	public void onReEnterUser(int userId)
	{
	  println("onReEnterUser - userId: " + userId);
	}

	public void onStartCalibration(int userId)
	{
	  println("onStartCalibration - userId: " + userId);
	}

	public void onEndCalibration(int userId, boolean successfull)
	{
	  println("onEndCalibration - userId: " + userId + ", successfull: " + successfull);
	  
	  if (successfull) 
	  { 
	    println("  User calibrated !!!");
	    context.startTrackingSkeleton(userId); 
	  } 
	  else 
	  { 
	    println("  Failed to calibrate user !!!");
	    println("  Start pose detection");
	    context.startPoseDetection("Psi",userId);
	  }
	}

	public void onStartPose(String pose,int userId)
	{
	  println("onStartPose - userId: " + userId + ", pose: " + pose);
	  println(" stop pose detection");
	  
	  context.stopPoseDetection(userId); 
	  context.requestCalibrationSkeleton(userId, true);
	 
	}

	public void onEndPose(String pose,int userId)
	{
	  println("onEndPose - userId: " + userId + ", pose: " + pose);
	}

}
