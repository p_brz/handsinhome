package main;

import gestures.Gesture;
import gestures.Gesture.RecognitionStatus;
import gestures.skeleton.BodyPart;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import gestures.skeleton.SkeletonUpdater;
import gui.components.ImageManager;
import gui.screens.HomeObjectScreen;
import gui.screens.HomeScreen;
import gui.screens.Screen;
import gui.screens.ScreenManager;
import househub.HomeManager;
import househub.RemoteHomeManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.UIManager;

import main.utils.GestureFactory;
import processing.core.PApplet;
import processing.core.PVector;
import SimpleOpenNI.SimpleOpenNI;

@SuppressWarnings("serial")
public class GHomeBeta extends PApplet{

    public static void main(String args[])  {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", GHomeBeta.class.getName() });
    }


	SimpleOpenNI  context;
	final boolean autoCalib=true;
	boolean gesturesEnable;
	
	Skeleton userSkeleton;
	SkeletonUpdater updater;
	List<Skeleton.SkeletonPart> visibleParts;

	Map<String, Gesture> gestures;
	String lastGesture;
	
    ScreenManager screenManager;
	
	public GHomeBeta(){
		ImageManager.getInstance().setPApplet(this);
		
		screenManager = new ScreenManager();
		gestures = new HashMap<String,Gesture>();
		lastGesture = "";
	}
	
	public void setup() {
		setupHomeConnection();
		
		setupView();
		
		setupLookAndFeel();
		
		setupGestures();

		setupScreens();
	}

	
	private void setupHomeConnection() {
		HomeManager.setInstance(new RemoteHomeManager());
		HomeManager.getInstance().connect("adm", "123456", "http://localhost/househub/token.php");
	}

	@Override
	public void stop(){
		super.stop();
		RemoteHomeManager.getInstance().disconnect();
	}

	private void setupView() {
		// size(640,480);
		size(1100, 600); //800x2,450
		this.frameRate(15);
		background(220);

		stroke(0, 0, 255);
		strokeWeight(3);
		smooth();
	}
	protected void setupLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void setupScreens() {
		ImageManager.getInstance().setPApplet(this);
		
		this.screenManager = new ScreenManager();
		
		Screen homeObjScreen = new HomeObjectScreen("Objetos da Casa", "objetos.png");
		homeObjScreen.setSize(this.width, this.height);
		this.screenManager.addScreen(homeObjScreen);
		this.screenManager.addScreen(((Screen) ((HomeObjectScreen) homeObjScreen).popUp(homeObjScreen.getTitle() + "popUp")));


//		Screen gestureScreen = new GestureScreen("Adicionar gesto", "objects.png",(this.gesturesEnable ? context : null));
//		gestureScreen.setSize(this.width, this.height);
//		this.screenManager.addScreen(gestureScreen);
		
		
		
		List<Screen> visibleScreens = new ArrayList<Screen>();
		visibleScreens.add(homeObjScreen);
//		visibleScreens.add(gestureScreen);

		Screen home = new HomeScreen(visibleScreens, "gHome", "home.png");
		home.setSize(this.width, this.height);
		this.screenManager.addScreen(home);
		this.screenManager.addScreen(((Screen) ((HomeScreen) home).popUp(home.getTitle() + "popUp")));
		
		this.screenManager.setCurrentScreen(home.getTitle());
	}

	private void setupGestures() {
		context = new SimpleOpenNI(this);
		gesturesEnable = context.enableDepth() && context.enableRGB()
				&& context.enableUser(SimpleOpenNI.SKEL_PROFILE_ALL);
//		gesturesEnable = context.enableDepth() 
//				&& context.enableUser(SimpleOpenNI.SKEL_PROFILE_UPPER);

		if(gesturesEnable){
			context.setMirror(true);
	
			userSkeleton = new Skeleton();
			updater = new SkeletonUpdater(context);
	
			addGestures();
	
			visibleParts = new LinkedList<Skeleton.SkeletonPart>();
			addVisiblePart(visibleParts);
		}
	}
	
	private void addGestures() {
		gestures.put("PassRight", GestureFactory.getInstance().createPassRightLHand());
		gestures.put("PassLeft", GestureFactory.getInstance().createPassLeftRHand());
//		gestures.put("PassRightRHand", GestureFactory.getInstance().createPassRightRHand());
//		gestures.put("PassUp", GestureFactory.getInstance().createPassUpRHand());
//		gestures.put("PassDown", GestureFactory.getInstance().createPassDownLHand());
		gestures.put("PassDownRight", GestureFactory.getInstance().createPassDownRHand());
		gestures.put("Click", GestureFactory.getInstance().createClick());
		gestures.put("GoBack", GestureFactory.getInstance().createGoBack());
		gestures.put("Over", GestureFactory.getInstance().createOver());
//		gestures.put("InitGoBack", GestureFactory.getInstance().createGoBackInitialPose());
//		gestures.put("MoveToBack", new LineMovementGesture(new Direction[]{Direction.BACK}, SkeletonPart.RightHand));
//		gestures.put("DistanceGesture", new DistanceGesture(new SkeletonPoint(SkeletonPart.RightHand)
//										  , new SkeletonPoint(SkeletonPart.RightShoulder,PartPoint.End)
//										  , Direction.RIGHT, 250.0,null));
//		gestures.put("HandLeft", new PositionGesture(new Position[]{Position.Left}
//													, SkeletonPart.RightHand
//													, SkeletonPart.Chest));
	}

	private void addVisiblePart(List<Skeleton.SkeletonPart> parts){
		parts.add(SkeletonPart.RightArm);
		parts.add(SkeletonPart.RightForearm);
		parts.add(SkeletonPart.LeftArm);
		parts.add(SkeletonPart.LeftForearm);
		parts.add(SkeletonPart.Head);
		parts.add(SkeletonPart.Abdomen);
		

		parts.add(SkeletonPart.RightThigh);
		parts.add(SkeletonPart.RightHip);
		parts.add(SkeletonPart.RightLeg);
		parts.add(SkeletonPart.LeftThigh);
		parts.add(SkeletonPart.LeftHip);
		parts.add(SkeletonPart.LeftLeg);
		parts.add(SkeletonPart.RightArm);
	}

	//*********************************************************************************************
	
	int countPoints=0;
	public void draw() {
		// Desenha tela
		screenManager.draw(this.g);
		// Desenha todo o resto
//		translate(favoritesScreen.getWidth(), 0);
		if(gesturesEnable){
			// update the cam
			context.update();
	
			int newHeight = this.height/3;
			translate(0,this.height - newHeight);
			scale(1f/3f);
			
			// draw depthImageMap
//			image(context.depthImage(), 0, 0);
			image(context.rgbImage(), 0, 0);
//			background(0);
			
			pushStyle();
	
			textSize(18);
			// draw the skeleton if it's available
			int[] userList = context.getUsers();
			if(userList.length > 0){
				updateUsers(userList);
			}
//			else{
//				showWaiting();
//			}
			
			popStyle();
		}
	}
	
//	private void showWaiting() {
//		if (this.frameCount / 50 == 0) {
//			this.countPoints = (countPoints + 1) % 5;
//		}
//
//		String points = "";
//		for (int i = 0; i < countPoints; ++i) {
//
//		}
//
//		for (int i = 0; i < countPoints; ++i) {
//			points += ".";
//		}
//
//		this.pushStyle();
//		textSize(40);
//		text("Buscando Usuário" + points, this.width / 2 - 25, this.height / 2);
//		this.popStyle();
//	}

	private void updateUsers(int[] userList){
		for (int i = 0; i < userList.length; i++) {
			if (context.isTrackingSkeleton(userList[i])) {

				updater.updateSkeleton(userList[0], userSkeleton);

				if (lastGesture != null && lastGesture != "") {
					text(lastGesture, 50, 50);
				}

				checkGestures(userSkeleton);
//				showDirection(SkeletonPart.RightArm, SkeletonPart.Body);
				for(SkeletonPart part: SkeletonPart.values()){
					showPart(userSkeleton, part);
				}
			}
		}
	}


	private void showPart(Skeleton skeleton, SkeletonPart part) {
		String partName = part.toString();
		
		drawBodyPart(partName, skeleton.getPart(part));
	}

	private void checkGestures(Skeleton skeleton) {
		
		for(String str: this.gestures.keySet()){
			if(this.gestures.get(str).getCurrentStatus() != RecognitionStatus.recognized
					&& this.gestures.get(str).update(skeleton) == RecognitionStatus.recognized){
				this.lastGesture = str + System.currentTimeMillis()/1000;
				this.fireGesture(str);
				
//				this.gestures.get(str).clearStatus();

				/*TODO: filtrar gestos a serem reiniciados. 
				 * Pois desta forma não é possível haver gestos concorrentes
				 * Talvez considerar apenas gestos realizados com as mesmas partes*/
//				clearOthers(this.gestures.get(str));
				//FIXME: problema com gestos estaticos -> o gesto não para de ser reconhecido
				clearAll();
				break;
			}
		}
	}

	private void clearAll() {
		for(Gesture gesture: this.gestures.values()){
			gesture.clearStatus();
		}
	}
//	private void clearOthers(Gesture gest) {
//		for(Gesture gesture: this.gestures.values()){
//			if(gesture != gest){
//				gesture.clearStatus();
//			}
//		}
//	}

	private void fireGesture(String gestureName) {
		System.out.println(gestureName);
		if(gestureName == "PassRight" || gestureName == "PassRightRHand"){
			screenManager.keyPressed((char)CODED, LEFT);
		}
		else if(gestureName == "PassLeft"){
			screenManager.keyPressed((char)CODED, RIGHT);
			
		}
		else if(gestureName == "PassUp" ){
			screenManager.keyPressed((char)CODED, UP);
			System.out.println("Up");
		}
		else if(gestureName == "PassDown" || gestureName == "PassDownRight"){
			screenManager.keyPressed((char)CODED, DOWN);
			System.out.println("Down");
			
		}
		else if(gestureName == "Click"){
//			screenManager.mouseClicked();
			screenManager.keyPressed('c',0);
			System.out.println("Click!");
		}
		else if(gestureName == "GoBack"){
			screenManager.keyPressed('b',0);
		}
		else if(gestureName == "Over"){
			
		}
	}

	public void mouseMoved() {
		screenManager.mouseMoved(mouseX, mouseY, pmouseX, pmouseY);
	}
	public void keyPressed() {
		screenManager.keyPressed(key, keyCode);
	}
	

	public void drawBodyPart(String partName, BodyPart part){
		this.pushStyle();
			PVector center = new PVector();
			part.getCentralPosition(center);
			context.convertRealWorldToProjective(center, center);
			stroke(255,0,0);
			drawLimb(part);
			this.noStroke();
			fill(0,255,0);
			ellipse(center.x,center.y,10,10);
			
//			fill(255,0,0);
//			text(partName, center.x,center.y);
		this.popStyle();
	}

	private void drawLimb(BodyPart part) {
		PVector start = new PVector();
		PVector end = new PVector();
		context.convertRealWorldToProjective(part.getStartPosition(), start);
		context.convertRealWorldToProjective(part.getEndPosition(), end);
		
		line(start.x,start.y,end.x,end.y);
	}

	// -----------------------------------------------------------------
	// SimpleOpenNI events

	public void onNewUser(int userId)
	{
	  println("onNewUser - userId: " + userId);
	  println("  start pose detection");
	  
	  if(autoCalib)
	    context.requestCalibrationSkeleton(userId,true);
	  else    
	    context.startPoseDetection("Psi",userId);
	}

	public void onLostUser(int userId)
	{
	  println("onLostUser - userId: " + userId);
	}

	public void onExitUser(int userId)
	{
	  println("onExitUser - userId: " + userId);
	}

	public void onReEnterUser(int userId)
	{
	  println("onReEnterUser - userId: " + userId);
	}

	public void onStartCalibration(int userId)
	{
	  println("onStartCalibration - userId: " + userId);
	}

	public void onEndCalibration(int userId, boolean successfull)
	{
	  println("onEndCalibration - userId: " + userId + ", successfull: " + successfull);
	  
	  if (successfull) 
	  { 
	    println("  User calibrated !!!");
	    context.startTrackingSkeleton(userId); 
	  } 
	  else 
	  { 
	    println("  Failed to calibrate user !!!");
	    println("  Start pose detection");
	    context.startPoseDetection("Psi",userId);
	  }
	}

	public void onStartPose(String pose,int userId)
	{
	  println("onStartPose - userId: " + userId + ", pose: " + pose);
	  println(" stop pose detection");
	  
	  context.stopPoseDetection(userId); 
	  context.requestCalibrationSkeleton(userId, true);
	 
	}

	public void onEndPose(String pose,int userId)
	{
	  println("onEndPose - userId: " + userId + ", pose: " + pose);
	}

}
