package main;

import gestures.GestureSimpleNiRecognizer;

import java.awt.AWTException;

import processing.core.PApplet;
import processing.core.PImage;
import SimpleOpenNI.SimpleOpenNI;

//FIXME: Excluir ou adaptar para nova interface

@SuppressWarnings("serial")
public class Prototipo extends PApplet{
	
    static public void main(String args[]) {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", Prototipo.class.getName() });
    }

	SimpleOpenNI openNi;
	boolean depthIsEnabled;
	GestureSimpleNiRecognizer gestureRecognizer;
	GestureRobot gestureRobot;
	
//	ObjectButton btn;
//	ObjectButton btn2;

	public void setup() {
		size(640, 400);

    	setupOpenNi();

		setupRobot();
		  
		setupGui();
	}

	private void setupOpenNi() {
		openNi = new SimpleOpenNI(this);
		gestureRecognizer = new GestureSimpleNiRecognizer(openNi);
	    //enable depthMap generation
	    depthIsEnabled = openNi.enableDepth(); //FIXME: como identificar se o kinect está conectado?	
	    
		if(depthIsEnabled){
			openNi.setMirror(true);
			gestureRecognizer.enableHands();
			gestureRecognizer.addHandListener(gestureRobot);
		}
		else{
			System.out.println("ERROR!");
		}
	}

	private void setupGui() {
//		btn = new ObjectButton(this.width/4, this.height/2, 50);
//		btn2 = new ObjectButton(3* this.width/4, this.height/2, 50);
//		
//		PImage portaAberta = loadImage( "images/portaAberta.png");
//		PImage portaFechada = loadImage("images/portaFechada.jpg");
//		PImage dialog1 = loadImage("images/Dialogo1.png");
//		dialog1.resize(dialog1.width/2, dialog1.height/2);
//	
//		btn.dialog = dialog1;
//		btn2.dialog = dialog1;
//		println(portaAberta);
//		
//		btn.setBackgroundImage(portaFechada);
//		
//		ServiceNode abrir = new ServiceNode("abrir");
//		abrir.setIcon(portaAberta);
//		btn.addServiceButton(abrir);
//		
//		ServiceNode fechar = new ServiceNode("fechar");
//		fechar.setIcon(portaFechada);
//		btn.addServiceButton(fechar);
//		
//		btn.addServiceButton( new ServiceNode("olho mágico"));
//		btn.addServiceButton( new ServiceNode("trancar"));
//		btn.addServiceButton( new ServiceNode("destrancar"));
//		btn.addServiceButton( new ServiceNode("outro"));
//		
//		btn2.addServiceButton( new ServiceNode("olho mágico"));
//		btn2.addServiceButton( new ServiceNode("trancar"));
//		btn2.addServiceButton( new ServiceNode("destrancar"));
//		btn2.addServiceButton( new ServiceNode("outro"));
	}

	private void setupRobot() {
		try
		{
			gestureRobot = new GestureRobot(this.frame);
		}
		catch (AWTException e)
		{
			println("Robot class not supported by your system!");
			exit();
		}
	}

	public void draw() {
		background(200);
		
		if(depthIsEnabled){
			openNi.update();
			PImage depthImage = openNi.depthImage();
			image(depthImage,0,0);
		}
		
//		btn.draw(this.g);
//		btn2.draw(this.g);
	}

	public void mouseMoved() {
//	  btn.cursorMoved(mouseX, mouseY);
//	  btn2.cursorMoved(mouseX, mouseY);
	}

	public void mousePressed() {
//	  btn.cursorPressed(mouseX, mouseY);
//	  btn2.cursorPressed(mouseX, mouseY);
	}
    
}
