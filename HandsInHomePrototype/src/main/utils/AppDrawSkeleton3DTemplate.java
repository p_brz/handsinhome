package main.utils;
import gestures.skeleton.Skeleton;
import gestures.skeleton.SkeletonUpdater;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import processing.core.PApplet;
import SimpleOpenNI.SimpleOpenNI;

public class AppDrawSkeleton3DTemplate extends PApplet{
    /**
	 * 
	 */
	private static final long serialVersionUID = 6386192463565544367L;

	static public void main(String args[])  {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", AppDrawSkeleton3DTemplate.class.getName() });
    }
    
	protected SimpleOpenNI context;
	protected float        zoomF =0.5f;
	protected float        rotX = radians(180);  // by default rotate the hole scene 180deg around the x-axis, 
	                                   // the data from openni comes upside down
	protected float        rotY = radians(0);
	protected boolean      autoCalib=true;

	protected SkeletonUpdater updater;
	protected SkeletonDrawer3D skeletonDrawer;
	private final Map<Integer, Skeleton> skeletons;
	private int[] userList;
	
	protected int backgroundColor;

	protected int myWidth;
	protected int myHeight;
	
	public AppDrawSkeleton3DTemplate(){
		skeletonDrawer = new SkeletonDrawer3D();
		skeletons = new HashMap<Integer, Skeleton>();
		userList = new int[0];
		
		this.backgroundColor = this.color(125);
		this.myWidth = 1024;
		this.myHeight = 768;
	}
	
	public void setup() {
		size(myWidth, myHeight, P3D); 
		
		setupGestures();
		
		stroke(255, 255, 255);
		smooth();
		perspective(radians(45), width / (1.0f * height), 10, 150000);
	}

	private void setupGestures() {
		context = new SimpleOpenNI(this);

		// disable mirror
		context.setMirror(true);

		// enable depthMap generation
		if (context.enableDepth() == false) {
			println("Can't open the depthMap, maybe the camera is not connected!");
			exit();
			return;
		}

		// enable skeleton generation for all joints
		context.enableUser(SimpleOpenNI.SKEL_PROFILE_ALL);

		updater = new SkeletonUpdater(context);
	}


	public void draw() {
		setupSceneDraw();
		updateSkeletons();
		drawUsers();
	}


	protected void setupSceneDraw() {
		background(backgroundColor);

		// set the scene pos
		translate(width / 2, height / 2, 0);
		rotateX(rotX);
		rotateY(rotY);
		scale(zoomF);
		translate(0, 0, -1000); // set the rotation center of the scene 1000
								// infront of the camera

	}
	protected void updateSkeletons() {
		context.update();
		// draw the skeleton if it's available
		this.userList = context.getUsers();
		
		for (int i = 0; i < userList.length; i++) {
			if (context.isTrackingSkeleton(userList[i])) {
				updateSkeleton(userList[i]);
			}
		}
	}

	private void updateSkeleton(int userId) {
		updater.updateSkeleton(userId, skeletons.get(userId));
	}
	
	protected Collection<Skeleton> getSkeletons(){
		return Collections.unmodifiableCollection(this.skeletons.values());
	}
	protected Skeleton getSkeleton(int userId){
		return this.skeletons.get(userId);
	}
	protected int[] getUsers(){
		return this.userList;
	}

	protected void drawUsers() {
		pushStyle();
			stroke(100);
			for(int userId : this.skeletons.keySet()){
				drawUser(userId);
			}
		popStyle();
	}
	
	protected void drawUser(int userId) {
		skeletonDrawer.drawSkeleton(this.g, this.getSkeleton(userId));
	}

//	protected void drawUser(int userId) {
//		drawSkeletonLimbs(userId);
//	}
//	// draw the skeleton with the selected joints
//	public void drawSkeletonLimbs(int userId)
//	{
//		pushStyle();
//			strokeWeight(3);
//	
//			// to get the 3d joint data
//			drawLimb(userId, SimpleOpenNI.SKEL_HEAD, SimpleOpenNI.SKEL_NECK);
//	
//			drawLimb(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_LEFT_SHOULDER);
//			drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_LEFT_ELBOW);
//			drawLimb(userId, SimpleOpenNI.SKEL_LEFT_ELBOW, SimpleOpenNI.SKEL_LEFT_HAND);
//	
//			drawLimb(userId, SimpleOpenNI.SKEL_NECK, SimpleOpenNI.SKEL_RIGHT_SHOULDER);
//			drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOU0.45359933, -0.30564606, 0.83715475LDER, SimpleOpenNI.SKEL_RIGHT_ELBOW);
//			drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_ELBOW, SimpleOpenNI.SKEL_RIGHT_HAND);
//	
//			drawLimb(userId, SimpleOpenNI.SKEL_LEFT_SHOULDER, SimpleOpenNI.SKEL_TORSO);
//			drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_SHOULDER, SimpleOpenNI.SKEL_TORSO);
//	
//			drawLimb(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_LEFT_HIP);
//			drawLimb(userId, SimpleOpenNI.SKEL_LEFT_HIP, SimpleOpenNI.SKEL_LEFT_KNEE);
//			drawLimb(userId, SimpleOpenNI.SKEL_LEFT_KNEE, SimpleOpenNI.SKEL_LEFT_FOOT);
//	
//			drawLimb(userId, SimpleOpenNI.SKEL_TORSO, SimpleOpenNI.SKEL_RIGHT_HIP);
//			drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_HIP, SimpleOpenNI.SKEL_RIGHT_KNEE);
//			drawLimb(userId, SimpleOpenNI.SKEL_RIGHT_KNEE, SimpleOpenNI.SKEL_RIGHT_FOOT);
//		popStyle();
//	}
//
//	public void drawLimb(int userId,int jointType1,int jointType2)
//	{
//		PVector jointPos1 = new PVector();
//		PVector jointPos2 = new PVector();
//		float  confidence;
//
//		// draw the joint position
//		confidence = context.getJointPositionSkeleton(userId,jointType1,jointPos1);
//		confidence = context.getJointPositionSkeleton(userId,jointType2,jointPos2);
//
//		stroke(255,0,0,confidence * 200 + 55);
//		line(jointPos1.x,jointPos1.y,jointPos1.z,
//				jointPos2.x,jointPos2.y,jointPos2.z);
//	}

	// -----------------------------------------------------------------
	// SimpleOpenNI user events

	public void onNewUser(int userId)
	{
		println("onNewUser - userId: " + userId);
		println("  start pose detection");

		if(autoCalib)
			context.requestCalibrationSkeleton(userId,true);
		else    
			context.startPoseDetection("Psi",userId);
	}

	public void onLostUser(int userId)
	{
		println("onLostUser - userId: " + userId);
	}

	public void onExitUser(int userId)
	{
		println("onExitUser - userId: " + userId);
		skeletons.remove(userId);
	}

	public void onReEnterUser(int userId)
	{
		println("onReEnterUser - userId: " + userId);
	}


	public void onStartCalibration(int userId)
	{
		println("onStartCalibration - userId: " + userId);
	}

	public void onEndCalibration(int userId, boolean successfull)
	{
		println("onEndCalibration - userId: " + userId + ", successfull: " + successfull);

		if (successfull) 
		{ 
			println("  User calibrated !!!");
			context.startTrackingSkeleton(userId);

			Skeleton skeleton = new Skeleton();
			this.updater.updateSkeleton(userId, skeleton);
			this.skeletons.put(userId, skeleton);
		} 
		else 
		{ 
			println("  Failed to calibrate user !!!");
			println("  Start pose detection");
			context.startPoseDetection("Psi",userId);
		}
	}

	public void onStartPose(String pose,int userId)
	{
		println("onStartdPose - userId: " + userId + ", pose: " + pose);
		println(" stop pose detection");

		context.stopPoseDetection(userId); 
		context.requestCalibrationSkeleton(userId, true);

	}

	public void onEndPose(String pose,int userId)
	{
		println("onEndPose - userId: " + userId + ", pose: " + pose);
	}

	// -----------------------------------------------------------------
	// Keyboard events

	public void keyPressed()
	{
		switch(key)
		{
			case ' ':
				context.setMirror(!context.mirror());
				break;
		}

		switch(keyCode)
		{
			case LEFT:
				rotY += 0.1f;
				break;
			case RIGHT:
				// zoom out
				rotY -= 0.1f;
				break;
			case UP:
				if(keyEvent.isShiftDown())
					zoomF += 0.01f;
				else
					rotX += 0.1f;
				break;
			case DOWN:
				if(keyEvent.isShiftDown())
				{
					zoomF -= 0.01f;
					if(zoomF < 0.01)
						zoomF = 0.01f;
				}
				else
					rotX -= 0.1f;
				break;
		}
	}
}
