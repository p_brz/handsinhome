package main.utils;

import gestures.CompositeGesture;
import gestures.Direction;
import gestures.DistanceGesture;
import gestures.Gesture;
import gestures.HoldGesture;
import gestures.HoldPositionGesture;
import gestures.LineMovementGesture;
import gestures.Position;
import gestures.PositionGesture;
import gestures.SequenceGesture;
import gestures.WaitTime;
import gestures.skeleton.Skeleton.SkeletonPart;
import gestures.skeleton.SkeletonPoint;
import gestures.skeleton.SkeletonPoint.PartPoint;

public class GestureFactory {
	private static GestureFactory singleton = null;
	
	public static GestureFactory getInstance(){
		if(singleton == null){
			singleton = new GestureFactory();
		}
		return singleton;
	}
	


	//******************************CRIAR GESTOS *********************************************************
	
	public Gesture createPassRightLHand() {
		Gesture handLeftShoulder = new PositionGesture(new Position[]{Position.Left}
				, SkeletonPart.LeftHand,SkeletonPart.LeftShoulder);
		Gesture handFrontAboveAbdomen = new PositionGesture(new Position[]{Position.Front, Position.Above}
				, SkeletonPart.LeftHand,SkeletonPart.Abdomen);
		Gesture handBelowHead = new PositionGesture(new Position[]{Position.Below}
				, SkeletonPart.LeftHand,SkeletonPart.Head);
		
		Gesture initialPosition = new CompositeGesture(handBelowHead,handFrontAboveAbdomen,handLeftShoulder);
		
		HoldGesture holdInitialPose = new HoldGesture(initialPosition,WaitTime.SmallTime.getTime()/4);
		
		LineMovementGesture moveLeft = new LineMovementGesture(
				new Direction[]{Direction.RIGHT}, SkeletonPart.LeftHand,80);

		SequenceGesture sequenceGesture = new SequenceGesture(holdInitialPose, moveLeft);
		
		return sequenceGesture;
	}
	public Gesture createPassLeftRHand() {
		Gesture handRightShoulder = new PositionGesture(new Position[]{Position.Right}
				, SkeletonPart.RightHand,SkeletonPart.RightShoulder);
		Gesture handFrontAboveAbdomen = new PositionGesture(new Position[]{Position.Front, Position.Above}
				, SkeletonPart.RightHand,SkeletonPart.Abdomen);
		Gesture handBelowHead = new PositionGesture(new Position[]{Position.Below}
				, SkeletonPart.RightHand,SkeletonPart.Head);
		
		Gesture initialPosition = new CompositeGesture(handBelowHead,handFrontAboveAbdomen,handRightShoulder);
		
		HoldGesture holdInitialPose = new HoldGesture(initialPosition,WaitTime.SmallTime.getTime()/4);
		
		LineMovementGesture moveLeft = new LineMovementGesture(
				new Direction[]{Direction.LEFT}, SkeletonPart.RightHand,80);

		SequenceGesture sequenceGesture = new SequenceGesture(holdInitialPose, moveLeft);
		
		return sequenceGesture;
	}

	public Gesture createPassRightRHand() {		
		Gesture handLeftChest = new DistanceGesture(SkeletonPart.RightHand, SkeletonPart.Chest,Direction.LEFT, 100.0,null);
		Gesture stopHandLeft = new HoldPositionGesture(SkeletonPart.RightHand,WaitTime.SmallTime.getTime()/8);
		Gesture holdHandLeft = new CompositeGesture(handLeftChest,stopHandLeft);
		LineMovementGesture moveRight = new LineMovementGesture(
				new Direction[]{Direction.RIGHT}, SkeletonPart.RightHand,80);

		SequenceGesture sequenceGesture = new SequenceGesture(holdHandLeft, moveRight);
		
		return sequenceGesture;
	}
	
	public Gesture createPassUpRHand() {
		Gesture handBelowAbdomen = new PositionGesture(new Position[]{Position.Below}
														, new SkeletonPoint(SkeletonPart.RightHand)
														, new SkeletonPoint(SkeletonPart.Abdomen, PartPoint.End));
		Gesture handFrontBody = new PositionGesture(new Position[]{Position.Front}
														, new SkeletonPoint(SkeletonPart.RightHand)
														, new SkeletonPoint(SkeletonPart.Body));
		Gesture stopHandALittleTime = new HoldPositionGesture(SkeletonPart.RightHand, WaitTime.SmallTime.getTime()/8);
		
		Gesture initialPose = new CompositeGesture(handBelowAbdomen, handFrontBody, stopHandALittleTime);
		
		LineMovementGesture handMoveUp = new LineMovementGesture(
				new Direction[]{Direction.UP}, SkeletonPart.RightHand);

		SequenceGesture sequenceGesture = new SequenceGesture(initialPose, handMoveUp);
		
		return sequenceGesture;
	}
	public Gesture createPassDownRHand() {
		Gesture handAboveHead = new PositionGesture(new Position[]{Position.Above}
															, new SkeletonPoint(SkeletonPart.RightHand)
															, new SkeletonPoint(SkeletonPart.Head, PartPoint.Start));
		Gesture handFrontBody = new PositionGesture(new Position[]{Position.Front}
													, new SkeletonPoint(SkeletonPart.RightHand)
													, new SkeletonPoint(SkeletonPart.Body));
//		Gesture stopHandALittleTime = new HoldPositionGesture(SkeletonPart.RightHand, WaitTime.SmallTime.getTime()/8);
		
		Gesture holdHandAbove = new CompositeGesture(handAboveHead, handFrontBody);
		LineMovementGesture handMoveDown = new LineMovementGesture(
				new Direction[]{Direction.DOWN}, SkeletonPart.RightHand);

		SequenceGesture sequenceGesture = new SequenceGesture(holdHandAbove, handMoveDown);
		
		return sequenceGesture;
	}
//	private Gesture createPassDownRHand() {
//		PositionGesture handUp = new PositionGesture(new Position[]{Position.Up}, SkeletonPart.RightHand,SkeletonPart.Chest);
//		HoldPositionGesture waitALittle = new HoldPositionGesture(SkeletonPart.RightHand, WaitTime.SmallTime.getTime()/2);
//		
//		CompositeGesture waitHandUp = new CompositeGesture(handUp, waitALittle);
//		
//		LineMovementGesture moveHandDown = new LineMovementGesture(
//				new Direction[]{Direction.DOWN}, SkeletonPart.RightHand);
//		
//		SequenceGesture gesture = new SequenceGesture(waitHandUp,moveHandDown);
//		
//		return gesture;
//	}
//	public Gesture createPassDownLHand() {
////		PositionGesture handAboveHead = new PositionGesture(new Position[]{Position.Up}, SkeletonPart.RightHand,SkeletonPart.Head);
//		Gesture handAboveHead = new PositionGesture(new Position[]{Position.Up}
//															, new SkeletonPoint(SkeletonPart.LeftHand)
//															, new SkeletonPoint(SkeletonPart.Head, PartPoint.Start));
//		Gesture handFrontBody = new PositionGesture(new Position[]{Position.Front}
//													, new SkeletonPoint(SkeletonPart.LeftHand)
//													, new SkeletonPoint(SkeletonPart.Body));
//		Gesture stopHandALittleTime = new HoldPositionGesture(SkeletonPart.LeftHand, WaitTime.SmallTime.getTime()/8);
//		
//		Gesture holdHandAbove = new CompositeGesture(handAboveHead, handFrontBody,stopHandALittleTime);
////		HoldGesture holdHandAbove = new HoldGesture(handAboveHead,0);
//		LineMovementGesture handMoveDown = new LineMovementGesture(new Direction[]{Direction.DOWN}, SkeletonPart.LeftHand);
//
//		SequenceGesture sequenceGesture = new SequenceGesture(holdHandAbove, handMoveDown);
//		
//		return sequenceGesture;
//	}

	public Gesture createClick() {
		
		Gesture handFrontUpChest = new PositionGesture(new Position[]{Position.Front, Position.Above} 
														, new SkeletonPoint(SkeletonPart.RightHand)
														, new SkeletonPoint(SkeletonPart.Chest,PartPoint.End));
		LineMovementGesture moveHandFront = new LineMovementGesture(new Direction[]{Direction.FRONT}, SkeletonPart.RightHand);
		Gesture holdHand = new HoldPositionGesture(SkeletonPart.RightHand,WaitTime.SmallTime.getTime()/2);


		SequenceGesture gesture = new SequenceGesture();
		gesture.addGesture(new CompositeGesture(handFrontUpChest,moveHandFront));
		gesture.addGesture(new CompositeGesture(handFrontUpChest,holdHand));
		
		return gesture;
	}

	public Gesture createGoBack() {

		Gesture initialPose = createGoBackInitialPose();
		LineMovementGesture forearmMove = new LineMovementGesture(new Direction[]{Direction.BACK}, SkeletonPart.RightHand);
//		Gesture handFrontDownChest = new PositionGesture(new Position[]{Position.Front, Position.Down} , SkeletonPart.RightHand, SkeletonPart.Chest);
		Gesture handFrontDownChest = new PositionGesture(new Position[]{Position.Front, Position.Below} 
														, new SkeletonPoint(SkeletonPart.RightHand),new SkeletonPoint(SkeletonPart.Chest));
		Gesture elbowBackChest = new PositionGesture(new Position[]{Position.Back}
														, new SkeletonPoint(SkeletonPart.RightArm, PartPoint.End)
														, new SkeletonPoint(SkeletonPart.Chest));
//		Gesture forearmUpArm = new DirectionGesture(new Direction[]{Direction.UP}, SkeletonPart.RightForearm, SkeletonPart.RightArm);
		Gesture holdHandAndElbow = new CompositeGesture(new HoldPositionGesture(SkeletonPart.RightHand, WaitTime.SmallTime.getTime()/3)
														, new HoldPositionGesture(SkeletonPart.RightArm, WaitTime.SmallTime.getTime()/3));
		
		SequenceGesture gesture = new SequenceGesture(initialPose
													, forearmMove
													,new CompositeGesture( handFrontDownChest,elbowBackChest, holdHandAndElbow));
		
		return gesture;
	}
	
	public Gesture createGoBackInitialPose() {
		Gesture handFrontDownChest = new PositionGesture(new Position[]{Position.Front, Position.Below} 
		, SkeletonPart.RightHand, SkeletonPart.Chest);
		Gesture elbowFrontChest = new PositionGesture(new Position[]{Position.Front}
													, new SkeletonPoint(SkeletonPart.RightArm, PartPoint.End)
													, new SkeletonPoint(SkeletonPart.Chest));
//		Gesture holdHandStop = new HoldPositionGesture(SkeletonPart.RightHand, WaitTime.SmallTime.getTime()/8);
		
		Gesture initialPose = new CompositeGesture(handFrontDownChest, elbowFrontChest);
		
		return initialPose;
	}

	public Gesture createOver() {
		PositionGesture handFrontBody = new PositionGesture(new Position[]{Position.Front}, SkeletonPart.RightHand, SkeletonPart.Body);
		PositionGesture handUpAbdomen = new PositionGesture(new Position[]{Position.Above}, SkeletonPart.RightHand, SkeletonPart.Abdomen);
		PositionGesture handDownHead  = new PositionGesture(new Position[]{Position.Below}, SkeletonPart.RightHand, SkeletonPart.Head);
		HoldPositionGesture handStopped   = new HoldPositionGesture(SkeletonPart.RightHand, WaitTime.MediumTime);
		
		CompositeGesture gesture = new CompositeGesture();
		gesture.addGesture(handFrontBody);
		gesture.addGesture(handUpAbdomen);
		gesture.addGesture(handDownHead);
		gesture.addGesture(handStopped);
		
		return gesture;
	}
}
