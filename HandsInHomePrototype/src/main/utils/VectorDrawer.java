package main.utils;

import processing.core.PGraphics;
import processing.core.PVector;

public class VectorDrawer {
	public static void drawVector(PGraphics graphics, PVector vector, PVector location){
		graphics.line(location.x, location.y, location.z
				, location.x + vector.x, location.y + vector.y, location.z + vector.z);
	}
	
	private PGraphics graphics;
	
	public VectorDrawer(PGraphics graphics) {
		super();
		this.graphics = graphics;
	}

	public PGraphics getGraphics() {
		return graphics;
	}

	public void setGraphics(PGraphics graphics) {
		this.graphics = graphics;
	}
	
	public void draw(PVector vector, PVector location, float size){
		PVector vecNormal = vector.get();
		vecNormal.normalize();
		
		PVector vecResize = PVector.mult(vecNormal, size);
		VectorDrawer.drawVector(this.graphics, vecResize, location);
	}

	public void drawVectors(PVector[] vectors, PVector location, float size){
		this.drawVectors(vectors, location, size,null);
	}
	public void drawVectors(PVector[] vectors, PVector location, float size, int colors[]){
		graphics.pushStyle();
		for(int i=0;i< vectors.length; ++i){
			if(colors != null){
				graphics.stroke(colors[i]);
			}
			this.draw(vectors[i], location, size);
		}
		graphics.popStyle();
	}
}
