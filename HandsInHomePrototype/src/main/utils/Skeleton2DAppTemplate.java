package main.utils;

import gestures.skeleton.BodyPart;
import gestures.skeleton.Skeleton;
import gestures.skeleton.Skeleton.SkeletonPart;
import gestures.skeleton.SkeletonUpdater;

import javax.swing.UIManager;

import processing.core.PApplet;
import processing.core.PVector;
import SimpleOpenNI.SimpleOpenNI;

public class Skeleton2DAppTemplate extends PApplet{

    /**
	 * 
	 */
	private static final long serialVersionUID = 8250332375652987825L;

	public static void main(String args[])  {
        PApplet.main(new String[] { "--bgcolor=#ECE9D8", Skeleton2DAppTemplate.class.getName() });
    }


	protected SimpleOpenNI  context;
	protected boolean autoCalib;
	protected boolean gesturesEnable;
	protected boolean showPartName = true;
	
	protected Skeleton userSkeleton;
	protected SkeletonUpdater updater;

//	protected SkeletonDrawer2D skelDrawer;

	public Skeleton2DAppTemplate(){
		this(false);
	}
	public Skeleton2DAppTemplate(boolean autoCalib){
//		skelDrawer = new SkeletonDrawer2D();
		this.autoCalib = autoCalib;
		userSkeleton = new Skeleton();
	}
	
	public void setup() {
		setupLookAndFeel(); 
		setupView();
		setupGestures();
	}
	protected void setupLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void setupView() {
		// size(640,480);
		size(1100, 600); //800x2,450
		this.frameRate(15);
		background(220);

		stroke(0, 0, 255);
		strokeWeight(3);
		smooth();
	}
	
	protected void setupGestures() {
		context = new SimpleOpenNI(this);
		gesturesEnable = context.enableDepth() 
				&& context.enableUser(SimpleOpenNI.SKEL_PROFILE_ALL);
		// context.enableUser(SimpleOpenNI.SKEL_PROFILE_UPPER);

		if(gesturesEnable){
			context.setMirror(true);
	
			updater = new SkeletonUpdater(context);
		}
	}

	//*********************************************************************************************
	
	public void draw() {
		if(gesturesEnable){
			// update the cam
			context.update();
			
			// draw depthImageMap
			image(context.depthImage(), 0, 0);
	
			drawUsers();
		}
	}

	protected void drawUsers() {
		pushStyle();

			textSize(18);
			// draw the skeleton if it's available
			int[] userList = context.getUsers();
			for (int i = 0; i < userList.length; i++) {
				if (context.isTrackingSkeleton(userList[i])) {

					updater.updateSkeleton(userList[0], userSkeleton);
//					skelDrawer.drawSkeleton(this.g, userSkeleton);
					drawSkeleton(userSkeleton);
				}
			}  
		
		popStyle();
	}

	protected void drawSkeleton(Skeleton skel) {
		for(SkeletonPart part: SkeletonPart.values()){
			showPart(skel, part);
		}
	}
	



	private void showPart(Skeleton skeleton, SkeletonPart part) {
		String partName = part.toString();
		
		drawBodyPart(partName, skeleton.getPart(part));
	}
	

	private void drawBodyPart(String partName, BodyPart part){
		this.pushStyle();
			PVector center = new PVector();
			part.getCentralPosition(center);
			context.convertRealWorldToProjective(center, center);
			
			drawLimb(part);
			ellipse(center.x,center.y,25,25);
			
			if(showPartName){
				fill(255,0,0);
				text(partName, center.x,center.y);
			}
		this.popStyle();
	}

	private void drawLimb(BodyPart part) {
		PVector start = new PVector();
		PVector end = new PVector();
		context.convertRealWorldToProjective(part.getStartPosition(), start);
		context.convertRealWorldToProjective(part.getEndPosition(), end);
		
		line(start.x,start.y,end.x,end.y);
	}

	// -----------------------------------------------------------------
	// SimpleOpenNI events

	public void onNewUser(int userId)
	{
	  println("onNewUser - userId: " + userId);
	  println("  start pose detection");
	  
	  if(autoCalib)
	    context.requestCalibrationSkeleton(userId,true);
	  else    
	    context.startPoseDetection("Psi",userId);
	}

	public void onLostUser(int userId)
	{
	  println("onLostUser - userId: " + userId);
	}

	public void onExitUser(int userId)
	{
	  println("onExitUser - userId: " + userId);
	}

	public void onReEnterUser(int userId)
	{
	  println("onReEnterUser - userId: " + userId);
	}

	public void onStartCalibration(int userId)
	{
	  println("onStartCalibration - userId: " + userId);
	}

	public void onEndCalibration(int userId, boolean successfull)
	{
	  println("onEndCalibration - userId: " + userId + ", successfull: " + successfull);
	  
	  if (successfull) 
	  { 
	    println("  User calibrated !!!");
	    context.startTrackingSkeleton(userId); 
	  } 
	  else 
	  { 
	    println("  Failed to calibrate user !!!");
	    println("  Start pose detection");
	    context.startPoseDetection("Psi",userId);
	  }
	}

	public void onStartPose(String pose,int userId)
	{
	  println("onStartPose - userId: " + userId + ", pose: " + pose);
	  println(" stop pose detection");
	  
	  context.stopPoseDetection(userId); 
	  context.requestCalibrationSkeleton(userId, true);
	 
	}

	public void onEndPose(String pose,int userId)
	{
	  println("onEndPose - userId: " + userId + ", pose: " + pose);
	}

}
