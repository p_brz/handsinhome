package main;


import gestures.HandEvent;
import gestures.HandListener;

import java.awt.AWTException;
import java.awt.Frame;
import java.awt.Robot;

import processing.core.PVector;

public class GestureRobot implements HandListener{

	Robot robby;
	Frame parent;	

	public GestureRobot(Frame parent) throws AWTException {
		this.parent = parent;
		robby = new Robot();
	}

	@Override
	public void handCreated(HandEvent evt) {
		System.out.println("Created Hand: " + evt.getHandId() + " at " + evt.getHandPosition());
	}

	@Override
	public void handUpdated(HandEvent evt) {		
		PVector position = evt.getScreenPosition();
		int lx= parent.getX();
		int ly=parent.getY();
		
		robby.mouseMove((int)position.x + lx, (int)position.y + ly);
	}

	@Override
	public void handDestroyed(HandEvent evt) {
		System.out.println("Destroyed Hand: " + evt.getHandId());
	}
}
