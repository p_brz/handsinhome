public class CompositeAligner implements ComponentAligner{
    private HorizontalAligner horAligner;
    private VerticalAligner   verAligner;
    
    public CompositeAligner(){
        this(new HorizontalAligner(),new VerticalAligner());
    }
    public CompositeAligner(HorizontalAligner horizontalAligner, VerticalAligner verticalAligner){
        horAligner = horizontalAligner;
        verAligner = verticalAligner;
    }
    
    public HorizontalAligner getHorizontalAligner(){return horAligner;}
    public void setHorizontalAligner(HorizontalAligner aligner){this.horAligner = aligner;}
    public VerticalAligner getVerticalAligner(){return verAligner;}
    public void setVerticalAligner(VerticalAligner aligner){this.verAligner = aligner;}
    
    
    @Override
    public void align(int xOrigin, int yOrigin, Component comp ){
        horAligner.align(xOrigin,yOrigin,comp);
        verAligner.align(xOrigin,yOrigin,comp);
    }

}
