
import java.awt.Dimension;

public class ListVerticalLayout implements ListLayout{
  private int gap;
  
  public int getGap(){ return gap;}
  public void setGap(int Gap){ this.gap = Gap;}
  
  @Override
  public void doLayout(Container container, Dimension maxSize){    
      int totalHeight = 0;
      int maxWidth = 0;
      for(Component comp : container.getComponents()){
        if(maxSize != null && (totalHeight + comp.getHeight() > maxSize.getHeight() || maxWidth + comp.getWidth() > maxSize.getWidth())){
            break; //this is ugly!!!
        }
        comp.setY(totalHeight);
        comp.setX(0);
        totalHeight += comp.getHeight() + getGap();
        
        maxWidth = (comp.getWidth() > maxWidth ? comp.getWidth() : maxWidth);
      } 
      container.setHeight(totalHeight);
      container.setWidth(maxWidth);
  }  
}
