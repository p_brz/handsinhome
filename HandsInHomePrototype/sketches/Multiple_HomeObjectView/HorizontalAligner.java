public class HorizontalAligner implements ComponentAligner{
    public enum HorizontalAlignment{Left,Center,Right};
    
    private HorizontalAlignment alignment;
    
    public HorizontalAligner(){
        this(HorizontalAlignment.Left);
    }
    public HorizontalAligner(HorizontalAlignment alignment){
        this.alignment = alignment;
    }
    
    public HorizontalAlignment getAlignment(){return alignment;}
    public void setAlignment(HorizontalAlignment alignment){this.alignment = alignment;}
    
    
    @Override
    public void align(int xOrigin, int yOrigin, Component comp ){
        if(alignment == HorizontalAlignment.Left){
            comp.setX(xOrigin);
        }
        else if(alignment == HorizontalAlignment.Center){
            comp.setX(xOrigin - comp.getWidth()/2);
        }
        else if(alignment == HorizontalAlignment.Right){
            comp.setX(xOrigin - comp.getWidth());
        }
    }

}
