
PImage lampOn;
PImage lampOff;
PImage background;

BlockService servLampOn;
BlockService servLampOff;
ServiceButton lampOnView;
ServiceButton lampOffView;

//ListView container;
CarouselListContainer container;
ListLayout horizontalLayout;
ListLayout verticalLayout;

boolean dragged;

void setup(){
  dragged = false;
  
  size(640,480);
  lampOn = loadImage("images/lampadaLigada.png");
  lampOff = loadImage("images/lampadaDesligada.png");
  background = loadImage("images/background.png");
  background.resize(this.width,this.height);
  
  servLampOn = new BlockService("ligar", lampOn);
  servLampOff = new BlockService("desligar", lampOff);
  
  lampOnView = new ServiceButton(servLampOn,50,height/2, 100,100);
  lampOffView = new ServiceButton(servLampOff,250,height/2, 100,100);
  
  //container = new ListView(20,height/4);
  container = new CarouselListContainer(20,height/4, 400, 500);
  container.addComponent(lampOnView);
  container.addComponent(lampOffView);
  
  horizontalLayout = new ListHorizontalLayout();
  verticalLayout = new ListVerticalLayout();
  container.setLayout(verticalLayout);
}

void draw(){
    background(background);
////  image(servLampOn.getIcon(),50,height/2);
////  image(servLampOff.getIcon(),250,height/2);
//  lampOnView.draw(this.g);
//  lampOffView.draw(this.g);
    container.draw(this.g);
}


void keyPressed(){
  if(key == CODED){
      if(keyCode == RIGHT){
        this.container.selectNext();
      }
      else if(keyCode == LEFT){
        this.container.selectPrevious();
      }
  }
  else{
      if(key == 'l'){
          changeLayout();
      }
  }
}

void changeLayout(){
    if(container.getLayout() == horizontalLayout){
        container.setLayout(verticalLayout);
    }
    else if(container.getLayout() == verticalLayout){
        container.setLayout(horizontalLayout);
    }
}

void mousePressed(){
}

void mouseReleased(){
  if(mouseButton == LEFT){
      if(!dragged){
          Component comp = container.getComponentAt(mouseX, mouseY);
          if(comp != null){
              container.removeComponent(comp);
          }
          else{
              container.addComponent(new ServiceButton(new BlockService("somebody", lampOn),0,0,100,100));
          }
      }
      else{
          dragged = false;
      }
  }
}

void mouseDragged(){
  if(mouseButton == LEFT){
      dragged = true;
      container.setLocation(mouseX,mouseY);
      
  }
}
