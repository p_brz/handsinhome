class Circle{
  private int x, y;
  private int radius;
  
  public int getX(){return x;}
  public int getY(){return y;}
  public int getRadius(){return radius;}
  
  public void setX(int X){this.x = X;}
  public void setY(int Y){this.y = Y;}
  public void setRadius(int Radius){this.radius = Radius;}
  
  public Circle(int x, int y, int radius){
    this.x = x;
    this.y = y;
    this.radius = radius;
  }
  
  public boolean isOver(int X, int Y){
    int deltaX = X - x;
    int deltaY = Y - y;
    double dist = Math.sqrt((deltaX)*(deltaX) + (deltaY)*(deltaY));
    return (dist <= radius);
  }
  
}
