public class CircularButton {
  Circle boundBox;
  int backgroundColor, highlightColor;
  PImage buttonImage;
  boolean isOver;

  public int getBackgroundColor() { 
    return backgroundColor;
  }
  public int getHighlightColor() { 
    return highlightColor;
  }
  public PImage getBackgroundImage() { 
    return buttonImage;
  }
  public Circle getBoundBox() { 
    return boundBox;
  }

  public void setBackgroundColor(int someColor) { 
    this.backgroundColor = someColor;
  }
  public void setHighlightColor(int someColor) { 
    this.highlightColor = someColor;
  }
  public void setBackgroundImage(PImage img) { 
    this.buttonImage = img;
  }

  public void setPosition(int x, int y) {
    this.boundBox.setX(x);
    this.boundBox.setY(y);
  }

  public CircularButton(int x, int y, int radius) {
    boundBox = new Circle(x, y, radius);
    buttonImage = null;
    backgroundColor = color(255);
    highlightColor = color(100);
    isOver = false;
  }

  public void cursorMoved(int x, int y) {
    if (this.boundBox != null) {
      boolean wasOver = this.isOver;
      this.isOver = boundBox.isOver(x, y);
      if (!wasOver && isOver) {
        onCursorEnter();
      }
      else if (wasOver && !isOver) {
        onCursorLeave();
      }
    }
  }
  public void cursorPressed(int x, int y) {
    if (this.boundBox != null && boundBox.isOver(x, y)) {
      onCursorPressed();
    }
  }

  public void onCursorEnter() {
    isOver = true;
    println("Cursor Enter!");
  }
  public void onCursorLeave() {
    isOver = false;
    println("Cursor Leave!");
  }
  public void onCursorPressed() {
    println("Cursor Click!");
  }

  public void draw(PGraphics graphic) {
    graphic.pushMatrix();
    graphic.fill((isOver? highlightColor : backgroundColor));
    int diameter = boundBox.getRadius()*2;
    graphic.ellipse(boundBox.getX(), boundBox.getY(), diameter, diameter);
 
    this.drawImage(graphic);
    graphic.popMatrix();
  }

  private void drawImage(PGraphics graphic) {
    if (this.buttonImage != null) {
      float scale = getScaleToFit(buttonImage);
      int newWidth = (int)(buttonImage.width*scale);
      int newHeight = (int)(buttonImage.height*scale);

      graphic.translate(boundBox.getX() - newWidth/2, boundBox.getY()- newHeight/2);
      graphic.scale(scale);
      graphic.image(this.buttonImage, 0, 0 );
    }
  }

  private float getScaleToFit(PImage img) {
    float imgDiagonal = (float)Math.sqrt(img.width*img.width + img.height*img.height); //teorema de pitágoras
    float scale = (this.getBoundBox().getRadius()*2) / imgDiagonal; //Semelhança de retangulos
    return scale;
  }
//  private void createGradient (float x, float y, float diameter, color c1, color c2) {
//    float px = 0, py = 0, angle = 0;
//
//    // calculate differences between color components 
//    float deltaR = red(c2)-red(c1);
//    float deltaG = green(c2)-green(c1);
//    float deltaB = blue(c2)-blue(c1);
//    // hack to ensure there are no holes in gradient
//    // needs to be increased, as radius increases
//    float gapFiller = 8.0;
//
//    for (int i=0; i< diameter/2; i++) {
//      for (float j=0; j<360; j+=1.0/gapFiller) {
//        px = x+cos(radians(angle))*i;
//        py = y+sin(radians(angle))*i;
//        angle+=1.0/gapFiller;
//        color c = color(red(c1)+i*(deltaR/diameter/2), green(c1)+i*(deltaG/diameter/2), blue(c1)+(i)*(deltaB/diameter/2));
//        set(int(px), int(py), c);
//      }
//    }
//    // adds smooth edge 
//    // hack anti-aliasing
//    noFill();
//    strokeWeight(3);
//    ellipse(x, y, diameter, diameter);
//  }
  
  
}

