
import java.awt.Dimension;

public class ListHorizontalLayout implements ListLayout{
  private int gap;
  
  public int getGap(){ return gap;}
  public void setGap(int Gap){ this.gap = Gap;}
  
  @Override
  public void doLayout(Container container, Dimension maxSize){    
      int totalWidth = 0;
      int maxHeight = 0;
      for(Component comp : container.getComponents()){
        if(maxSize != null && (totalWidth + comp.getWidth() > maxSize.getWidth() || maxHeight + comp.getHeight() > maxSize.getHeight())){
            break; //this is ugly!!!
        }
        comp.setX(totalWidth);
        comp.setY(0);
        totalWidth += comp.getWidth() + getGap();
        
        maxHeight = (comp.getHeight() > maxHeight ? comp.getHeight() : maxHeight);
      } 
      container.setWidth(totalWidth);
      container.setHeight(maxHeight);
  }  
}
