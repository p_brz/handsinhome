//package gui.components;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import processing.core.PGraphics;

public abstract class AbstractComponent implements Component, MouseListener
//public abstract class AbstractComponent implements MouseListener
{
  int x,y;
  int width, height;
  
  public AbstractComponent(){
    this(0,0,0,0);
  }
  public AbstractComponent(int X,int Y, int Width, int Height){
    this.x = X;
    this.y = Y;
    this.width = Width;
    this.height = Height;
  }
  
  public int getX(){return x;}
  public int getY(){return y;}
  public int getWidth(){return this.width;}
  public int getHeight(){return this.height;}
  
  public void setX(int X){this.x = X;}
  public void setY(int Y){this.y = Y;}
  public void setLocation(int x, int y){
    this.setX(x);
    this.setY(y);
  }
  public void setWidth(int Width){this.width = Width;}
  public void setHeight(int Height){this.height = Height;}
  public void setSize(int width, int height){
    this.setWidth(width);
    this.setHeight(height);
  }
  
  public boolean isOver(int X, int Y){
    return (this.x <= X && (this.x + this.width) >= X) && (this.y <= Y && (this.y + this.height) >= Y);
  }
  
    public void draw(PGraphics graphics){}
  
  @Override
  public void mouseClicked (MouseEvent e) {}
  @Override
  public void mouseEntered (MouseEvent e) {}
  @Override
  public void mouseExited  (MouseEvent e) {}
  @Override
  public void mousePressed (MouseEvent e) {}
  @Override
  public void mouseReleased(MouseEvent e) {}
}

