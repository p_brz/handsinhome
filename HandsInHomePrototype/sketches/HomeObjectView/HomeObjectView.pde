
PImage lampOn;
PImage lampOff;
PImage background;

BlockService servLampOn;
BlockService servLampOff;
//ServiceButton lampOnView;
//ServiceButton lampOffView;

////ListView container;
//CarouselListContainer container;
ListLayout horizontalLayout;
ListLayout verticalLayout;

HomeNodeView nodeView;

boolean dragged;

HorizontalAligner horAligner;
VerticalAligner verAligner;

void setup(){
  dragged = false;
  
  size(640,480);
  lampOn = loadImage("images/lampadaLigada.png");
  lampOff = loadImage("images/lampadaDesligada.png");
  background = loadImage("images/background.png");
  background.resize(this.width,this.height);
  
  servLampOn = new BlockService("ligar", lampOn);
  servLampOff = new BlockService("desligar", lampOff);
  
//  lampOnView = new ServiceButton(servLampOn,50,height/2, 100,100);
//  lampOffView = new ServiceButton(servLampOff,250,height/2, 100,100);
  
//  //container = new ListView(20,height/4);
//  container = new CarouselListContainer(20,height/4, 400, 500);
//  container.addComponent(lampOnView);
//  container.addComponent(lampOffView);
  
  horizontalLayout = new ListHorizontalLayout();
  verticalLayout = new ListVerticalLayout();
  
  nodeView = new HomeNodeView(20,height/4,100,100);
  nodeView.setImage(lampOn);
  nodeView.addServiceButton(servLampOn);
  nodeView.addServiceButton(servLampOff);
  nodeView.setServicesLayout(verticalLayout);
  
  horAligner = new HorizontalAligner(HorizontalAligner.HorizontalAlignment.Center);
  verAligner = new VerticalAligner(VerticalAligner.VerticalAlignment.Top);
}

void draw(){
    background(background);
//////  image(servLampOn.getIcon(),50,height/2);
//////  image(servLampOff.getIcon(),250,height/2);
////  lampOnView.draw(this.g);
////  lampOffView.draw(this.g);
//    container.draw(this.g);
    int centerX = width/2;
    int centerY = height/4;
    horAligner.align(centerX,centerY,nodeView);
    verAligner.align(centerX,centerY,nodeView);
    nodeView.draw(this.g);
    ellipse(centerX,centerY,5,5);
}


void mouseMoved(){
    boolean wasOver = nodeView.isOver(pmouseX, pmouseY);
    boolean isOver  = nodeView.isOver(mouseX, mouseY);
    if(!wasOver && isOver){ //mouseEnter
        nodeView.showServices();
    }
    else if(!isOver && wasOver){
        nodeView.hideServices();
    }
}

void keyPressed(){
  if(key == CODED){
//      if(keyCode == RIGHT){
//        this.container.selectNext();
//      }
//      else if(keyCode == LEFT){
//        this.container.selectPrevious();
//      }
  }
  else{
      switch(key){
          case 'l':
            changeLayout();
            break;
          case 'a':
            changeHorAlignment(HorizontalAligner.HorizontalAlignment.Left);
            break;
          case 's':
            changeHorAlignment(HorizontalAligner.HorizontalAlignment.Center);
            break;
          case 'd':
            changeHorAlignment(HorizontalAligner.HorizontalAlignment.Right);
            break;
          case 'q':
            changeVerAlignment(VerticalAligner.VerticalAlignment.Top);
            break;
          case 'w':
            changeVerAlignment(VerticalAligner.VerticalAlignment.Center);
            break;
          case 'e':
            changeVerAlignment(VerticalAligner.VerticalAlignment.Bottom);
            break;
      }
      if(key == 'l'){
      }
      
  }
}

void changeHorAlignment(HorizontalAligner.HorizontalAlignment alignment){
    horAligner.setAlignment(alignment);
    println("ChangeAlignment to " + horAligner.getAlignment());
}
void changeVerAlignment(VerticalAligner.VerticalAlignment alignment){
    verAligner.setAlignment(alignment);
    println("ChangeAlignment to " + verAligner.getAlignment());
}

void changeLayout(){
//    if(container.getLayout() == horizontalLayout){
//        container.setLayout(verticalLayout);
//    }
//    else if(container.getLayout() == verticalLayout){
//        container.setLayout(horizontalLayout);
//    }
    if(nodeView.getServicesLayout() == horizontalLayout){
        nodeView.setServicesLayout(verticalLayout);
    }
    else if(nodeView.getServicesLayout() == verticalLayout){
        nodeView.setServicesLayout(horizontalLayout);
    }
}

void mousePressed(){
}

void mouseReleased(){
  if(mouseButton == LEFT){
//      if(!dragged){
//          Component comp = container.getComponentAt(mouseX, mouseY);
//          if(comp != null){
//              container.removeComponent(comp);
//          }
//          else{
//              container.addComponent(new ServiceButton(new BlockService("somebody", lampOn),0,0,100,100));
//          }
//      }
//      else{
//          dragged = false;
//      }
  }
}

void mouseDragged(){
  if(mouseButton == LEFT){
      dragged = true;
//      container.setLocation(mouseX,mouseY);
      nodeView.setLocation(mouseX,mouseY);
      
  }
}
