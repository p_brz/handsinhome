//package gui.home;

//import gui.components.AbstractComponent;
//import gui.components.ImageComponent;
//import home.BlockService;

import processing.core.PGraphics;
import processing.core.PImage;

public class ServiceButton extends AbstractComponent{
    private BlockService service;   
    private ImageComponent imgComp;
       
    public BlockService getService(){ return service;}
    public void setService(BlockService service){this.service = service;}

    public ServiceButton(int x, int y, int width, int height){
        this(null,x,y,width, height);
    }
    public ServiceButton(BlockService service, int x, int y, int width, int height){
        super(x,y,width,height);
        this.service = service;
        imgComp = new ImageComponent();
    }
   
    @Override
    public void draw(PGraphics graphic){
      if(this.getService() != null){
        graphic.pushMatrix();
            graphic.translate(this.getX(), this.getY());
            
            drawServiceImage(graphic);
            drawServiceName(graphic);   
        graphic.popMatrix();
      }
    }
      
    private void drawServiceImage(PGraphics graphic){
        PImage serviceImg = getService().getIcon();
        this.imgComp.setWidth(this.getWidth()); //TODO: otimizar! Não realizar reposicionamento de imgComp a cada draw
        this.imgComp.setHeight(this.getHeight() - getTextSize(graphic));
        if(serviceImg != null){
            imgComp.setImage(serviceImg);        
            imgComp.draw(graphic);
        }
    }
    
    private int getTextSize(PGraphics graphic){
        return (int)(graphic.textAscent() + graphic.textDescent());
    }

    void drawServiceName(PGraphics graphic){        
        graphic.pushStyle();
            int width = this.imgComp.getWidth();
            int height = this.imgComp.getHeight();
            graphic.textAlign(graphic.CENTER,graphic.TOP);
            graphic.text(this.getService().getName(), width/2, height);
        graphic.popStyle();
    }
  
}
