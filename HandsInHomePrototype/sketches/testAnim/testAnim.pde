int dist;
float angle;
float porc;

void setup(){
  size(640,480);
  angle = (float)Math.PI;
  dist = 100;
}

void draw(){
  background(0);
  translate(width/2,height/2);
  
  ellipse(0,0,50,50);
    
  pushMatrix();
    int middleX = (int)(Math.cos(angle)*(dist/2));
    int middleY = (int)(Math.sin(angle)*(dist/2));
    translate(middleX, middleY);
    float angleRot = (float)(porc * (Math.PI/2));
    
    int x = (int)(Math.cos(angleRot)*(dist/2));
    int y = (int)(Math.sin(angleRot)*(dist/2));
    
    ellipse(x,y,25,25);
    
    if(porc < 1.0){
      porc += 0.03;
    }
    
  popMatrix();
}
