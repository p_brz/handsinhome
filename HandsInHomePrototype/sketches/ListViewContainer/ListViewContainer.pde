import java.awt.Point;

CarouselListContainer container;

boolean isAddingElement;
Point mouseStartPos;
int count;

void setup(){
  size(640,480);
//  container = new ListView(100,50);
  container = new CarouselListContainer(100,50,500,50);
  container.addComponent(new EllipseComponent("0",0,0,50,50));
  count = 1;
  
  isAddingElement = false;
  mouseStartPos = new Point(0, 0);
}

void draw(){
  background(222);
  container.draw(this.g);
  
  if(isAddingElement){    
    int distance = (int)mouseStartPos.distance(mouseX,mouseY);
    ellipse((int)mouseStartPos.getX(),(int)mouseStartPos.getY(), distance, distance);
  }
}

void mouseClicked(){
  Component compClicked = container.getComponentAt(mouseX, mouseY);
  if(compClicked != null && container.contains(compClicked)){
    container.removeComponent(compClicked);
  }
  
  println("clicked at " + compClicked);
}

void mousePressed(){
  isAddingElement = true;
  mouseStartPos.setLocation(mouseX,mouseY);
}

void mouseReleased(){
  if(isAddingElement){
    int distance = (int)mouseStartPos.distance(mouseX,mouseY);
    if(distance > 5){
      container.addComponent(new EllipseComponent(Integer.toString(count),0,0,distance,distance));
      ++count;
    }
    
    isAddingElement = false;
  }
}

void mouseDragged(){
  if(mouseButton == RIGHT){
    isAddingElement = false;
    container.setLocation(mouseX, mouseY);
  }
}

void keyPressed(){
  if(key == CODED){
    if(keyCode == RIGHT){
      this.container.selectNext();
    }
    else if(keyCode == LEFT){
      this.container.selectPrevious();
    }
  }
}
